function [ trajTbl, dataStr ] = loadArgoMat( argoMatPath )
%
%   Loads Argo file in OWC .mat format and returns 
%       - the table with Argo float trajectory
%       - the structures with Argo data (PRES, PRES_QC, TEMP, TEMP_QC, SAL, SAL_QC, 
%           PTMP, PTMP_QC). The *_QC fields are set to ones. 
%
%   Syntax: [trajTbl,dataStr] = loadArgoMat( argoMatPath )
%
%   Input [Required]:
%       argoMatPath - the path to Argo .mat file (OWC format)
%           If the .mat file does not exist, returnes empty table and 2 []s.  
%
%   Output:
%       trajTbl - the float trajectory table (height nProf) with columns:
%           platform_number - cell of char (size nProf*1)
%           cycle_number - numeric (size nProf*1)
%           latitude - numeric (size nProf*1)
%           longitude - numeric (size nProf*1)
%           mtime - datenum format (size nProf*1) 
%           data_mode - char {'R','A','D'}  (size nProf*1)
%               Contains '' if .mat file does not have this variable
%       dataStr - the structure with Argo data (PRES, TEMP, 
%           SAL, PTMP), each a matrix of size [nLayers,nProf]
%           The fields (PRES_QC,TEMP_QC,SAL_QC,PTMP_QC ) contain ones(nLayers,nProf). 
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-05
%
p = inputParser;
addRequired(p,'argoMatPath',@ischar);
parse(p, argoMatPath )
argoMatPath = p.Results.argoMatPath;
%
if( exist( argoMatPath,'file' ) == 0 )
    error([argoMatPath,' does not exit'])
end
%
s = load( argoMatPath );
nProf = length(s.DATES);
if( ~isfield(s,'PLATFORM_NO') && isfield(s,'WMO_ID') )
    s.PLATFORM_NO = s.WMO_ID;
    s = rmfield(s,'WMO_ID');
end
trajTbl = table( cellstr(reshape(s.PLATFORM_NO,nProf,1)),...
            'VariableNames',{'platform_number'} );
trajTbl.cycle_number = reshape(s.PROFILE_NO,nProf,1);
if( isfield( s,'mtime' ) )
    trajTbl.mtime = reshape(s.mtime,nProf,1); % ....
else
    trajTbl.mtime = s.DATES' .* 365.2425 + datenum('1-1-0000');
end
if( isfield( s,'data_mode' ) )
    trajTbl.data_mode = reshape(s.data_mode,nProf,1);
end
trajTbl.latitude = reshape(s.LAT,nProf,1);
trajTbl.longitude = reshape(s.LONG,nProf,1);
%
nLayers = size(s.PRES,1);
if( ~isfield(s,'PTMP') )
    LAT = repmat(trajTbl.latitude,1,nLayers)';
    LONG = repmat(trajTbl.longitude,1,nLayers)';
    SA = gsw_SA_from_SP( s.SAL, s.PRES, LONG, LAT );
    s.PTMP = gsw_pt0_from_t( SA, s.TEMP, s.PRES );
end
%
dataStr = struct( 'PRES',s.PRES, 'TEMP',s.TEMP, 'SAL',s.SAL, 'PTMP',s.PTMP );
dataStr.PRES_QC = ones(nLayers,nProf);
dataStr.TEMP_QC = ones(nLayers,nProf);
dataStr.SAL_QC = ones(nLayers,nProf);
dataStr.PTMP_QC = ones(nLayers,nProf);
%
end


