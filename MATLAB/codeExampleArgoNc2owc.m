% codeExampleArgoNc2owc.m
%   Test the functions transforming Argo NetCDF file to Argo OWC mat file
%
clear variables
projDir = '/Users/nikolaynezlin/Documents/MATLAB/argoOWCviewer/argoowcviewer/';
argoFn = 'data/5904925_prof.nc';
% Read the Argo NetCDF file
[ trajTbl, dataRaw, dataAdj, metaPar ] = readArgoNc([ projDir, argoFn ]);
% Figure 
figure( 'Position',[100 100 1000 600], 'visible','on','paperpositionmode','auto','Color','w' )
ax1 = subplot( 1,2,1);
% Map and profiles with colors demonstrating the location of profiles along the 
% float trajectory
plotTrajMap( trajTbl, 'plotAxes',ax1 );
% Profiles of one variable (TEMP, SAL or PTMP)
ax2 = subplot(1,2,2);
ax2 = plotProf( dataRaw.PRES, dataRaw.SAL, 'FontSize',16, 'mtime',trajTbl.mtime,....
    'plotAxes',ax2,...
    'xLab','Salinity','yLim',[1000 2000],...
    'title',['Argo WMO ID ',trajTbl.platform_number{1}],...
    'legendFontSize',12, 'legendLocation','northeast' );
% Add one selected profile
kProf = 10;
h2 = plot( ax2, dataRaw.SAL(:,kProf),dataRaw.PRES(:,kProf), 'Color','k','LineWidth',5 );
ax2=axes('position',get(gca,'position'),'visible','off');
hLeg = legend(ax2,h2,datestr(trajTbl.mtime(kProf),'DD-mmm-yyyy'),'Location','southeast');
hLeg.FontSize = 20;

%% TS diagrams with profiles in time and with quality codes
figure( 'Position',[100 100 600 600], 'visible','on','paperpositionmode','auto','Color','w' )
plotTS( dataRaw, 'sLim',[34.3 34.8], 'ptLim',[1.5 10], 'densLevels',20:0.1:30,...
    'mtime',trajTbl.mtime ); 

%% Calculate the table with missing values (raw and adjusted)
misValTblR = calcMisValTbl( dataRaw );
disp('Raw data')
disp(misValTblR)
disp('Adjusted data')
misValTblA = calcMisValTbl( dataAdj );
disp(misValTblA)

%% Calculate the number of data with different QC codes
% Logical column 'sel' is used for selection of QC 
QCtbl = calcQcTbl( dataRaw.SAL_QC );
disp(QCtbl)
% Display selected codes
disp('Selected codes')
disp( QCtbl.code(QCtbl.sel) )
% Unselect (false) the codes to remove 
QCtbl.sel(QCtbl.code=='3') = true;
QCtbl.sel(QCtbl.code=='4') = false;
% Fill with NaNs the data with selected code ('4')
dataRaw2 = dataStrQc2nan( dataRaw, 'SAL_QC', QCtbl.code(~QCtbl.sel) );
% Fill with NaNs SAL<20
dataRaw2.SAL(dataRaw2.SAL<20) = NaN;
% Plot the TS diagrams before and after selected QC data
figure( 'Position',[100 100 1000 600], 'visible','on','paperpositionmode','auto','Color','w' )
ax1 = subplot(1,2,1);
plotTSQC( dataRaw, 'plotAxes',ax1, 'densLevels',0:5:30 );
ax2 = subplot(1,2,2);
plotTSQC( dataRaw2, 'plotAxes',ax2, 'densLevels',0:5:30 );

%% Calculate and plot the maximum profile depth with SAL data
figure( 'Position',[100 100 1000 600], 'visible','on','paperpositionmode','auto','Color','w' )
PRES1 = dataRaw.PRES;
PRES1(isnan(dataRaw.SAL)) = NaN;
trajTbl.maxPRESwPSAL = max( PRES1 )';
ax1 = subplot(1,2,1);
plotTimeBar( trajTbl.mtime, trajTbl.maxPRESwPSAL, 'yDir','rev',...
 'timeTickStyle','months', 'timeTickInterval',6, 'yLab','Pressure (dbar)',...
 'plotAxes',ax1 );

% Remove the profiles with depth <1900dbar
trajTbl.sel = true(height(trajTbl),1);
trajTbl.sel(trajTbl.maxPRESwPSAL<1900) = false;
[ trajTbl2b, dataRaw2b, metaPar2b ] = dataProfSel( trajTbl, dataRaw2, metaPar, trajTbl.sel );
ax2 = subplot(1,2,2);
plotTimeBar( trajTbl2b.mtime, trajTbl2b.maxPRESwPSAL, 'yDir','rev',...
 'timeTickStyle','months', 'timeTickInterval',6, 'yLab','Pressure (dbar)',...
  'plotAxes',ax2 );

%% Interpolate profiles to fixed pressure levels
pres2Int = [0,50:50:max(dataRaw2b.PRES(:))];
dataRaw2c = dataStrIntPres( dataRaw2b, pres2Int );
figure( 'Position',[100 100 1000 600], 'visible','on','paperpositionmode','auto','Color','w' )
ax1 = subplot(1,2,1);
plotProf( dataRaw2b.PRES, dataRaw2b.PTMP, 'FontSize',16, 'mtime',trajTbl2b.mtime,....
    'xLab','Temperature (^oC)','yLim',[1000 2000], 'title',trajTbl2b.platform_number{1},...
    'legendFontSize',16, 'legendLocation','southeast', 'plotAxes',ax1 );
ax2 = subplot(1,2,2);
plotProf( dataRaw2c.PRES, dataRaw2c.PTMP, 'FontSize',16, 'mtime',trajTbl2b.mtime,....
    'xLab','Temperature (^oC)','yLim',[1000 2000], 'title',trajTbl2b.platform_number{1},...
    'legendFontSize',16, 'legendLocation','southeast', 'plotAxes',ax2 );

%% Save data to mat file
saveFn = [projDir,'data/',trajTbl2b.platform_number{1},'.mat'];
save2OWCmat( saveFn, trajTbl2b, dataRaw2c );

%% Load the saved Argo file and see the float trajectory and salinity profiles
clear variables
projDir = '/Users/nikolaynezlin/Documents/MATLAB/argoOWCviewer/argoowcviewer/';
argoFn = 'data/5904925.mat';
[ trajTbl, dataStr ] = loadArgoMat( [projDir,argoFn] );
figure( 'Position',[100 100 1000 600], 'visible','on','paperpositionmode','auto','Color','w' )
ax1 = subplot( 1,2,1);
% Map and profiles with colors demonstrating the location of profiles along the 
% float trajectory
plotTrajMap( trajTbl, 'plotAxes',ax1 );
% Profiles of one variable (TEMP, SAL or PTMP)
ax2 = subplot(1,2,2);
ax2 = plotProf( dataStr.PRES, dataStr.SAL, 'FontSize',16, 'mtime',trajTbl.mtime,....
    'plotAxes',ax2,...
    'xLab','Salinity','yLim',[1000 2000],...
    'title',['Argo WMO ID ',trajTbl.platform_number{1}],...
    'legendFontSize',12, 'legendLocation','northeast' );

