function [ CLM_TEMP, CLM_SAL, CLM_PRES ] = climMIMOC( LAT, LONG, mtime, MIMOCpath )
% 
%   Finds the MIMOC Climatology in the path MIMOCpath and selects 
%   the profiles of climatic temperature and salinity for the locations and seasons 
%   determined by LAT, LONG and mtime.    
%
%   In argoOWCviewer, clim_get_MIMOC is called from the function climAlongTraj
%
% Syntax:  [ CLM_TEMP, CLM_SAL, CLM_PRES ] = climMIMOC( LAT, LONG, mtime, MIMOCpath );
% 
%   Input [Required]:
%       LAT, LONG, mtime - arrays of the length equal to the number of
%           profiles (nProf)
%       MIMOCpath - location of MIMOC database on the local computer
%           (e.g., MIMOCpath = 'my_dir.../MIMOC/data/')
%
%   Output:
%       CLM_TEMP, CLM_SAL, CLM_PRES - arrays of size(nLayers,nProf). 
%           For MIMOC climatology, nLayers=81;
%       CLM_TEMP - temperature
%       CLM_SAL - salinity
%       CLM_PRES - pressure
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-25
%
p = inputParser;
addRequired(p,'LAT',@isnumeric);
addRequired(p,'LONG',@isnumeric);
addRequired(p,'mtime',@isnumeric);
addRequired(p,'MIMOCpath',@ischar);
parse(p, LAT, LONG, mtime, MIMOCpath )
% 
LAT = p.Results.LAT;
LONG = p.Results.LONG;
mtime = p.Results.mtime;
MIMOCpath = p.Results.MIMOCpath;
% Check the inputs, set defaults
if( (length(LAT)~=length(LONG)) && (length(LAT)~=length(mtime)))
    error('LAT, LONG and mtime must be the same length')
end
if( ~exist(MIMOCpath,'dir') )
    error(['No dir ',MIMOCpath] )
end
%
nProf = length( mtime );
MIMOC_fn = [ MIMOCpath, 'MIMOC_Z_GRID_v2.2_PT_S_month01.nc' ];
PRESSURE = ncread( MIMOC_fn, 'PRESSURE' );  
nLayers = length(PRESSURE);
CLM_PRES = NaN( nLayers, nProf );
CLM_TEMP = NaN( nLayers, nProf );
CLM_SAL = NaN( nLayers, nProf );
%
h_wait = waitbar( 0, 'Extracting data from MIMOC...' );
wax = findobj(h_wait, 'type','axes');
tax = get(wax,'title');
set(tax,'fontsize',16)
warning('off','all')
for kProf = 1:nProf
    waitbar( kProf / nProf, h_wait )
    lat1 = LAT(kProf);
    lon1 = LONG(kProf);
    mtime1 = mtime(kProf);
    [ ~, month1, ~ ] = ymd( datetime( mtime1,'ConvertFrom','datenum' ) );
    MIMOC_fn = [ MIMOCpath, 'MIMOC_Z_GRID_v2.2_PT_S_month',num2str(month1,'%02.0f'),'.nc' ];
    MIMOClat = ncread( MIMOC_fn, 'LATITUDE' );
    MIMOClon = ncread( MIMOC_fn, 'LONGITUDE' );
    while( lon1 < MIMOClon(1) )
        lon1 = lon1 + 360;
    end
    while( lon1 > MIMOClon(end) )
        lon1 = lon1 - 360;
    end
%     n_lon = length( MIMOClon );
    MIMOCpres = ncread( MIMOC_fn, 'PRESSURE' );
    if( ~isequal( PRESSURE, MIMOCpres ) )
        disp( 'Different PRESSURE in MIMOC files' );
        return
    end
    [~,kLat] = min(abs(MIMOClat-lat1));
    [~,kLon] = min(abs(MIMOClon-lon1));
    SALINITY = squeeze(ncread( MIMOC_fn, 'SALINITY',[kLon, kLat, 1],[1,1,Inf] ));
    POTENTIAL_TEMPERATURE = squeeze(ncread( MIMOC_fn, 'POTENTIAL_TEMPERATURE',[kLon, kLat, 1],[1,1,Inf] ));
    PRESSURE = ncread( MIMOC_fn, 'PRESSURE' );
    [SA, ~] = gsw_SA_from_SP( SALINITY, PRESSURE, lon1, lat1 );
    TEMPERATURE = gsw_t_from_pt0( SA, POTENTIAL_TEMPERATURE, PRESSURE );
    %
    CLM_TEMP( :,kProf ) = TEMPERATURE;
    CLM_SAL( :,kProf ) = SALINITY;
    CLM_PRES( :,kProf ) = PRESSURE;
    %
end
warning('on','all')
close( h_wait )
%
end

