function ax = plotOWCsOffsetsBB( owcTbl, varargin )
%
%   Plot the salinity offsets calculated by OWC method as 'bubbles' as time
%       series
%
%   Stntax: ax = plotOWCsOffsetsBB( owcStr, ... )
%
%   Input [Required]:
%       owcTbl - includes the columns mtime and the OWC results characterizing profiles:
%              (avg_Soffset,avg_Soffset_err,avg_Staoffset,avg_Staoffset_err)
%   Input [Optional]:
%       plotAxes - Axes object. Created by default
%       xLim - X-axis limits (datenum, default min/max)
%       yLim - Y (deltaS) limits (numeric 2x1) (default auto)
%       sOffColor - the color of S offset (default [0.5 0.5 0.5])
%       sOffLineWidth - line width of S offset (default 1)
%       sOffMarker - marker of S offset (default 'o')
%       sOffFaceColor - face color of the S offset symbols (default [0 0.5 1])
%       sOffMarkerSize - marker size of the S offset symbols (default 6)
%       zeroColor - the color of zero (default 'k')
%       zeroLineWidth - the width of zero line (default 1)
%       profFitLineColor - the color of profile fit coefficients (default 'k')
%       profFitLineWidth - the line width of profile fit coefficients (default 1)
%       profFitMarkerSize - the coefficient of profile fit marker size
%           (default auto)
%       profFitMarkerEdgeColor - profile fit marker edge color (default 'k')
%       profFitMarkerColor - colorscale (matrix N*3) (default [])
%       cLim - the limits of the profile fit coefficients color scale (numeric 2x1) (default auto)
%       fontSize - font size of the axes (numeric) (default 12)
%       yLab - Y label (default '\Delta S')
%       yLabFontSize - font size of the Y label
%       title - figure title (char)
%       titleFontSize - title font size (numeric, default 20)
%       timeTickStyle - the style of X-axis (mtime) ticks:
%           {'auto' (default), 'years','months','days'}
%       timeTickInterval - the interval between the time ticks 
%           (numeric, defailt 1)
%
%   Output:     
%       ax - Axes object. 
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-26
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'owcTbl',@istable);
addParameter(p,'plotAxes',[],isaxes);
addParameter(p,'xLim',[],is2nums);
addParameter(p,'yLim',[],is2nums);
addParameter(p,'sOffColor',[0.5 0.5 0.5],iscolor);
addParameter(p,'sOffLineWidth',1,@isnumeric);
addParameter(p,'sOffMarker','o',@ischar);
addParameter(p,'sOffFaceColor',[0.5 0.5 0.5],iscolor);
addParameter(p,'sOffMarkerSize',6,@isnumeric);
addParameter(p,'zeroColor','k',iscolor);
addParameter(p,'zeroLineWidth',1,@isnumeric);
addParameter(p,'profFitLineColor','k',iscolor);
addParameter(p,'profFitLineWidth',1,@isnumeric);
addParameter(p,'profFitMarkerSize',[],@isnumeric);
addParameter(p,'profFitMarkerEdgeColor','k',iscolor);
addParameter(p,'profFitMarkerColor',[],@ismatrix);
addParameter(p,'cLim',[],is2nums);
addParameter(p,'fontSize',12,@isnumeric);
addParameter(p,'yLab','\Delta S',@ischar);
addParameter(p,'yLabFontSize',16,@isnumeric);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
istimetick = @(x) (ischar(x) && (ismember(x,{'auto','years','months','days'})));
addParameter(p,'timeTickStyle','auto',istimetick);
addParameter(p,'timeTickInterval',1,@isnumeric);
parse(p, owcTbl, varargin{:} )
owcTbl = p.Results.owcTbl;
ax = p.Results.plotAxes;
xLim = p.Results.xLim;
yLim = p.Results.yLim;
sOffColor = p.Results.sOffColor;
sOffLineWidth = p.Results.sOffLineWidth;
sOffMarker = p.Results.sOffMarker;
sOffFaceColor = p.Results.sOffFaceColor;
sOffMarkerSize = p.Results.sOffMarkerSize;
zeroColor = p.Results.zeroColor;
zeroLineWidth= p.Results.zeroLineWidth;
profFitLineColor = p.Results.profFitLineColor;
profFitLineWidth = p.Results.profFitLineWidth;
profFitMarkerSize = p.Results.profFitMarkerSize;
profFitMarkerEdgeColor = p.Results.profFitMarkerEdgeColor;
profFitMarkerColor = p.Results.profFitMarkerColor;
cLim = p.Results.cLim;
fontSize = p.Results.fontSize;
yLab = p.Results.yLab;
yLabFontSize = p.Results.yLabFontSize;
plotTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
timeTickStyle = p.Results.timeTickStyle;
timeTickInterval = p.Results.timeTickInterval;
%
% Check the column names in owcTbl
owcColNames = {'mtime','avg_Soffset','avg_Soffset_err','avg_Staoffset','avg_Staoffset_err'};
iCol = ismember(owcColNames,owcTbl.Properties.VariableNames);
if(~all(iCol))
    error(['No columns in owcTbl: ',strjoin(owcColNames(~iCol),',')])
end
%
if( isempty(ax) )
    ax = axes;
else
    axes(ax)
    cla(ax)
end
if( isempty(xLim) )
    dX = diff(owcTbl.mtime([1,end]));
    xLim = [min(owcTbl.mtime)-dX./50 max(owcTbl.mtime)+dX./50];
end
if(isempty(profFitMarkerSize))
    profFitMarkerSize = 200./max(abs(owcTbl.profFitCoef));
end
if(isempty(profFitMarkerColor))
    profFitMarkerColor = makeColormap4anom(64);
end
colormap(ax,profFitMarkerColor)
%
plot( xLim, [0 0], 'Color',zeroColor, 'LineWidth',zeroLineWidth )
hold on
plot( owcTbl.mtime, owcTbl.avg_Soffset, 'Color',sOffColor, 'LineWidth',sOffLineWidth,...
    'Marker','none' );
errorbar( owcTbl.mtime, owcTbl.avg_Soffset, 2*owcTbl.avg_Soffset_err,...
    'Color',sOffColor, 'LineWidth',sOffLineWidth, 'Marker',sOffMarker,...
    'MarkerFaceColor',sOffFaceColor, 'MarkerSize',sOffMarkerSize )
plot( owcTbl.mtime, owcTbl.avg_Staoffset, 'Color',profFitLineColor, 'LineWidth',profFitLineWidth);
scatter( owcTbl.mtime, owcTbl.avg_Staoffset, abs(owcTbl.profFitCoef).*profFitMarkerSize,...
    owcTbl.profFitCoef, 'filled', 'MarkerEdgeColor',profFitMarkerEdgeColor )
xlim(xLim)
if(~isempty(yLim))
    ylim(yLim)
end
if(isempty(cLim))
    maxFC = max(abs(owcTbl.profFitCoef));
    cLim = [-maxFC maxFC];
end
caxis(cLim)
set( ax, 'FontSize',fontSize )
ylabel(yLab, 'FontSize',yLabFontSize)
grid('on')
box('on')
switch timeTickStyle
    case 'auto'
        xTicks = get( ax, 'XTick' );
        dateFmt = 'ddmmmyy';
    case 'days'
        xTicks = calcTimeTicks( owcTbl.mtime, timeTickStyle, timeTickInterval );
        dateFmt = 'ddmmmyy';
    case {'months','years'}
        xTicks = calcTimeTicks( owcTbl.mtime, timeTickStyle, timeTickInterval );
        dateFmt = 'mmmyy';
end
set( ax, 'XTick',xTicks, 'XTickLabel',datestr(xTicks,dateFmt) )
set( ax,'TickLength',[0.01, 0.01], 'TickDir','out','LineWidth',2)
title( ax, plotTitle, 'FontSize', titleFontSize )
set(ax,'Layer','top')

end


