function guiArgoNetCDF2owc
%
%   GUI interface to convert Argo floats NetCDF files to OWC mat files
%
%   Syntax: guiArgoNetCDF2owc
%
%   Input: No
%
%   Output: No    
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-29
%
close all
% Set the structures for handles (hh), settings (ss), data (dd) and figure
%   (ff)
hh = struct; % Handles
ss = struct; % Settings (paths, file names, etc.)
dd = struct; % Data
ff = struct; % Figure settings
% Set useful variables
warndlg_opts = struct( 'WindowStyle','modal', 'Interpreter','tex');
fsp1 = '<HTML><FONT size=+1>';
x_rect = [];
y_rect = [];
colormapList = {'parula','jet','hsv','hot','cool','spring','summer',...
    'autumn','winter','gray','bone','copper','pink'};
GSHHSlist = {'','c','l','i','h','f';'auto','coarse','low','intermedian','high','full'};
timeTicksList = {'auto','years','months','days'};
axPos = [0.1,0.1,0.8,0.8];
%
% Anonymous Functions
is2lims = @(x) (all(isnumeric(x)) && (length(x)==2) && (x(1)<x(2)) );
%
% Find the code source directory
codeDir = which('guiArgoNetCDF2owc');
codeDir = strsplit(codeDir,filesep);
codeDir = strjoin(codeDir(1:end-1),filesep);
% Read the RBR logo
RBRlogo = imread([codeDir,filesep,'ArgoRBRlogo.png']);
% Load initial settings
ss.initSettingsFn = 'argoOWCinitSettings.mat';
if(exist([codeDir,filesep,ss.initSettingsFn],'file')==2)
    load( [codeDir,filesep,ss.initSettingsFn] )
end
ss.paths.codeDir = codeDir;
%
% Start the GUI window
hh.hFig = figure('Visible','on','Units','normalized','Position',[0.1,0.1,0.8,0.8],...
    'Toolbar','none', 'Menubar','none', 'Name','guiArgoNetCDF2owc', 'NumberTitle','off');
menuMtbl = cell2table( {... % Main menu table. 'mat' - activate when '.mat' file is loaded
    'File',             'File',                 1;...
    'Figure',           'Figure',               1;...
    'Metadata',         'Metadata',             0;...
    'Select_data',      'Select datasets',      0;...
    'Select_data_points','Select data points',  1;...
    'Select_profiles',  'Select profiles',      1;...
    'Save2mat',         'Save to .mat file',    1;...
    }, 'VariableNames',{'Tag','Label','mat'} );
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[ fsp1, menuMtbl.Label{1} ],...
    'Tag',menuMtbl.Tag{1}, 'Enable','on' );    % File
%
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Set directory with NetCDF files' ],...
    'Separator','off','Callback',@callback_set_dir_nc);
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Set directory with mat files' ],...
    'Separator','off','Callback',@callback_set_dir_mat);
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Open Argo NetCDF file' ],...
    'Callback',@callback_open_argo_nc, 'Separator','on' );
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Open Argo OWC .mat file' ],...
    'Callback',@callback_open_argo_mat, 'Separator','off' );
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Exit' ],...
    'Separator','on','Callback',@callback_exit);
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[ fsp1, menuMtbl.Label{2} ],...       % Figure
    'Tag',menuMtbl.Tag{2}, 'Enable','off' ); 
menuFtbl = cell2table( {...
    'Map',              'Map of float trajectory',      'on',   1,  @callback_map;...
    'prof_TEMP',        'Profiles of temperature',      'on',   1,  @callback_prof_TEMP;...
    'prof_SAL',         'Profiles of salinity',         'off',  1,  @callback_prof_SAL;...
    'prof_dP',          'Profile pres_adjusted - pres', 'on',   0,  @callback_prof_dP;...
    'prof_dT',          'Profile temp_adjusted - temp', 'off',  0,  @callback_prof_dT;...
    'prof_dS',          'Profile psal_adjusted - psal', 'off',  0,  @callback_prof_dS;...
    'TS_diagr',         'Theta-S diagram',              'on',   1,  @callback_TS_diagr;...
    'theta_S_QC',       'Theta-S diagram with QC',      'off',  0,  @callback_theta_S_QC;...
    'profile_max_depth','Profile max depth',            'on',   1,  @callback_profile_max_depth;...
    'data_profiles',    'Valid data in profiles',       'off',  1,  @callback_data_profiles;...
    'Figure_properties','Figure properties',            'on',   1,  @callback_fig_properties;...
    'export_figure',    'Export plot',                  'on',   1,  @callback_export_plot;...
    'new_figure',       'Open plot in new figure',      'off',  1,  @callback_new_figure;...   
    }, 'VariableNames',{'Tag','Label','sep','mat','callback'} );
%
for kmF = 1:height(menuFtbl)
    uimenu( hMenuItem2add, 'Label',[ fsp1, menuFtbl.Label{kmF} ],...
    'Callback',menuFtbl.callback{kmF}, 'Tag',menuFtbl.Tag{kmF},...
    'Enable','on', 'Checked','off', 'Separator',menuFtbl.sep{kmF});
end
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[ fsp1, menuMtbl.Label{3} ],...      % Metadata
    'Tag',menuMtbl.Tag{3}, 'Enable','off');     
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Adjustment equations' ],...
    'Callback',@callback_metadata, 'Tag','Metadata_str','Enable','on','Checked','off');
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Missing values' ],...
    'Callback',@callback_missing_values, 'Tag','Missing_values','Enable','on','Checked','off');
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[ fsp1, menuMtbl.Label{4} ],...      % 'Select datasets'
    'Tag',menuMtbl.Tag{4}, 'Enable','off');    
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Select' ],...
    'Tag','Select_datasets','Enable','on', 'Callback',@callback_sel_datasets );
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[ fsp1, menuMtbl.Label{5} ],...   'Select data points'
    'Tag',menuMtbl.Tag{5}, 'Enable','off' );
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Select QC' ],...
    'Tag','Select_QC','Enable','on', 'Callback',@callback_sel_QC );
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Select points in rectangle' ],...
    'Tag','Select_rect','Enable','on', 'Callback',@callback_sel_rect );
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Remove points in rectangle' ],...
    'Tag','Remove_rect','Enable','off', 'Callback',@callback_rem_rect );
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[ fsp1, menuMtbl.Label{6} ],...      'Select profiles'
    'Tag',menuMtbl.Tag{6}, 'Enable','off' );
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Select' ],...
    'Tag','Sel_prof','Enable','on', 'Callback',@callback_sel_prof );
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[ fsp1, menuMtbl.Label{7} ],...    'Save to .mat file'
    'Tag',menuMtbl.Tag{7},'Enable','off' );
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Save' ],...
    'Tag','Save_2_mat','Enable','on', 'Callback',@callback_save2mat );
%
hh.staticText_argoFn = uicontrol( hh.hFig, 'Style','text',...
    'Units','normalized','Position',[0.02 0.88 0.28 0.1],...
    'FontSize',16, 'HorizontalAlignment','left',...
    'String','No Argo NetCDF file selected' );
hh.staticText_argoid = uicontrol( hh.hFig, 'Style','text',...
    'Units','normalized','Position',[0.02 0.82 0.28 0.1],...
    'FontSize',16, 'HorizontalAlignment','left',...
    'String','' );
hh.staticText_datasets = uicontrol( hh.hFig, 'Style','text',...
    'Units','normalized','Position',[0.02 0.76 0.28 0.1],...
    'FontSize',16, 'HorizontalAlignment','left',...
    'String','', 'Visible','off' );
%
hh.staticText_data_mode = uicontrol( hh.hFig, 'Style','text',...
    'Units','normalized','Position',[0.02 0.60 0.28 0.1],...
    'FontSize',16, 'HorizontalAlignment','left',...
    'String','DATA MODE:', 'Visible','off' );
hh.uitable_data_mode = uitable( hh.hFig, 'FontSize',14,...
    'Units','normalized','Position',[0.015 0.50 0.28 0.15],... 
    'Visible','off', 'ColumnWidth',{55,50,110,110},...
    'ColumnFormat',{'char','char',[],[]} );
%
hh.hpax1 = uipanel( hh.hFig, 'Position',[0.3 0 0.7 1],'BackgroundColor','w' );
delete(findobj(hh.hpax1, 'type', 'axes'))
hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',[0.1, 0.1, 0.8, 0.8 ] );
image(RBRlogo);
set( hh.hax1, 'XTick',[], 'YTick',[], 'Box','on' )
%
% Nested functions - callbacks
% Set directory with NetCDF files
function callback_set_dir_nc( hObject, eventdata )
    if(~isfield(ss.paths,'ncDir') || (~ischar(ss.paths.ncDir)))
        ss.paths.ncDir = ss.paths.codeDir;
    end
    data_dir1 = uigetdir( ss.paths.ncDir );
    if( data_dir1 )
        ss.paths.ncDir = data_dir1;
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
    end
end
%
% Set directory with OWC mat files
function callback_set_dir_mat( hObject, eventdata )
    if(~isfield(ss.paths,'matDir') || (~ischar(ss.paths.matDir)))
        ss.paths.matDir = ss.paths.codeDir;
    end
    data_dir1 = uigetdir( ss.paths.matDir );
    if( data_dir1 )
        ss.paths.matDir = data_dir1;
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
    end
end
%
% Read Argo NetCDF file
function callback_open_argo_nc( hObject, eventdata )
    [argoFnR,ncDirR] = uigetfile([ss.paths.ncDir,filesep,'*.nc']);
    if( argoFnR ~= 0 )
        ss.argoFn = argoFnR;
        ss.paths.ncDir = ncDirR;
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
        [dd.trajTbl,dd.dataRaw,dd.dataAdj,dd.metaPar] = readArgoNc([ss.paths.ncDir,filesep,ss.argoFn]);
        dd.nProf = height(dd.trajTbl);
        dd.nLayers = size(dd.dataRaw.PRES,1);
        ff = setFig; % Figure settings
        % Remove the profiles with no coordinates or time
        indxFin = isfinite(dd.trajTbl.latitude)&isfinite(dd.trajTbl.longitude)&isfinite(dd.trajTbl.mtime);
        if( ~all(indxFin) )
            [dd.trajTbl,dd.dataRaw,dd.metaPar] = dataProfSel(dd.trajTbl,dd.dataRaw,dd.metaPar,indxFin);
            [~,dd.dataAdj,~] = dataProfSel(dd.trajTbl,dd.dataAdj,[],indxFin);
            dd.nProf = height(dd.trajTbl);
        end
        if( isempty(dd.trajTbl.latitude) || isempty(dd.trajTbl.longitude) )
            warndlg('\fontsize {16} Coordinates contain only NaN',...
                'Corrupted data', warndlg_opts);
        else
            % Enable menu elements
            for k_obj = 1:height(menuMtbl)
                set( findobj( 'Tag',menuMtbl.Tag{k_obj} ), 'Enable','on' )
            end
            set( findobj( 'Tag','prof_dP' ), 'Enable','on' )
            set( findobj( 'Tag','prof_dT' ), 'Enable','on' )
            set( findobj( 'Tag','prof_dS' ), 'Enable','on' )
            set( findobj( 'Tag','theta_S_QC' ), 'Enable','on' )
            set( findobj( 'Tag','Select_QC' ), 'Enable','on' )
            menu_figure_uncheck()
            % File name
            hh.staticText_argoFn.String = ['File: ',ss.argoFn];
            % The string with profiles and layers
            argoid_str = [ 'Argo # ', dd.trajTbl.platform_number{1},' / ',...
                num2str( dd.nProf ),' profiles / ',...
                num2str( dd.nLayers ),' layers' ];
            set(hh.staticText_argoid,'String', argoid_str );
            % Map
            delete(findobj(hh.hpax1, 'type','axes'))
%             dd.title = dd.trajTbl.platform_number{1};
            hh.hax1 = axes('Parent',hh.hpax1, 'Units','normalized', 'Position',axPos);
            plotTrajMap(dd.trajTbl, 'plotAxes',hh.hax1,...
                'title',ff.title, 'titleFontSize',ff.titleFontSize,...
                'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
                'colormap',ff.colormap,...
                'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
                'axesFontSize',ff.map.axesFontSize,...
                'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize );
            set( findobj( 'Tag','Map' ), 'Checked','on' )
            %   Selected dataset
            dd.dataStr = dd.dataAdj;
            dd.datasetsSel = 'adjusted';
            hh.staticText_datasets.String = ['DATASETS: ',dd.datasetsSel];
            hh.staticText_datasets.Visible = 'on';
            % The table with data mode 
            hh.staticText_data_mode.Visible = 'on';
            hh.uitable_data_mode.Visible = 'on';
            hh.uitable_data_mode.Data = setDataModeCell(dd.trajTbl);
            hh.uitable_data_mode.ColumnName = { [fsp1,'Mode'],...
                [fsp1,'N'],[fsp1,'Start'],[fsp1,'End'] };
            hh.uitable_data_mode.RowName = [];
            % Max depth
            PRES1 = dd.dataStr.PRES;
            PRES1(isnan(dd.dataStr.SAL)) = NaN;
            dd.trajTbl.maxPRESwPSAL = max(PRES1)';
            % The number of profiles
            dd.trajTbl.nvPSAL = sum(isfinite(dd.dataStr.SAL))';
            dd.nProfChanged = false;
            set( findobj( 'Tag','Select_data' ), 'Enable','on' )
        end
    end
end
%
% Load Argo mat file
function callback_open_argo_mat( hObject, eventdata )
    [argoFnR,matDirR] = uigetfile([ss.paths.matDir,filesep,'*.mat']);
    if( argoFnR ~= 0 )
        ss.argoFn = argoFnR;
        ss.paths.matDir = matDirR;
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
        [dd.trajTbl,dd.dataStr] = loadArgoMat([ss.paths.matDir,filesep,ss.argoFn]);
        dd.dataRaw = [];
        dd.dataAdj = [];
        dd.nProf = height(dd.trajTbl);
        dd.nLayers = size(dd.dataStr.PRES,1);
        dd.metaPar = [];
        ff = setFig; % Figure settings
        % Remove the profiles with no coordinates or time
        indxFin = isfinite(dd.trajTbl.latitude)&isfinite(dd.trajTbl.longitude)&isfinite(dd.trajTbl.mtime);
        if( ~all(indxFin) )
            [dd.trajTbl,dd.dataStr,dd.metaPar] = dataProfSel(dd.trajTbl,dd.dataStr,dd.metaPar,indxFin);
            dd.nProf = height(dd.trajTbl);
        end
        % Add SAL_QC
        dd.dataStr.SAL_QC = repmat('1',size(dd.dataStr.SAL));
        if( isempty(dd.trajTbl.latitude) || isempty(dd.trajTbl.longitude) )
            warndlg('\fontsize {16} Coordinates contain only NaN',...
                'Corrupted data', warndlg_opts);
        else
            % Enable menu elements
            for k_obj = 1:height(menuMtbl)
                set( findobj( 'Tag',menuMtbl.Tag{k_obj} ), 'Enable','on' )
            end
            set( findobj( 'Tag','prof_dP' ), 'Enable','off' )
            set( findobj( 'Tag','prof_dT' ), 'Enable','off' )
            set( findobj( 'Tag','prof_dS' ), 'Enable','off' )
            set( findobj( 'Tag','theta_S_QC' ), 'Enable','off' )
            set( findobj( 'Tag','Select_QC' ), 'Enable','off' )
            menu_figure_uncheck()
            % File name
            hh.staticText_argoFn.String = ['File: ',ss.argoFn];
            % The string with profiles and layers
            argoid_str = [ 'Argo # ', dd.trajTbl.platform_number{1},' / ',...
                num2str( dd.nProf ),' profiles / ',...
                num2str( dd.nLayers ),' layers' ];
            set(hh.staticText_argoid,'String', argoid_str );
            % Map
            delete(findobj(hh.hpax1, 'type','axes'))
%             dd.title = dd.trajTbl.platform_number{1};
            hh.hax1 = axes('Parent',hh.hpax1, 'Units','normalized', 'Position',axPos);
            plotTrajMap(dd.trajTbl, 'plotAxes',hh.hax1,...
                'title',ff.title, 'titleFontSize',ff.titleFontSize,...
                'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
                'colormap',ff.colormap,...
                'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
                'axesFontSize',ff.map.axesFontSize,...
                'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize );
            set( findobj( 'Tag','Map' ), 'Checked','on' )
            dd.datasetsSel = 'unknown';
            hh.staticText_datasets.String = ['DATASETS: ',dd.datasetsSel];
            hh.staticText_datasets.Visible = 'on';
            % The table with data mode 
            hh.staticText_data_mode.Visible = 'on';
            hh.uitable_data_mode.Visible = 'on';
            hh.uitable_data_mode.Data = setDataModeCell(dd.trajTbl);
            hh.uitable_data_mode.ColumnName = { [fsp1,'Mode'],...
                [fsp1,'N'],[fsp1,'Start'],[fsp1,'End'] };
            hh.uitable_data_mode.RowName = [];
            dd.nProfChanged = false;
            % Max depth
            PRES1 = dd.dataStr.PRES;
            PRES1(isnan(dd.dataStr.SAL)) = NaN;
            dd.trajTbl.maxPRESwPSAL = max(PRES1)';
            % The number of valid salinity scans in profiles
            dd.trajTbl.nvPSAL = sum(isfinite(dd.dataStr.SAL))';
            dd.nProfChanged = false;
            set( findobj( 'Tag','Select_data' ), 'Enable','on' )
        end
    end
end
%
% Exit
function callback_exit( hObject, eventdata )
    save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
    close( hh.hFig )
end
%
% Trajectory map
function callback_map( hObject, eventdata )
    delete(findobj(hh.hpax1, 'type','axes'))
    hh.hax1 = axes('Parent',hh.hpax1, 'Units','normalized','Position',axPos );
    plotTrajMap(dd.trajTbl, 'plotAxes',hh.hax1,...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
        'colormap',ff.colormap,...
        'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
        'axesFontSize',ff.map.axesFontSize,...
        'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize );
    menu_figure_uncheck()
    set( findobj( 'Tag','Map' ), 'Checked','on' )
end
%
% Temperature profiles
function callback_prof_TEMP( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.TEMP(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotProf( dd.dataStr.PRES, dd.dataStr.PTMP, 'plotAxes',hh.hax1,...
            'xLab','Potential temperature (^oC)','mtime',dd.trajTbl.mtime,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize, 'fontSize',ff.fontSize,...
            'colormap',ff.colormap, 'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'yLim',ff.prof.presLim, 'xLim',ff.prof.tempLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','prof_TEMP' ), 'Checked','on' )
    end
end
%
% Salinity profiles
function callback_prof_SAL( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotProf( dd.dataStr.PRES, dd.dataStr.SAL, 'plotAxes',hh.hax1,...
            'xLab','Salinity','mtime',dd.trajTbl.mtime,... 
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'fontSize',ff.fontSize, 'colormap',ff.colormap,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'yLim',ff.prof.presLim, 'xLim',ff.prof.salLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','prof_SAL' ), 'Checked','on' )
    end
end
%
% TS diagram   
function callback_TS_diagr( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.TEMP(:)))==0)||...
            (sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotTS(dd.dataStr, 'plotAxes',hh.hax1, 'mtime',dd.trajTbl.mtime,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'fontSize',ff.fontSize, 'colormap',ff.colormap,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'ptLim',ff.prof.tempLim, 'sLim',ff.prof.salLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','TS_diagr' ), 'Checked','on' )
    end
end
%
% Profile pres_adjusted - pres
function callback_prof_dP( hObject, eventdata )
    if(isempty(dd.dataRaw)||isempty(dd.dataAdj))
        warndlg('\fontsize {16} No raw and adjusted datasets!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        dP = dd.dataAdj.PRES-dd.dataRaw.PRES;
        plotProf( dd.dataStr.PRES, dP, 'plotAxes',hh.hax1,...
            'xLab','pres\_adjusted - pres (dbar)','mtime',dd.trajTbl.mtime,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'fontSize',ff.fontSize, 'colormap',ff.colormap,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'yLim',ff.prof.presLim, 'xLim',ff.prof.presDiffLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','prof_dP' ), 'Checked','on' )
        
    end
end
%
% Profile temp_adjusted - temp
function callback_prof_dT( hObject, eventdata )
    if(isempty(dd.dataRaw)||isempty(dd.dataAdj))
        warndlg('\fontsize {16} No raw and adjusted datasets!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        dT = dd.dataAdj.TEMP-dd.dataRaw.TEMP;
        plotProf( dd.dataStr.PRES, dT, 'plotAxes',hh.hax1,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'xLab','temp\_adjusted - temp (^oC)','mtime',dd.trajTbl.mtime,...
            'fontSize',ff.fontSize, 'colormap',ff.colormap,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'yLim',ff.prof.presLim, 'xLim',ff.prof.tempDiffLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','prof_dT' ), 'Checked','on' )
    end
end
%
% Profile psal_adjusted - psal
function callback_prof_dS( hObject, eventdata )
    if(isempty(dd.dataRaw)||isempty(dd.dataAdj))
        warndlg('\fontsize {16} No raw and adjusted datasets!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        dS = dd.dataAdj.SAL-dd.dataRaw.SAL;
        plotProf( dd.dataStr.PRES, dS, 'plotAxes',hh.hax1,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'xLab','psal\_adjusted - psal','mtime',dd.trajTbl.mtime,...
            'fontSize',ff.fontSize, 'colormap',ff.colormap,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'yLim',ff.prof.presLim, 'xLim',ff.prof.salDiffLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','prof_dS' ), 'Checked','on' )
    end
end
%
%   Theta-S with QC
function callback_theta_S_QC( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.TEMP(:)))==0)||...
            (sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotTSQC(dd.dataStr, 'plotAxes',hh.hax1,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'fontSize',ff.fontSize, 'colormap',ff.colormap,...
            'legendFontSize',ff.legendFontSize,...
            'ptLim',ff.prof.tempLim, 'sLim',ff.prof.salLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','theta_S_QC' ), 'Checked','on' )
    end
end
%
%   Profile max depth
function callback_profile_max_depth( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)&&(sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} PRES and SAL contain only NaN',...
                'No data', warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotTimeBar( dd.trajTbl.mtime, dd.trajTbl.maxPRESwPSAL,'plotAxes',hh.hax1,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'yDir','rev', 'yLab','Maximum PRES with valid SAL',...
            'fontSize',ff.fontSize, 'xLim',[ff.time.startTime ff.time.endTime],...
            'timeTickStyle',ff.time.timeTickStyle, 'timeTickInterval',ff.time.timeTickInterval,...
            'yLim',ff.time.maxPRESwPSALlim );
        menu_figure_uncheck()
        set( findobj( 'Tag','profile_max_depth' ), 'Checked','on' )
    end
end
%
%   Number of valid data in profiles
function callback_data_profiles( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)&&(sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} PRES and SAL contain only NaN',...
                'No data', warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotTimeBar( dd.trajTbl.mtime, dd.trajTbl.nvPSAL,'plotAxes',hh.hax1,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'yLab','N of valid SAL in profile', 'fontSize',ff.fontSize,...
            'xLim',[ff.time.startTime ff.time.endTime],...
            'timeTickStyle',ff.time.timeTickStyle, 'timeTickInterval',ff.time.timeTickInterval,...
            'yLim',ff.time.nvPSALlim );
        menu_figure_uncheck()
        set( findobj( 'Tag','data_profiles' ), 'Checked','on' )
    end
end
%
%   Export the plot to png file
function callback_export_plot( hObject, eventdata )
    [save_fn,save_path] = uiputfile( '*.png' );
    if( save_fn )
        imageData = screencapture( hh.hpax1 );       
        imwrite( imageData,  [ save_path, save_fn ] )
    end
end
%
%   Open the plot in new figure
function callback_new_figure( hObject, eventdata )
    h_new_fig = figure( 'Position',[200 200 672 560] );
    copyobj( hh.hax1, h_new_fig ) 
end
%
%   Show metadata
function callback_metadata( hObject, eventdata )
    kProf = 1;
    tbl_cw = [30,25,40];
    cycle_cell = cellstr(repmat(' ',dd.nProf,1));
    for k_cycle = 1:dd.nProf
        cycle_cell(k_cycle) = cellstr(sprintf( '%3d-%s %s\n',k_cycle,...
            dd.trajTbl.data_mode(k_cycle,:), datestr(dd.trajTbl.mtime(k_cycle),1) ));
    end
    hFigModal = figure('WindowStyle','modal',...
        'Units','normalized','Position',[0.05,0.1,0.9,0.8],...
        'Name','Metadata', 'NumberTitle','off');
    hpum_cycle = uicontrol( hFigModal, 'Style','popupmenu',...
        'Units','normalized','Position',[0.2 0.88 0.3 0.1],...
        'FontSize',16, 'String',cycle_cell, 'Value',kProf,...
        'Callback',@callback_cycle_meta );
    uitable_metadata = uitable( 'Parent',hFigModal,...
        'Units','normalized','Position',[0.02,0.12,0.96,0.8],...
        'FontSize',16, 'RowName',[], 'ColumnWidth',{60,340,300,450},...
        'ColumnEditable',[false,true,true,true], 'FontName','FixedWidth',...
        'Visible','on', 'ColumnName',{'',[fsp1,'Equation'],...
        [fsp1,'Coefficients'],[fsp1,'Comment']});
    metadata_cell = set_metadata_cell( dd.metaPar, kProf, tbl_cw ); % metadata of the 1-st profile
    uitable_metadata.Data = metadata_cell;
    uicontrol( 'Parent',hFigModal,'Style','pushbutton',...
        'Units','normalized','Position',[0.2,0.03,0.15,0.07],...
        'String','Export metadata', 'FontSize',16,...
        'Callback',@text_export_meta );
    % Nested functions
    %   Creates and fills the cell with metadata
    function metadata_cell = set_metadata_cell( metaPar, kProf, tbl_cw )
        %
        %     indx_prof = find( argo.mtime == mtime(k_prof) );
        parameter = metaPar(kProf).parameter;
        n_par = size( parameter,1 );
        %
        calib_eq = metaPar(kProf).scientific_calib_equation;
        calib_coef = metaPar(kProf).scientific_calib_coefficient;
        calib_comm = metaPar(kProf).scientific_calib_comment;
        nr_par = zeros(n_par,3); % The number of rows
        for k_par = 1:n_par
            A1 = calib_eq{k_par};
            str1 = reshape([A1(:);repmat(' ',mod(-numel(A1),tbl_cw(1)),1)],tbl_cw(1),[])';
            A2 = calib_coef{k_par};
            str2 = reshape([A2(:);repmat(' ',mod(-numel(A2),tbl_cw(2)),1)],tbl_cw(2),[])';
            A3 = calib_comm{k_par};
            str3 = reshape([A3(:);repmat(' ',mod(-numel(A3),tbl_cw(3)),1)],tbl_cw(3),[])';
            nr_par(k_par,1) = size(str1,1);
            nr_par(k_par,2) = size(str2,1);
            nr_par(k_par,3) = size(str3,1);
        end
        nr_par_max = max(nr_par,[],2);
        nr_cum = cumsum(nr_par_max);
        row_lims = [ [1;nr_cum(1:end-1)+1], nr_cum ];
        metadata_cell = cell(sum(nr_par_max),4);
        %
        for k_par = 1:n_par
            metadata_cell{row_lims(k_par),1} = parameter{k_par};
            A1 = calib_eq{k_par};
            str1 = cellstr(reshape([A1(:);repmat(' ',mod(-numel(A1),tbl_cw(1)),1)],tbl_cw(1),[])');
            for kk = 1:size(str1,1)
                metadata_cell{ row_lims(k_par,1)+kk-1, 2 } = str1{kk};
            end
            A2 = calib_coef{k_par};
            str2 = cellstr(reshape([A2(:);repmat(' ',mod(-numel(A2),tbl_cw(2)),1)],tbl_cw(2),[])');
            for kk = 1:size(str2,1)
                metadata_cell{ row_lims(k_par,1)+kk-1, 3 } = str2{kk};
            end
            A3 = calib_comm{k_par};
            str3 = cellstr(reshape([A3(:);repmat(' ',mod(-numel(A3),tbl_cw(3)),1)],tbl_cw(3),[])');
            for kk = 1:size(str3,1)
                metadata_cell{ row_lims(k_par,1)+kk-1, 4 } = str3{kk};
            end
        end
%
    end
    %     
    function callback_cycle_meta( hObject, eventdata )
        kProf = hpum_cycle.Value;
        metadata_cell = set_metadata_cell( dd.metaPar, kProf, tbl_cw );
        uitable_metadata.Data = metadata_cell;
    end
    %
    function text_export_meta( hObject, eventdata )
        cd( ss.paths.ncDir )
        [save_fn,save_path] = uiputfile( '*.txt' );
        if( save_fn )
            fid = fopen( [ save_path, save_fn ], 'w' );
            fprintf( fid, 'Profile: %s\n', cycle_cell{kProf} );
            for kk = 1:size(dd.metaPar(kProf).parameter,1)
                fprintf( fid, 'Parameter: %s\n', dd.metaPar(kProf).parameter{kk} );
                fprintf( fid, 'Equation: %s\n', dd.metaPar(kProf).scientific_calib_equation{kk} );
                fprintf( fid, 'Coefficient: %s\n', dd.metaPar(kProf).scientific_calib_coefficient{kk} );
                fprintf( fid, 'Comment: %s\n', dd.metaPar(kProf).scientific_calib_comment{kk} );
                fprintf( fid, '----------------\n' );
            end
            fclose(fid);
        end
    end
    uicontrol( 'Parent',hFigModal, 'Style','pushbutton', ...
            'Units','normalized','Position',[0.6,0.03,0.10,0.07],...
            'String','Close', 'FontSize',16,...
            'Callback',@close_hfig_meta );
    function close_hfig_meta( hObject, eventdata )
        close(hFigModal)
    end
end
%
%   Show the table of missing values
function callback_missing_values( hObject, eventdata )
    hFigModal = figure('Position',[200,300,550,300],'WindowStyle','modal',...
            'Name','Missing values', 'NumberTitle','off');
    misValTbl = calcMisValTbl( dd.dataStr );
    missing_values_cell = table2cell(misValTbl);
    missing_values_cell = [misValTbl.Properties.RowNames,missing_values_cell];
    uitable_miss_val = uitable( 'Parent',hFigModal,'Position',[50,100,425,175],...
        'FontSize',16, 'RowName',[], 'ColumnWidth',{150,75,100,75} );
    uitable_miss_val.Data = missing_values_cell;
    uitable_miss_val.ColumnName = {[fsp1,'Parameter'],...
        [fsp1,'Total'],...
        [fsp1,'Missing'],...
        [fsp1,'%'] };
    uicontrol( 'Parent',hFigModal,'Position',[50,35,100,50],...
            'Style','pushbutton', 'String','Export table', 'FontSize',16,...
            'Callback',@text_export_miss );
    function text_export_miss( hObject, eventdata )
        cd( ss.paths.ncDir )
        [save_fn,save_path] = uiputfile( '*.txt' );
        if( save_fn )
            fid = fopen( [ save_path, save_fn ], 'w' );
            [ nPar, ~ ] = size( misValTbl );
            fprintf( fid, 'Parameter,Missing,Total,%%\n' );
            for k = 1 : nPar
                fprintf( fid, '%s,%d,%d,%.5f\n', misValTbl.Properties.RowNames{k},misValTbl{k,:} );
            end
            fclose(fid);
        end
    end
    uicontrol( 'Parent',hFigModal,'Position',[350,35,100,50],...
            'Style','pushbutton', 'String','Close', 'FontSize',16,...
            'Callback',@close_hfig_miss );
    function close_hfig_miss( hObject, eventdata )
        close(hFigModal)
    end
    %
end
%
%   Select between raw and adjusted datasets
function callback_sel_datasets( hObject, eventdata )
    hFigModal = figure( 'Position',[200,500,350,180],'WindowStyle','modal',...
            'Name','Select dataset', 'NumberTitle','off');
    hbg_dat = uibuttongroup( hFigModal, 'Position',[ 0.05 0.35 0.9 0.6 ],...
        'Title','Select dataset', 'FontSize',20 );
    hrb_raw = uicontrol( hbg_dat,'Style','radiobutton','FontSize',16,...
                  'String','raw',...
                  'Position',[50 45 200 30],...
                   'Value',strcmp(dd.datasetsSel,'raw') );
    hrb_adj = uicontrol( hbg_dat,'Style','radiobutton','FontSize',16,...
                  'String','adjusted',...
                  'Position',[50 15 200 30],...
                  'Value',strcmp(dd.datasetsSel,'adjusted'));
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Select',...
        'Position',[50 20 100 30],...
        'FontSize',16, 'Callback',@callback_accept_dat );        
    function callback_accept_dat( hObject, eventdata )
        if( hrb_raw.Value )
            dd.datasetsSel = 'raw';
            dd.dataStr = dd.dataRaw;
        elseif( hrb_adj.Value )
            dd.datasetsSel = 'adjusted';
            dd.dataStr = dd.dataAdj;
        end
        close( hFigModal )
        hh.staticText_datasets.String = ['DATASETS: ',dd.datasetsSel];
        % Data mode table
        hh.uitable_data_mode.Data = setDataModeCell(dd.trajTbl);
        hh.uitable_data_mode.ColumnName = { [fsp1,'Mode'],...
                [fsp1,'N'],[fsp1,'Start'],[fsp1,'End'] };
        hh.uitable_data_mode.RowName = [];
        % Max depth
        PRES1 = dd.dataStr.PRES;
        PRES1(isnan(dd.dataStr.SAL)) = NaN;
        dd.trajTbl.maxPRESwPSAL = max(PRES1)';
        % The number of profiles
        dd.trajTbl.nvPSAL = sum(isfinite(dd.dataStr.SAL))';
    end
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Cancel',...
        'Position',[200 20 100 30],...
        'FontSize',16, 'Callback',@callback_cancel_dat );  
    function callback_cancel_dat( hObject, eventdata )
        close( hFigModal )
    end
end
%
%   Select points based on QC codes
function callback_sel_QC( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.TEMP(:)))==0)||...
            (sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotTSQC(dd.dataStr, 'plotAxes',hh.hax1, 'title',ff.title);
        menu_figure_uncheck()
        set( findobj( 'Tag','theta_S_QC' ), 'Checked','on' )
        %
        QCtbl = calcQcTbl( dd.dataStr.SAL_QC );
        hFigModal = figure( 'Position',[200,350,600,400],'WindowStyle','modal',...
            'Name','Select QC', 'NumberTitle','off');
        uitable_QC = uitable( hFigModal, 'Position',[20 100 550 250], 'FontSize',16, 'Visible','on' );
        uitable_QC.Data = table2cell(QCtbl);    
        uitable_QC.ColumnName = {[fsp1,'QC code'],[fsp1,'.    meaning    .'],...
            [fsp1,'Number of points'],[fsp1,'Selected']};
        uitable_QC.ColumnEditable = [ false, false, false, true ];
        uitable_QC.ColumnWidth = {100,200,100,100 };
        %
        uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
            'String','Select', 'Position',[50, 25, 100, 30],...
            'Callback',@callback_QC_select );
        uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
            'String','Cancel', 'Position',[200, 25, 100, 30],...
            'Callback',@callback_QC_cancel );
    end
    function callback_QC_select( hObject, eventdata )
        % Fill with NaNs data with unselected codes
        QCtbl.sel = [uitable_QC.Data{:,4}]';
        close( hFigModal )
        dd.dataStr = dataStrQc2nan( dd.dataStr, 'SAL_QC', QCtbl.code(~QCtbl.sel) );
        if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.TEMP(:)))==0)||...
            (sum(isfinite(dd.dataStr.SAL(:)))==0))
                warndlg('\fontsize {16} No data!','No data',warndlg_opts);
        else
            delete(findobj(hh.hpax1, 'type', 'axes'))
            hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
            plotTSQC(dd.dataStr, 'plotAxes',hh.hax1, 'title',ff.title);
        end
    end
    function callback_QC_cancel( hObject, eventdata )
        close( hFigModal )
    end
end
%
%   Select data points in rectangle at TS diagram
function callback_sel_rect( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.TEMP(:)))==0)||...
            (sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotTSQC(dd.dataStr, 'plotAxes',hh.hax1, 'title',ff.title);
        menu_figure_uncheck()
        set( findobj( 'Tag','theta_S_QC' ), 'Checked','on' )
        % Switch all menu to 'off'
        for k_obj = 1:height(menuMtbl)
            set( findobj( 'Tag',menuMtbl.Tag{k_obj} ), 'Enable','off' )
        end
        xLim = get(hh.hax1,'XLim');
        yLim = get(hh.hax1,'YLim');
        F = getframe(hh.hax1);
        imageData = frame2im(F);
        cla(hh.hax1)
        image(xLim,yLim,flip(imageData),'Parent',hh.hax1)
        [x_rect,y_rect] = ginput(2);
        for k_obj = 1:height(menuMtbl)
            set( findobj( 'Tag',menuMtbl.Tag{k_obj} ), 'Enable','on' )
        end
        hold(hh.hax1,'on')
        plot(hh.hax1,x_rect([1,2,2,1,1]),y_rect([1,1,2,2,1]),'Marker','o','MarkerSize',20,...
            'Color','k', 'MarkerEdgeColor','k', 'MarkerFaceColor',[0.9 0.9 0.9])
        hold(hh.hax1,'off')
        set( findobj( 'Tag','Remove_rect' ), 'Enable','on' )
    end
end
%
%   Remove data points from selected rectangle
function callback_rem_rect( hObject, eventdata )
    indx_rem = ( dd.dataStr.SAL >= min(x_rect) ) & ( dd.dataStr.SAL <= max(x_rect) ) &...
        ( dd.dataStr.PTMP >= min(y_rect) ) & ( dd.dataStr.PTMP <= max(y_rect) );
    dd.dataStr.SAL(indx_rem) = NaN;
    dd.dataStr.TEMP(indx_rem) = NaN;
    dd.dataStr.PRES(indx_rem) = NaN;
    dd.dataStr.PTMP(indx_rem) = NaN;
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    plotTSQC(dd.dataStr, 'plotAxes',hh.hax1, 'title',ff.title);
    set( findobj( 'Tag','Remove_rect' ), 'Enable','off' )
end
%
%   Select profiles
function callback_sel_prof( hObject, eventdata )
    hFigModal = figure( 'Position',[300,250,500,400],'WindowStyle','modal',...
          'Name','Select profiles', 'NumberTitle','off');
    %
    hcb_Pmin = uicontrol( hFigModal,'Style','checkbox','FontSize',16,...
        'String','Minimum PRES with SAL ', 'Position',[25 325 250 30],'Value',0,...
        'Callback',@callback_cbPmin );
    Pmin = min(dd.trajTbl.maxPRESwPSAL);
    hed_Pmin = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',num2str(Pmin), 'Position',[325,325,100,30],'Callback',@callback_Pmin,...
        'Enable','off');
    %
    hcb_Nmin = uicontrol( hFigModal,'Style','checkbox','FontSize',16,...
        'String','Minimum number of SAL ', 'Position',[25 275 250 30],'Value',0,...
        'Callback',@callback_cbNmin );
    Nmin = 1;
    hed_Nmin = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',num2str(Nmin), 'Position',[325,275,100,30],'Callback',@callback_Nmin,...
        'Enable','off');
    %
    hcb_Date_start = uicontrol( hFigModal,'Style','checkbox','FontSize',16,...
        'String','Start date (dd/mm/yyyy)', 'Position',[25 225 250 30],'Value',0,...
        'Callback',@callback_cbDateStart );
    Date_start = dd.trajTbl.mtime(1);
    hed_Date_start = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',datestr(Date_start,24), 'Position',[325,225,100,30],...
        'Callback',@callback_Date_start, 'Enable','off' );
        %
    hcb_Date_end = uicontrol( hFigModal,'Style','checkbox','FontSize',16,...
        'String','End date (dd/mm/yyyy)', 'Position',[25 175 250 30],'Value',0,...
        'Callback',@callback_cbDateEnd );
    Date_end = dd.trajTbl.mtime(end);
    hed_Date_end = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',datestr(Date_end,24), 'Position',[325,175,100,30], 'Enable','off',...
        'Callback',@callback_Date_end );
        %
    uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
        'String','Select profiles by data_mode', 'Position',[25, 125, 225, 30],...
        'Callback',@callback_sel_prof_datamode );
    data_mode_cell = setDataModeCell(dd.trajTbl);
    datamode_str = set_datamode_str(data_mode_cell); 
    uitext_prof_sel_datamode = uicontrol( hFigModal, 'Style','text',...
            'Position',[275 130 200 20], 'FontSize',16, 'HorizontalAlignment','left',...
            'String',datamode_str );
%        
    uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
            'String','Select profiles from table', 'Position',[25, 75, 225, 30],...
            'Callback',@callback_sel_prof_tbl );
    uitext_prof_sel_tbl = uicontrol( hFigModal, 'Style','text',...
            'Position',[275 80 100 20], 'FontSize',16, 'HorizontalAlignment','left',...
            'String',[num2str(dd.nProf),' of ',num2str(dd.nProf)] );
    indx_sel_tbl = true( height(dd.trajTbl), 1 );
    uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
            'String','Select', 'Position',[100, 25, 100, 30],...
            'Callback',@callback_prof_select );
    uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
            'String','Cancel', 'Position',[300, 25, 100, 30],...
            'Callback',@callback_prof_cancel );

        %
        
    % Nested functions
    function callback_cbPmin( hObject, eventdata )
        if(hcb_Pmin.Value)
            hed_Pmin.Enable = 'on';
        else
            hed_Pmin.Enable = 'off';
        end
    end
    function callback_Pmin( hObject, eventdata )
        Pmin = str2double( hed_Pmin.String );
        if( isnan(Pmin) )
            Pmin = min(dd.trajTbl.maxPRESwPSAL);
            hed_Pmin.String = num2str(Pmin);
        end
    end
    function callback_cbNmin( hObject, eventdata )
        if(hcb_Nmin.Value)
            hed_Nmin.Enable = 'on';
        else
            hed_Nmin.Enable = 'off';
        end
    end
    function callback_Nmin( hObject, eventdata )
        Nmin = str2double( hed_Nmin.String );
        if( isnan(Nmin) )
            Nmin = 1;
            hed_Nmin.String = num2str(Nmin);
        end
    end
    function callback_cbDateStart( hObject, eventdata )
        if(hcb_Date_start.Value)
            hed_Date_start.Enable = 'on';
        else
            hed_Date_start.Enable = 'off';
        end
    end
    function callback_cbDateEnd( hObject, eventdata )
        if(hcb_Date_end.Value)
            hed_Date_end.Enable = 'on';
        else
            hed_Date_end.Enable = 'off';
        end
    end
    dataModeSel = {'D','A','R'};
    function datamode_str = set_datamode_str(data_mode_cellInp)
        datamode_str = '';
        for k_mode = 1:3
            datamode_str = [ datamode_str, data_mode_cellInp{k_mode,1},...
                '(',num2str(data_mode_cellInp{k_mode,2}),') '];
        end
    end
    function callback_sel_prof_datamode( hObject, eventdata )
        hfig_sel_prof_datamode = figure( 'Position',[300,300,400,300],'WindowStyle','modal',...
             'Name','Select profiles', 'NumberTitle','off');
        uitable_sel_prof_datamode = uitable( hfig_sel_prof_datamode,...
            'Position',[20 70 360 220],...
            'FontSize',16, 'Visible','on' );
        uicontrol( hfig_sel_prof_datamode,'Style','pushbutton','FontSize',16,...
            'String','Select', 'Position',[100, 25, 100, 30],...
            'Callback',@callback_prof_datamode_select );
        uicontrol( hfig_sel_prof_datamode,'Style','pushbutton','FontSize',16,...
            'String','Cancel', 'Position',[250, 25, 100, 30],...
            'Callback',@callback_prof_datamode_cancel );
    %
        function callback_prof_datamode_select( hObject, eventdata )
            sel_prof_datamode = uitable_sel_prof_datamode.Data;
            close( hfig_sel_prof_datamode )
            indx_sel_datamode = [sel_prof_datamode{:,3}];
            dataModeSel = sel_prof_datamode(indx_sel_datamode,1);
            data_mode_cell1 = setDataModeCell(dd.trajTbl);
            data_mode_cell1(~indx_sel_datamode,2) = {0};
            datamode_str = set_datamode_str( data_mode_cell1 );
            uitext_prof_sel_datamode.String = datamode_str;
        end
        %
        function callback_prof_datamode_cancel( hObject, eventdata )
            close( hfig_sel_prof_datamode )
        end
        uitable_sel_prof_datamode.Data = set_datamode_sel_cell( data_mode_cell );
        uitable_sel_prof_datamode.ColumnName = {'data mode','N'};
        uitable_sel_prof_datamode.ColumnEditable = [ false, false, true ];
    end
    function datamode_sel_cell = set_datamode_sel_cell( hObject, eventdata )
        tbl_temp = cell2table( data_mode_cell(:,1:2) );
        tbl_temp.selected = true( height(tbl_temp),1 );
        datamode_sel_cell = table2cell( tbl_temp );
    end
    
    function callback_sel_prof_tbl( hObject, eventdata )
        hfig_sel_prof_tbl = figure( 'Position',[100,200,600,700],'WindowStyle','modal',...
            'Name','Select profiles', 'NumberTitle','off');
        uitable_sel_prof_tbl = uitable( hfig_sel_prof_tbl,...
            'Position',[20 70 560 620],...
            'FontSize',14, 'Visible','on', ...
            'ColumnWidth',{60,120,80,80,80,80},...
            'ColumnFormat',{'char','char',[],[]} );
        uicontrol( hfig_sel_prof_tbl,'Style','pushbutton','FontSize',16,...
            'String','Select', 'Position',[100, 25, 100, 30],...
            'Callback',@callback_prof_tbl_select );
        uicontrol( hfig_sel_prof_tbl,'Style','pushbutton','FontSize',16,...
            'String','Cancel', 'Position',[350, 25, 100, 30],...
            'Callback',@callback_prof_tbl_cancel );
        %
        uitable_sel_prof_tbl.Data = set_atbl_cell( dd.trajTbl );
        uitable_sel_prof_tbl.ColumnName = {'cycle','Date','data_mode',...
            'pres_max','Nsal','Selected'};
        uitable_sel_prof_tbl.ColumnEditable = [ false, false, false, false, false, true ];
        %
        function atbl_cell = set_atbl_cell( trajTbl )
            tbl_temp = table( trajTbl.cycle_number, 'VariableNames',{'cycle_number'});
            tbl_temp.Date = datestr( trajTbl.mtime, 1 );
            tbl_temp.data_mode = trajTbl.data_mode;
            tbl_temp.max_pres = trajTbl.maxPRESwPSAL;
            tbl_temp.Nsal = trajTbl.nvPSAL;
            indx_sel_tbl = true( height(trajTbl),1 );        
            indx_sel_tbl2 = indx_sel_tbl_add(indx_sel_tbl); % Add to indx_sel_tbl checked filters
            tbl_temp.selected = indx_sel_tbl2;
            atbl_cell = table2cell( tbl_temp );
        end
        function callback_prof_tbl_select( hObject, eventdata )
            sel_prof_tbl = uitable_sel_prof_tbl.Data;
            indx_sel_tbl = cell2mat(sel_prof_tbl(:,6));
            n_prof_sel = sum( indx_sel_tbl );
            uitext_prof_sel_tbl.String = [num2str(n_prof_sel),' of ',num2str(dd.nProf)];
            close( hfig_sel_prof_tbl )
        end
        %
        function callback_prof_tbl_cancel( hObject, eventdata )
            close( hfig_sel_prof_tbl )
        end
    end
    
    function callback_prof_select( hObject, eventdata )
        indx_sel_tbl2 = indx_sel_tbl_add(indx_sel_tbl); % Add to indx_sel_tbl checked filters
        close( hFigModal )
        [dd.trajTbl,dd.dataStr,dd.metaPar] = dataProfSel(dd.trajTbl,dd.dataStr,dd.metaPar,indx_sel_tbl2);
        nProf = height(dd.trajTbl);
        if(nProf~=dd.nProf)
            dd.nProfChanged = true;
            set( findobj( 'Tag','Select_data' ), 'Enable','off' )
            dd.nProf = nProf;
        end
        % The string with profiles and layers
        argoid_str = [ 'Argo # ', dd.trajTbl.platform_number{1},' / ',...
                num2str( dd.nProf ),' profiles / ',...
                num2str( dd.nLayers ),' layers' ];
        set(hh.staticText_argoid,'String', argoid_str );
        % Map
        delete(findobj(hh.hpax1, 'type','axes'))
%         dd.title = dd.trajTbl.platform_number{1};
        hh.hax1 = axes('Parent',hh.hpax1, 'Units','normalized', 'Position',axPos);
        plotTrajMap(dd.trajTbl, 'plotAxes',hh.hax1,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
            'colormap',ff.colormap,...
            'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
            'axesFontSize',ff.map.axesFontSize,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize );
        set( findobj( 'Tag','Map' ), 'Checked','on' )
        hh.uitable_data_mode.Data = setDataModeCell(dd.trajTbl);
        hh.uitable_data_mode.ColumnName = {...
            [fsp1,'Mode'],[fsp1,'N'],[fsp1,'Start'],[fsp1,'End'] };
        hh.uitable_data_mode.RowName = [];
    end
    function indx_sel_tblOut = indx_sel_tbl_add(indx_sel_tblIn)
        indx_sel_tblOut = indx_sel_tblIn;
        if(hcb_Pmin.Value)
            vPmin = str2double( hed_Pmin.String );
            indx_sel_tblOut = indx_sel_tblOut & (dd.trajTbl.maxPRESwPSAL>=vPmin);
        end
        if(hcb_Nmin.Value)
            vNmin = str2double( hed_Nmin.String );
            indx_sel_tblOut = indx_sel_tblOut & (dd.trajTbl.nvPSAL>=vNmin);
        end
        if(hcb_Date_start.Value)
            vDate_start = datenum( hed_Date_start.String,'dd/mm/yyyy' );
            indx_sel_tblOut = indx_sel_tblOut & (dd.trajTbl.mtime>=vDate_start);
        end
        if(hcb_Date_end.Value)
            vDate_end = datenum( hed_Date_end.String,'dd/mm/yyyy' );
            indx_sel_tblOut = indx_sel_tblOut & (dd.trajTbl.mtime<=vDate_end);
        end
        indx_sel_tblOut = indx_sel_tblOut & ismember(dd.trajTbl.data_mode,dataModeSel);
    end
    function callback_prof_cancel( hObject, eventdata )
        close( hFigModal )
    end
end
%
function callback_save2mat( hObject, eventdata )
    stn_with_data = logical(sum(isfinite(dd.dataStr.SAL))) &...
    logical(sum(isfinite(dd.dataStr.TEMP))) &...
    logical(sum(isfinite(dd.dataStr.PRES))) &...
    logical(sum(isfinite(dd.dataStr.PTMP)));
    stn_with_data = stn_with_data & ~isnan(dd.trajTbl.latitude)' & ~isnan(dd.trajTbl.longitude)';
    if( length(stn_with_data) ~= sum(stn_with_data) ) % Remove empty profiles
        options.Default = 'Yes';
        options.Interpreter = 'tex';
        quest = ['\fontsize{16} ', num2str(length(stn_with_data)-sum(stn_with_data)),' of ',...
            num2str(length(stn_with_data)),' profiles are empty/corrupted. Remove?'];
        answer = questdlg(quest,'',[fsp1,'Yes'],[fsp1,'No'],options);
        if( strcmp( answer, [fsp1,'Yes'] ) ) % Remove empty profiles
            [dd.trajTbl,dd.dataStr,dd.metaPar] = dataProfSel(dd.trajTbl,dd.dataStr,dd.metaPar,stn_with_data);
        end
    end
    if( sum(isfinite(dd.dataStr.PRES(:))) == 0 )
            warndlg('\fontsize {16} PRES contain only NaN',...
                   'No data', warndlg_opts);
    elseif( sum(isfinite(dd.dataStr.SAL(:))) == 0 )
            warndlg('\fontsize {16} SAL contain only NaN',...
                   'No data', warndlg_opts);
    elseif( sum(isfinite(dd.dataStr.TEMP(:))) == 0 )
            warndlg('\fontsize {16} TEMP contain only NaN',...
                   'No data', warndlg_opts);
    elseif( sum(isfinite(dd.dataStr.PTMP(:))) == 0 )
            warndlg('\fontsize {16} TEMP contain only NaN',...
                   'No data', warndlg_opts);
    else
        hFigModal = figure( 'Units','normalized','WindowStyle','modal',...
            'Position',[0.25,0.3,0.4,0.4], 'Name','Save to .mat file',...
            'NumberTitle','off');
        hcb_save_interp = uicontrol( hFigModal,'Style','checkbox',...
                'FontSize',16, 'Value',0,...
                'Units','normalized', 'Position',[0.1 0.7 0.8 0.1],...
                'String','Interpolate to pressure levels (e.g., [1,10:10:2000])',...
                'Callback',@callback_cb_interp );
        interp_str = ['[',num2str(ceil(min(dd.dataStr.PRES(:)))),',',...
                num2str( ceil(min(dd.dataStr.PRES(:))/10)*10 ), ':10:'...
                num2str(floor(max(dd.dataStr.PRES(:)))),']'];
        hed_interp = uicontrol( hFigModal,'Style','edit','FontSize',16,...
                'Units','normalized', 'Position',[0.1 0.5 0.8 0.1],...
                'String',interp_str, 'Enable','off','Callback',@callback_interp_str );
        uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
                'String','Save', 'Units','normalized','Position',[0.2, 0.1, 0.2, 0.12],...
                'Callback',@callback_save );
        uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
                'String','Cancel', 'Units','normalized','Position',[0.6, 0.1, 0.2, 0.12],...
                'Callback',@callback_save_cancel );
    end
    function callback_cb_interp( hObject, eventdata )
        if( hcb_save_interp.Value )
            hed_interp.Enable = 'on';
        else
            hed_interp.Enable = 'off';
        end
    end
    function callback_interp_str( hObject, eventdata )
        try
            unique( eval( hed_interp.String ) );
        catch
            hed_interp.String = interp_str;
        end
    end
    function callback_save( hObject, eventdata )
        if( hcb_save_interp.Value )
            pres_int = unique( eval( hed_interp.String ) );
            PRES1 = repmat( pres_int', 1, dd.nProf );
            SAL1 = NaN(size(PRES1));
            TEMP1 = NaN(size(PRES1));
            PTMP1 = NaN(size(PRES1));
            for k_cycle = 1:dd.nProf
                indx_fin = isfinite(dd.dataStr.PRES(:,k_cycle));
                if(sum(indx_fin)>1)
                    [PRESuniqe,indxP] = unique(dd.dataStr.PRES(indx_fin,k_cycle)); 
                    SALunique = dd.dataStr.SAL(indx_fin,k_cycle);
                    SALunique = SALunique(indxP);
                    SAL1(:,k_cycle) = interp1(PRESuniqe,SALunique,PRES1(:,k_cycle));
                    TEMPunique = dd.dataStr.TEMP(indx_fin,k_cycle);
                    TEMPunique = TEMPunique(indxP);
                    TEMP1(:,k_cycle) = interp1(PRESuniqe,TEMPunique,PRES1(:,k_cycle));
                    PTMPunique = dd.dataStr.PTMP(indx_fin,k_cycle);
                    PTMPunique = PTMPunique(indxP);
                    PTMP1(:,k_cycle) = interp1(PRESuniqe,PTMPunique,PRES1(:,k_cycle));
                end
            end
            dataStr4save = struct('PRES',PRES1,'TEMP',TEMP1,'SAL',SAL1,'PTMP',PTMP1);
        else
            dataStr4save = dd.dataStr;
        end
        close( hFigModal )
        % 
        cd( ss.paths.matDir )
        [save_fn,save_path] = uiputfile( [ dd.trajTbl.platform_number{1},'.mat' ] );
        if( save_fn ~= 0 )
            save2OWCmat( [save_path,filesep,save_fn], dd.trajTbl, dataStr4save );
        end
    end
    function callback_save_cancel( hObject, eventdata )
        close( hFigModal )
    end
end
%
%   Other nested functions
function menu_figure_uncheck()
    for kmF1 = 1:height(menuFtbl)
        set( findobj( 'Tag',menuFtbl.Tag{kmF1} ), 'Checked','off' )
    end
end
%
function dataModeCell = setDataModeCell(trajTbl)
    dataModeCell = { 'D',0,'',''; 'A',0,'',''; 'R',0,'','' };
    for kMode = 1:3
        indxSel = strcmpi(dataModeCell{kMode,1}, cellstr(trajTbl.data_mode) );
        dataModeCell{kMode,2} = sum(indxSel);
        startTime = min(trajTbl.mtime(indxSel));
        if( isempty(startTime) )
            dataModeCell{kMode,3} = '';
        else
            dataModeCell{kMode,3} = datestr(startTime,1);
        end
        endTime = max( trajTbl.mtime(indxSel) );
        if( isempty(endTime) )
            dataModeCell{kMode,4} = '';
        else
            dataModeCell{kMode,4} = datestr(endTime,1);
        end
    end
end
%
% Figure properties
function callback_fig_properties( hObject, eventdata )
    hFigModal = figure( 'Position',[200,200,400,500],'WindowStyle','modal',...
            'Name','Map properties', 'NumberTitle','off');
    uicontrol( hFigModal, 'Style','text',...
        'Position',[20 450 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Title' );
    hed_argo_title = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',ff.title, 'Position',[70,455,300,30] );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 410 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Title font size' );
    hed_titleFontSize = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',num2str(ff.titleFontSize), 'Position',[180,415,100,30],...
        'Callback',@callback_numeric );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 370 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Axes font size' );
    hed_axFontSize = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',num2str(ff.fontSize), 'Position',[180,375,100,30],...
        'Callback',@callback_numeric );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 330 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','colormap' );
    kColormap = find(strcmp(colormapList,ff.colormap));
    hpum_colormap = uicontrol( hFigModal, 'Style','popupmenu',...
        'Position',[180 330 150 30],...
        'FontSize',16, 'String',colormapList, 'Value',kColormap );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 290 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Items in legend' );
    hed_nLeg = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',num2str(ff.nLeg), 'Position',[180,295,100,30],...
        'Callback',@callback_numeric );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 250 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Legend font size' );
    hed_legendFontSize = uicontrol( hFigModal,'Style','edit','FontSize',16,...
            'String',num2str(ff.legendFontSize), 'Position',[180,255,100,30],...
            'Callback',@callback_numeric );
   %
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Map',...
        'Position',[100 200 150 30],...
        'FontSize',16, 'Callback',@callback_map_properties );
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Profiles and TS',...
        'Position',[80 150 190 30],...
        'FontSize',16, 'Callback',@callback_profiles_properties );
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Time series',...
        'Position',[100 100 150 30],...
        'FontSize',16, 'Callback',@callback_timeseries_properties );
    %
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Select',...
        'Position',[80 20 100 30],...
        'FontSize',16, 'Callback',@callback_accept_properties );        
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Cancel',...
        'Position',[200 20 100 30],...
        'FontSize',16, 'Callback',@callback_cancel_properties ); 
    function callback_map_properties( hObject, eventdata )
        hFigMapPr = figure( 'Position',[300,150,350,310],'WindowStyle','modal',...
            'Name','Map properties', 'NumberTitle','off');
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 260 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','latLim' );
        hed_latLim = uicontrol( hFigMapPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.map.latLim), 'Position',[100,265,120,30],'Callback',@callback_is2lims );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 220 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','lonLim' );
        hed_lonLim = uicontrol( hFigMapPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.map.lonLim), 'Position',[100,225,120,30],'Callback',@callback_is2lims );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 180 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','landcolor' );
        hed_landcolor = uicontrol( hFigMapPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.map.landcolor), 'Position',[120,185,200,30],...
            'Callback',@callback_landcolor );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 140 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Coastline resolution' );
        kGSHHS = find(strncmp({GSHHSlist{1,:}},ff.map.GSHHS,1));
        hpum_GSHHS = uicontrol( hFigMapPr, 'Style','popupmenu',...
            'Position',[180 140 150 30],...
            'FontSize',16, 'String',{GSHHSlist{2,:}}, 'Value',kGSHHS );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 100 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Map axes font size' );
        hed_mAxFontSize = uicontrol( hFigMapPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.map.axesFontSize), 'Position',[180,105,100,30],...
            'Callback',@callback_numeric );
        %
        uicontrol( hFigMapPr, 'Style','pushbutton', 'String','Select',...
            'Position',[50 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_map_properties );        
        uicontrol( hFigMapPr, 'Style','pushbutton', 'String','Cancel',...
            'Position',[200 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_map_properties ); 
        function callback_accept_map_properties( hObject, eventdata )
            ff.map.latLim = str2num(hed_latLim.String);
            ff.map.lonLim = str2num(hed_lonLim.String);
            lcnum = str2num(hed_landcolor.String);
            if(~isempty(lcnum))
                ff.map.landcolor = lcnum;
            else
                ff.map.landcolor = hed_landcolor.String;
            end
            ff.map.GSHHS = GSHHSlist{1,hpum_GSHHS.Value};
            ff.map.axesFontSize = str2double(hed_mAxFontSize.String);
            close( hFigMapPr )
        end
        function callback_cancel_map_properties( hObject, eventdata )
            close( hFigMapPr )
        end
        function callback_landcolor( hObject, eventdata )
            if(~iscolor(hObject.String))
                hObject.String = num2str(ff.map.landcolorDef);
            end
        end
    end
    %
    function callback_profiles_properties( hObject, eventdata )
        hFigPrfPr = figure( 'Position',[300,150,400,310],'WindowStyle','modal',...
            'Name','Profile properties', 'NumberTitle','off');
        %
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 260 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Pressure limits' );
        hed_presLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.prof.presLim), 'Position',[200,265,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 220 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Temperature limits' );
        hed_tempLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.prof.tempLim), 'Position',[200,225,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 180 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Salinity limits' );
        hed_salLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.prof.salLim), 'Position',[200,185,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 140 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Pressure difference limits' );
        hed_presDiffLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.prof.presDiffLim), 'Position',[250,145,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 100 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Temperature difference limits' );
        hed_tempDiffLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.prof.tempDiffLim), 'Position',[250,105,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 60 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Salinity difference limits' );
        hed_salDiffLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.prof.salDiffLim), 'Position',[250,65,120,30],...
            'Callback',@callback_is2lims );
        %
        uicontrol( hFigPrfPr, 'Style','pushbutton', 'String','Select',...
            'Position',[50 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_prof_properties );        
        uicontrol( hFigPrfPr, 'Style','pushbutton', 'String','Cancel',...
            'Position',[200 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_prof_properties ); 
        function callback_accept_prof_properties( hObject, eventdata )
            ff.prof.presLim = str2num(hed_presLim.String);
            ff.prof.tempLim = str2num(hed_tempLim.String);
            ff.prof.salLim = str2num(hed_salLim.String);
            ff.prof.presDiffLim = str2num(hed_presDiffLim.String);
            ff.prof.tempDiffLim = str2num(hed_tempDiffLim.String);
            ff.prof.salDiffLim = str2num(hed_salDiffLim.String);
            close( hFigPrfPr )
        end
        function callback_cancel_prof_properties( hObject, eventdata )
            close( hFigPrfPr )
        end
    end
    %
    function callback_timeseries_properties( hObject, eventdata )
        hFigTsPr = figure( 'Position',[300,150,450,310],'WindowStyle','modal',...
            'Name','Time series properties', 'NumberTitle','off');
        %
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 260 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Start time' );
        hed_Date_start = uicontrol( hFigTsPr,'Style','edit','FontSize',16,...
            'String',datestr(ff.time.startTime,24), 'Position',[150,265,100,30],...
            'Callback',@callback_Date_start, 'Enable','on' );
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 220 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','End time' );
        hed_Date_end = uicontrol( hFigTsPr,'Style','edit','FontSize',16,...
            'String',datestr(ff.time.endTime,24), 'Position',[150,225,100,30],...
            'Callback',@callback_Date_end, 'Enable','on' );
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 180 150 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','timeTickStyle' );
        kTimeTickStyle = find(strcmp(timeTicksList,ff.time.timeTickStyle));
        hpum_TimeTickStyle = uicontrol( hFigTsPr, 'Style','popupmenu',...
            'Position',[180 180 150 30],...
            'FontSize',16, 'String',timeTicksList, 'Value',kTimeTickStyle );
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 140 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','timeTickInterval' );
        hed_timeTickInterval = uicontrol( hFigTsPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.time.timeTickInterval), 'Position',[180,145,120,30],...
            'Callback',@callback_numeric );
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 100 300 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Maximum PRES with SAL - Y limits' );
        hed_maxPRESwPSALlim = uicontrol( hFigTsPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.time.maxPRESwPSALlim), 'Position',[300,105,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 60 300 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','SAL data in profile - Y limits' );
        hed_nvPSALlim = uicontrol( hFigTsPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.time.nvPSALlim), 'Position',[300,65,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigTsPr, 'Style','pushbutton', 'String','Select',...
            'Position',[50 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_ts_properties );        
        uicontrol( hFigTsPr, 'Style','pushbutton', 'String','Cancel',...
            'Position',[200 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_ts_properties ); 
        function callback_accept_ts_properties( hObject, eventdata )
            ff.time.startTime = datenum( hed_Date_start.String,'dd/mm/yyyy' );
            ff.time.endTime = datenum( hed_Date_end.String,'dd/mm/yyyy' );
            ff.time.timeTickStyle = timeTicksList{hpum_TimeTickStyle.Value};
            ff.time.timeTickInterval = str2num(hed_timeTickInterval.String);
            if(isempty(ff.time.timeTickInterval))
                ff.time.timeTickInterval = 1;
            end
            ff.time.maxPRESwPSALlim = str2num(hed_maxPRESwPSALlim.String);
            ff.time.nvPSALlim = str2num(hed_nvPSALlim.String);
            close( hFigTsPr )
        end
        function callback_cancel_ts_properties( hObject, eventdata )
            close( hFigTsPr )
        end
    end
    %
    function callback_accept_properties( hObject, eventdata )
        ff.title = hed_argo_title.String;
        ff.titleFontSize = str2num(hed_titleFontSize.String);
        if(isempty(ff.titleFontSize))
            ff.titleFontSize = 20;
        end
        ff.fontSize = str2num(hed_axFontSize.String);
        if(isempty(ff.fontSize))
            ff.fontSize = 12;
        end
        ff.colormap = colormapList{hpum_colormap.Value};
        ff.nLeg = str2num(hed_nLeg.String);
        if(isempty(ff.nLeg))
            ff.nLeg = 10;
        end
        ff.legendFontSize = str2num(hed_legendFontSize.String);
        if(isempty(ff.legendFontSize))
            ff.legendFontSize = 12;
        end
        close( hFigModal )
        feval( get(findobj('Checked','on'),'callback') )
    end
    function callback_cancel_properties( hObject, eventdata )
        close( hFigModal )
    end
end
%
function callback_is2lims( hObject, eventdata )
    if(~is2lims(str2num(hObject.String)))
        hObject.String = [];
    end
end
%
function callback_numeric( hObject, eventdata )
    x = str2double(hObject.String);
    if(isnan(x))
        hObject.String = [];
    end
end
%
function callback_Date_start( hObject, eventdata )
    try
        Date_start = datenum( hObject.String,'dd/mm/yyyy' );
    catch
        Date_start = dd.trajTbl.mtime(1);
    end
    hObject.String = datestr(Date_start,24);
end
%
function callback_Date_end( hObject, eventdata )
    try
        Date_end = datenum( hObject.String,'dd/mm/yyyy' );
    catch
        Date_end = dd.trajTbl.mtime(end);
    end
    hObject.String = datestr(Date_end,24);
end
%
function yes = iscolor( x )
    if(ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) )
        yes = true;
    else
        x = str2num(x);
        if(all(isnumeric(x)) && (length(x)==3) && all(x<=1) && all(x>=0))
            yes = true;
        else
            yes = false;
        end
    end
end
%
%   Initial figure settings
function ff = setFig
    ff.title = dd.trajTbl.platform_number{1};
    ff.titleFontSize = 20;
    ff.fontSize = 12;
    ff.nLeg = 10;
    ff.legendFontSize = 12;
    ff.map.latLim = [];
    ff.map.lonLim = [];
    ff.colormap = 'jet';
    ff.map.landcolorDef = [0.8 0.8 0.8];
    ff.map.landcolor = ff.map.landcolorDef;
    ff.map.GSHHS = '';
    ff.map.axesFontSize = 16;
    ff.prof.presLim = [];
    ff.prof.tempLim = [];
    ff.prof.salLim = [];
    ff.prof.presDiffLim = [];
    ff.prof.tempDiffLim = [];
    ff.prof.salDiffLim = [];
    ff.time.startTime = min(dd.trajTbl.mtime);
    ff.time.endTime = max(dd.trajTbl.mtime);
    ff.time.timeTickStyle = timeTicksList{1};
    ff.time.timeTickInterval = 1;
    ff.time.maxPRESwPSALlim = [];
    ff.time.nvPSALlim = [];
end
%
end



