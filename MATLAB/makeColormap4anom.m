function colMap = makeColormap4anom( N, varargin ) 
%   
%   Make a colormap. By default make a colormap for anomalies, symmetrical 
%       from blue to white to red 
%
%   Syntax: colMap = makeColormap4anom( N,... )
%
%   Input [Required]:
%       N - the size of the colormap
%   Input [Optional]:
%       vec4cm - input vector for colormap, default [-0.05;-0.01;0;0.01;0.05]
%       hex4cm - input hex array for colormap,  
%                default ['#0000ff';'#ddeeff';'#ffffff';'#ffeedd';'#ff0000']
%
%   Output:     
%       colMap - colormap of size N*3
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-23
%
p = inputParser;
addRequired(p,'N',@isscalar);
addParameter(p,'vec4cm',[-0.05;-0.01;0;0.01;0.05],@isvector);
addParameter(p,'hex4cm',['#0000ff';'#ddeeff';'#ffffff';'#ffeedd';'#ff0000'],@ischar);
parse(p, N, varargin{:} )
N = p.Results.N;
vec4cm = p.Results.vec4cm;
hex4cm = p.Results.hex4cm;
%
[nC,hL] = size(hex4cm);
if(length(vec4cm)~=nC)
    error('The length of vec4cm must be equal to the size of hex4cm')
end
if(hL~=7)
    error('The length of each element of hex4cm must be 7')
end
if(~all(strncmp(cellstr(hex4cm),'#',1)))
    error('The elements of hex4cm must start with #')
end
%
raw = sscanf(hex4cm','#%2x%2x%2x',[3,size(hex4cm,1)]).' / 255;
colMap = interp1(vec4cm,raw,linspace( vec4cm(1), vec4cm(end), N ),'linear');
%
end

