function CLMdata = climAlongTraj( climDir, climTbl, climSel, trajTbl )
%
%  Extracts data from one of the 5 climatologies ('WOA1';'WOA4';'MIMOC';'CARS';'RG')
%       along the float trajectory
%
% Syntax: CLMdata = climAlongTraj( climDir, climTbl, climSel, trajTbl );
%
%   Input [Required]:
%       climDir - the directory there climatologies are installed
%       climTbl - table with VariableNames {code,climatology,path},
%           created by the function "climTblSet.m"
%               climTbl.code - climatology codes {'WOA1';'WOA4';'MIMOC';'CARS';'RG'}
%               climTbl.climatology - climatology description
%               climTbl.path - paths to climatology data files in climDir
%       climSel - the selected climatology, onr of {'WOA1';'WOA4';'MIMOC';'CARS';'RG'}
%       trajTbl - table of size n_prof with columns:
%               latitude - (-90 to 90)
%               longitude - (-180 to 360)
%               mtime - MATLAB datenum format
%
%   Output:
%       CLMdata - structure with the fields - arrays of size(n_depths,n_prof).
%           TEMP - temperature
%           SAL - salinity
%           PRES - pressure
%           PTMP - potential temperature
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-065-19
%
p = inputParser;
addRequired(p,'climDir',@ischar);
addRequired(p,'climTbl',@istable);
addRequired(p,'climSel',@ischar);
addRequired(p,'trajTbl',@istable);
parse(p, climDir, climTbl, climSel, trajTbl )
climDir = p.Results.climDir;
climTbl = p.Results.climTbl;
climSel = p.Results.climSel;
trajTbl = p.Results.trajTbl;
%
if( ~exist( climDir, 'dir' ) )
    error(['The directory ',climDir,' does not exist'])
end
if( ~ismember( climSel, climTbl.code ) )
    error(['The climatology code ',climSel,' is incorrect'] )
end
if( ~all(ismember(  {'latitude','longitude','mtime'}, trajTbl.Properties.VariableNames ) ) )
    error( 'trajTbl must contain the columns latitude, longitude and mtime' )
end
%
climDir = strtrim( climDir );
if( strcmp( climDir(end), filesep ) )
    climDir(end) = '';
end
lClim = find(strcmp(climTbl.code,climSel),1,'first');
climPath = [climDir,filesep,climTbl.path{lClim}];
%
if( exist(climPath,'dir') )
    switch( climSel )
        case 'WOA1'
            WOA4 = false;
            WOA1path = climPath;
            WOA4path = '';
            [ CLM_TEMP, CLM_SAL, CLM_PRES ] = climWOA( trajTbl.latitude, trajTbl.longitude,...
                         trajTbl.mtime, WOA4, WOA1path, WOA4path );
        case 'WOA4'
            WOA4 = true;
            WOA1path = '';
            WOA4path = climPath;
            [ CLM_TEMP, CLM_SAL, CLM_PRES ] = climWOA( trajTbl.latitude, trajTbl.longitude, trajTbl.mtime,...
                        WOA4, WOA1path, WOA4path );
        case 'MIMOC'
            MIMOCpath = climPath;
            [ CLM_TEMP, CLM_SAL, CLM_PRES ] = climMIMOC( trajTbl.latitude,...
                        trajTbl.longitude, trajTbl.mtime, MIMOCpath );
        case 'CARS2009'
            CARSpath = climPath;
             [ CLM_TEMP, CLM_SAL, CLM_PRES ] = climCARS2009(...
                 trajTbl.latitude, trajTbl.longitude, trajTbl.mtime, CARSpath );
        case 'RG'
            RGpath = climPath;
            [ CLM_TEMP, CLM_SAL, CLM_PRES ] =...
                climRG( trajTbl.latitude, trajTbl.longitude, trajTbl.mtime, RGpath );
    end
else
    disp('Climatology path does not exist!')
    CLM_TEMP = [];
    CLM_SAL = [];
    CLM_PRES = [];
end
%
LAT1 = repmat(trajTbl.latitude,1,size(CLM_SAL,1))';
LONG1 = repmat(trajTbl.longitude,1,size(CLM_SAL,1))';
SA1 = gsw_SA_from_SP( CLM_SAL, CLM_PRES, LONG1, LAT1 );
CLM_PTMP = gsw_pt0_from_t( SA1, CLM_TEMP, CLM_PRES );
%
CLMdata = struct( 'PRES',CLM_PRES, 'SAL',CLM_SAL, 'TEMP',CLM_TEMP, 'PTMP',CLM_PTMP );
%
end



