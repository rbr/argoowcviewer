function [ CLM_TEMP, CLM_SAL, CLM_PRES ] = climCARS2009( LAT, LONG, mtime, CARSpath )
% 
%   Finds the CARS2009 Climatology in the path CARSpath and selects 
%   the profiles of climatic temperature and salinity for the locations and seasons 
%   determined by LAT, LONG and mtime.    
%
%   In argoOWCviewer, clim_get_CARS2009 is called from the function climAlongTraj
%
% Syntax:  [ CLM_PRES, CLM_TEMP, CLM_SAL ] = climCARS2009( LAT, LONG, mtime, CARSpath );
% 
%   Input [Required]:
%       LAT, LONG, mtime - arrays of the length equal to the number of
%           profiles (nProf)
%       CARSpath - location of RG database on local computer
%           (e.g., CARSpath = 'my_dir.../CARS2009/data/')
%   Output:
%       CLM_TEMP, CLM_SAL, CLM_PRES - arrays of size(nLayers,nProf). 
%           For CARS2009 climatology, nLayers=79;
%       CLM_PRES - pressure profiles
%       CLM_TEMP - temperature
%       CLM_SAL - salinity. 
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-07
%
t_fn = 'temperature_cars2009a.nc';
s_fn = 'salinity_cars2009a.nc';
%
p = inputParser;
addRequired(p,'LAT',@isnumeric);
addRequired(p,'LONG',@isnumeric);
addRequired(p,'mtime',@isnumeric);
addRequired(p,'CARSpath',@ischar);
parse(p, LAT, LONG, mtime, CARSpath )
% 
LAT = p.Results.LAT;
LONG = p.Results.LONG;
mtime = p.Results.mtime;
CARSpath = p.Results.CARSpath;
% Check the inputs, set defaults
if( (length(LAT)~=length(LONG)) && (length(LAT)~=length(mtime)))
    error('LAT, LONG and mtime must be the same length')
end
if( ~exist(CARSpath,'dir') )
    error(['No dir ',CARSpath] )
end
%
nProf = length( mtime );
CARSdepth = ncread( [CARSpath,t_fn], 'depth' );
nDepths = length( CARSdepth );
CLM_PRES = NaN( nDepths, nProf );
CLM_TEMP = NaN( nDepths, nProf );
CLM_SAL = NaN( nDepths, nProf );
%
CARSlat = ncread( [CARSpath,t_fn], 'lat' );
CARSlon = ncread( [CARSpath,t_fn], 'lon' );
CARSdepthAnn = ncread( [CARSpath,t_fn], 'depth_ann' );
nAnn = length( CARSdepthAnn );
CARSdepthSemiann = ncread( [CARSpath,t_fn], 'depth_semiann' );
nSemiann = length( CARSdepthSemiann );
%
h_wait = waitbar( 0, 'Extracting data from CARS2009...' );
wax = findobj(h_wait, 'type','axes');
tax = get(wax,'title');
set(tax,'fontsize',16)
%
for kProf = 1:nProf
    waitbar( kProf / nProf, h_wait )
    CLM_PRES( :,kProf ) = gsw_p_from_z( -CARSdepth, LAT(kProf) );
    %
    [~,kLat] = min(abs(CARSlat-LAT(kProf)));
    LONG1 = LONG(kProf);
    while( LONG1 < CARSlon(1) )
        LONG1 = LONG1 + 360;
    end
    while( LONG1 > CARSlon(end) )
        LONG1 = LONG1 - 360;
    end
    [~,kLon] = min(abs(CARSlon-LONG1));
    %
    jday = day( datetime(mtime(kProf),'ConvertFrom','datenum'), 'dayofyear' );
    tJday = 2 .* pi .* jday ./ 366;
    % 
    TEMP = squeeze(ncread( [CARSpath,t_fn], 'mean', [kLon, kLat, 1],[1,1,Inf] ));
    TEMPann_cos = squeeze(ncread( [CARSpath,t_fn], 'an_cos', [kLon, kLat, 1],[1,1,Inf] ));
    TEMPann_sin = squeeze(ncread( [CARSpath,t_fn], 'an_sin', [kLon, kLat, 1],[1,1,Inf] ));
    TEMP(1:nAnn) = TEMP(1:nAnn) + TEMPann_cos.*cos(tJday) + TEMPann_sin.*sin(tJday);
    TEMPsemiann_cos = squeeze(ncread( [CARSpath,t_fn], 'sa_cos', [kLon, kLat, 1],[1,1,Inf] ));
    TEMPsemiann_sin = squeeze(ncread( [CARSpath,t_fn], 'sa_sin', [kLon, kLat, 1],[1,1,Inf] ));
    TEMP(1:nSemiann) = TEMP(1:nSemiann) + TEMPsemiann_cos.*cos(2.*tJday) +...
        TEMPsemiann_sin.*sin(2.*tJday);
    %
    SAL = squeeze(ncread( [CARSpath,s_fn], 'mean',[kLon, kLat, 1],[1,1,Inf] ));
    SALann_cos = squeeze(ncread( [CARSpath,s_fn], 'an_cos', [kLon, kLat, 1],[1,1,Inf] ));
    SALann_sin = squeeze(ncread( [CARSpath,s_fn], 'an_sin', [kLon, kLat, 1],[1,1,Inf] ));
    SAL(1:nAnn) = SAL(1:nAnn) + SALann_cos.*cos(tJday) + SALann_sin.*sin(tJday);
    SALsemiann_cos = squeeze(ncread( [CARSpath,s_fn], 'sa_cos', [kLon, kLat, 1],[1,1,Inf] ));
    SALsemiann_sin = squeeze(ncread( [CARSpath,s_fn], 'sa_sin', [kLon, kLat, 1],[1,1,Inf] ));
    SAL(1:nSemiann) = SAL(1:nSemiann) + SALsemiann_cos.*cos(2.*tJday) +...
        SALsemiann_sin.*sin(2.*tJday);
    %
    CLM_TEMP( :,kProf ) = TEMP;
    CLM_SAL( :,kProf ) = SAL;
end
close( h_wait )
% 
end

