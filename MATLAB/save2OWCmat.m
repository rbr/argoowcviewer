function save2OWCmat( saveFn, trajTbl, dataStr )
%
%   Save the data to MATLAB file readable by OWC and argoOWCviewer codes
%
%   Syntax: save2OWCmat( saveFn, trajTbl, dataStr )
% 
%   Input [Required]:
%       saveFn - .mat file path 
%       trajTbl - table of size nProf with fields:
%           platform_number(cell),cycle_number,latitude,longitude,mtime,data_mode(char)
%       dataStr - the structure with Argo data (PRES,TEMP,SAL,PTMP), 
%           each a double matrix of size [nLayers*nProf]
%
%   Output:     no
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-20
%
p = inputParser;
addRequired(p,'saveFn',@ischar);
addRequired(p,'trajTbl',@istable);
addRequired(p,'dataStr',@isstruct);
parse(p, saveFn, trajTbl, dataStr )
saveFn = p.Results.saveFn;
trajTbl = p.Results.trajTbl;
dataStr = p.Results.dataStr;
%
% Check does the path exist
if( ~exist( fileparts(saveFn),'dir' ) )
    error(['Dir ',saveFn,' does not exist'])
end
% Check the necessary fields in the data structure
strFieldNames = fieldnames( dataStr );
strFieldNames2 = {'PRES','TEMP','SAL','PTMP'};
if( ~all(ismember(strFieldNames2,strFieldNames)) )
    error(['The input structire does not contain some of the fields: ',strjoin(strFieldNames2,',')])
end
% Check the necessary columns in the trajectory table
trajTblColumns = {'platform_number','cycle_number','latitude','longitude','mtime','data_mode'};
if( ~all(ismember(trajTblColumns,trajTbl.Properties.VariableNames)))
    error(['The input table does not contain some of the columns: ',strjoin(trajTblColumns,',')])
end
% Check the equal numbers of profiles and layers
nProf = height( trajTbl );
nFields = length(strFieldNames2);
nLayers = size(dataStr.(strFieldNames2{1}),1);
for kField = 1:nFields
    [nLayers1,nProf1] = size(dataStr.(strFieldNames2{kField}));
    if( nLayers1 ~= nLayers )
        error(['The number of layers in ',strFieldNames{kField},' is incorrect'])
    end
    if( nProf1 ~= nProf )
        error(['The number of profiles in ',strFieldNames{kField},' is incorrect'])
    end
end
%
PROFILE_NO = (1:nProf)';
DATES = reshape((trajTbl.mtime-datenum('1-1-0000'))./365.2425,nProf,1);
PLATFORM_NO = cellstr(strtrim(trajTbl.platform_number));
LAT = reshape( double(trajTbl.latitude), 1, nProf );
LONG = reshape( double(trajTbl.longitude), 1, nProf );
indx_w = LONG<0;
LONG(indx_w) = LONG(indx_w)+360;
mtime = reshape( double(trajTbl.mtime), 1, nProf );
data_mode = reshape( trajTbl.data_mode, 1, nProf );
PRES = double(dataStr.PRES);
SAL = double(dataStr.SAL);
TEMP = double(dataStr.TEMP);
PTMP = double(dataStr.PTMP);
 
%
save( saveFn, 'PROFILE_NO', 'LAT', 'LONG', 'DATES', 'PRES',...
        'SAL', 'TEMP', 'PTMP', 'mtime', 'PLATFORM_NO', 'data_mode' );
%
end


