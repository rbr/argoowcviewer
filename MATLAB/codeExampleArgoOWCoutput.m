% codeExampleArgoOWCoutput.m
clear variables
% Loading OWC output
owcDir = '/Users/nikolaynezlin/Documents/MATLAB/matlab_owc/matlab_owc-2.1.0_old/';
owcProjdir1 = 'CSIRO_AW';
owcFn1 = 'f7394_10dbarArgo3y.mat';
[ trajTbl, dataStr, owcStr ] = loadOWC( owcDir, owcProjdir1, owcFn1 );
figure( 'Position',[100 100 1200 600], 'visible','on','paperpositionmode','auto','Color','w' )
ax1 = subplot(2,2,1);
ax2 = subplot(2,2,3);
ax3 = subplot(1,2,2);
plotOWCsOffsets( owcStr.owcTbl, 'plotAxes',ax1, 'timeTickStyle','months', 'timeTickInterval',6 );
plotOWCsOffsetsBB( owcStr.owcTbl, 'plotAxes',ax2, 'profFitMarkerSize',20000,...
    'cLim',[-0.03 0.03], 'timeTickStyle','months', 'timeTickInterval',6 );
plotOWCsOffsetsBBmap( owcStr.owcTbl, 'plotAxes',ax3, 'projection','mercator',...
    'profFitMarkerSize',20000, 'cLim',[-0.03 0.03], 'title',trajTbl.platform_number{1},...
    'box','fancy', 'latLim',[-20 0], 'lonLim',[153 168]);

%% Combine the plotOWCsOffsetsBB with the differences between reference salinity and climatology 
climDir = '/Users/nikolaynezlin/Documents/Projects/2019/Climatology/';
climTbl = climTblSet( climDir );
CLMdata = climAlongTraj( climDir, climTbl, 'WOA1', trajTbl );
figure( 'Position',[100 100 800 800], 'visible','on','paperpositionmode','auto','Color','w' )
%  
plotOWCsOffsetsClim( owcStr, dataStr.PRES, CLMdata,...
    'title',trajTbl.platform_number{1} );





