function climTbl = climTblSet( climDir )
%
%   Creates the table used to get climatology 
%
%   Syntax: climTbl = climTblSet( climDir )
%
%   Input [Required]:
%       climDir - the path to climatology
%
%   Output:
%       climTbl - the table with three columns:
%           'code' - {'WOA1'; 'WOA4'; 'MIMOC'; 'CARS'; 'RG'}
%           'climatology' - brief description
%           'path' - path to climatology data files
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-15
%
p = inputParser;
addRequired(p,'climDir',@ischar);
parse(p, climDir )
climDir = p.Results.climDir;
%
if( ~exist( climDir, 'dir' ) )
    error(['The directory ',climDir,' does not exist'])
end
climDir = strtrim( climDir );
if( strcmp( climDir(end), filesep ) )
    climDir(end) = '';
end
%
clim_code = {'WOA1','WOA4','MIMOC','CARS2009','RG'};
clim_list = {'World Ocean Atlas 1-deg.(2005-2017)',...
    'World Ocean Atlas 0.25-deg.(1955-2017)',...
    'Monthly Isopycnal / Mixed-layer Ocean Climatology (MIMOC) 0.5-deg',....
    'CSIRO Atlas of Regional Seas (CARS2009) 0.5-deg',...
    'Roemmich-Gilson Argo Climatology'};
clim_path = {   [ 'WOA1', filesep, 'data', filesep ],...
                [ 'WOA4', filesep, 'data', filesep ],...
                [ 'MIMOC', filesep, 'data', filesep ],...
                [ 'CARS2009', filesep, 'data', filesep ],...
                [ 'RG', filesep, 'data', filesep ]};
nClim = length(clim_code);
climTbl = table( clim_code',clim_list', clim_path',true(nClim,1),...
    'VariableNames',{'code','climatology','path','exist'} );
% Remove the non-existent climatologies
% clim_exist = true( nClim, 1 );
for kClim = 1:nClim
    if( ~exist([climDir,filesep,climTbl.path{kClim}], 'dir' ))
        climTbl.exist(kClim) = false;
    end
end
% climTbl = climTbl( clim_exist, : );
%
end

