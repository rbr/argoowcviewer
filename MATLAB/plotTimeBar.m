function ax = plotTimeBar( mtime, Param, varargin )
%
%   Plot the time series of 'Param' with line and bars
%
%   Syntax: plotTimeBar( mtime, Param,... )
%
%   Input [Required]:
%       mtime - datenum format, size nProf
%       Param - numeric, size nProf
%   Input [Optional]:
%       xLim - X-axis limits (datenum, default min/max)
%       yLim - Y-axis limits (numeric, default [0 max(Y)*1.1]
%       yDir - direction of Y axis {'normal' (default) or 'reverse'}
%       fontSize - font size of the axes (numeric, default 12)
%       yLab - Y-axis label (char, default '')
%       title - plot title (char, default '')
%       titleFontSize - title font size (numeric, default 20)
%       barWidth - bar width, (numeric, default 0.5)
%       barEdgeColor - bar edge color (default 'k')
%       barFaceColor - bar face color (default [0.5 0.5 0.5]
%       barLineWidth - bar line width (default 1)
%       lineColor - line color (default 'b')
%       lineWidth - line width (default 1)
%       lineStyle - line style (default '-')
%       marker - marker symbol (default 'o')
%       markerSize - marker size (default 10)
%       markerEdgeColor - marker edge color (default 'k')
%       markerFaceColor - marker face color (default 'b')
%       grid - char {'on','off'} (default 'on')
%       timeTickStyle - the style of X-axis (mtime) ticks:
%           {'auto' (default), 'years','months','days'}
%       timeTickInterval - the interval between the time ticks 
%           (numeric, defailt 1)
%
%   Output:     
%       ax - Axes object. Can be used to make future modifications to the axes.
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-11
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'mtime',@isnumeric);
addRequired(p,'Param',@isnumeric);
addParameter(p,'xLim',[],is2nums);
addParameter(p,'yLim',[],is2nums);
addParameter(p,'yDir','normal',@ischar);
addParameter(p,'fontSize',12,@isnumeric);
addParameter(p,'yLab','',@ischar);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
addParameter(p,'barWidth',0.5,@isnumeric);
addParameter(p,'barEdgeColor','k',iscolor);
addParameter(p,'barFaceColor',[0.5 0.5 0.5],iscolor);
addParameter(p,'barLineWidth',1,@isnumeric);
addParameter(p,'lineColor','b',iscolor);
addParameter(p,'lineWidth',1,@isnumeric);
addParameter(p,'lineStyle','-',@ischar);
addParameter(p,'marker','o',@ischar);
addParameter(p,'markerSize',10,@isnumeric);
addParameter(p,'markerEdgeColor','k',iscolor);
addParameter(p,'markerFaceColor','b',iscolor);
addParameter(p,'grid','on',@ischar);
istimetick = @(x) (ischar(x) && (ismember(x,{'auto','years','months','days'})));
addParameter(p,'timeTickStyle','auto',istimetick);
addParameter(p,'timeTickInterval',1,@isnumeric);
addParameter(p,'plotAxes',[],isaxes);
parse(p, mtime, Param, varargin{:} )
mtime = p.Results.mtime;
Param = p.Results.Param;
xLim = p.Results.xLim;
yLim = p.Results.yLim;
yDir = p.Results.yDir;
fontSize = p.Results.fontSize;
yLab = p.Results.yLab;
plotTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
barWidth = p.Results.barWidth;
barEdgeColor = p.Results.barEdgeColor;
barFaceColor = p.Results.barFaceColor;
barLineWidth = p.Results.barLineWidth;
lineColor = p.Results.lineColor;
lineWidth = p.Results.lineWidth;
lineStyle = p.Results.lineStyle;
marker = p.Results.marker;
markerSize = p.Results.markerSize;
markerEdgeColor = p.Results.markerEdgeColor;
markerFaceColor = p.Results.markerFaceColor;
gridOnOff = p.Results.grid;
timeTickStyle = p.Results.timeTickStyle;
timeTickInterval = p.Results.timeTickInterval;
ax = p.Results.plotAxes;
%
if( length(mtime) ~= length(Param) )
    error('The lengths of time and parameter vectors are not equal')
end
if( isempty(xLim) )
    xLim = [min(mtime) max(mtime)];
end
if( isempty(yLim) )
    yLim = [ 0, max(Param).*1.1 ];
end
%
if( isempty(ax) )
    ax = axes;
else
    cla(ax)
end
%
bar( ax, mtime, Param, barWidth, 'EdgeColor',barEdgeColor, 'FaceColor',barFaceColor,...
    'BaseValue',yLim(1), 'LineWidth',barLineWidth )
hold on
plot( ax, mtime, Param, 'Color',lineColor, 'lineWidth',lineWidth, 'lineStyle',lineStyle,...
    'marker',marker, 'markerSize',markerSize, 'markerEdgeColor',markerEdgeColor,...
    'markerFaceColor',markerFaceColor )
% x_ticks = setTickMonths( mtime(1), mtime(end) );
switch timeTickStyle
    case 'auto'
        xTicks = get( ax, 'XTick' );
        dateFmt = 'ddmmmyy';
    case 'days'
        xTicks = calcTimeTicks( xLim, timeTickStyle, timeTickInterval );
        dateFmt = 'ddmmmyy';
    case {'months','years'}
        xTicks = calcTimeTicks( xLim, timeTickStyle, timeTickInterval );
        dateFmt = 'mmmyy';
end
set( ax, 'XTick',xTicks, 'XTickLabel',datestr(xTicks,dateFmt) )
set( ax,'YDir',yDir, 'fontSize',fontSize )
xlim(ax,xLim)
ylim(ax,yLim)
ylabel( ax,yLab )
grid(ax,gridOnOff)
title( ax,plotTitle, 'FontSize',titleFontSize )
set(ax,'Layer','top')
%
end

