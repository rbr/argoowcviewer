function ax = plotProf( PRES, Param, varargin )
%
%   Plot the profiles of the 'Param' along the PRES Y-axis
%
%   Syntax: plotProf( PRES, Param, ... )
%
%   Input [Required]:
%       PRES - numerical matrix (nLayers,nProf)
%       Param - numerical matrix (nLayers,nProf)
%   Input [Optional]:
%       xLim - X limits (numeric 2x1)
%       yLim - PRES limits (numeric 2x1)
%       colormap - predefined colormap: 'jet' (default),'parula',etc.)
%       LineWidth - the width of the lines (default 1)
%       LineStyle - the style of the lines (default '-')
%       nLeg - the number of profiles in legend (default 10)
%       mtime - datenum format, size nProf, default [] 
%       legendFontSize - legend font size (default 12)
%       legendLocation - legend location (default 'best')
%       fontSize - font size of the axes (numeric)
%       yLab - Y label (char) (default 'Sea Pressure (dbar)')
%       xLab - X label (char) (default '')
%       grid - char {'on','off'} (default 'on')
%       title - figure title
%       titleFontSize - title font size (numeric, default 20)
%       plotAxes - Axes object. Created by default
%
%   Output:     
%       ax - Axes object. Can be used to make future modifications to the axes.
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-12
%
colormapList = {'parula','jet','hsv','hot','cool','spring','summer',...
    'autumn','winter','gray','bone','copper','pink'};
iscolormap = @(x) (all(isnumeric(x)) && (size(x,2)==3)) ||...
    (ischar(x) && (ismember(x,colormapList)) );
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
p = inputParser;
addRequired(p,'PRES',@ismatrix);
addRequired(p,'Param',@ismatrix);
addParameter(p,'xLim',[],is2nums);
addParameter(p,'yLim',[],is2nums);
addParameter(p,'colormap','jet',iscolormap);
addParameter(p,'LineWidth',1,@isnumeric);
addParameter(p,'LineStyle','-',@ischar);
addParameter(p,'nLeg',10,@isnumeric);
addParameter(p,'mtime',[],@isnumeric);
addParameter(p,'legendFontSize',12,@isnumeric);
addParameter(p,'legendLocation','best',@ischar);
addParameter(p,'fontSize',12,@isnumeric);
addParameter(p,'yLab','Sea Pressure (dbar)',@ischar);
addParameter(p,'xLab','',@ischar);
addParameter(p,'grid','on',@ischar);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
addParameter(p,'plotAxes',[],isaxes);
parse(p, PRES, Param, varargin{:} )
PRES = p.Results.PRES;
Param = p.Results.Param;
xLim = p.Results.xLim;
yLim = p.Results.yLim;
if( isempty(yLim) )
    yLim = [min(PRES(:)) max(PRES(:))];
end
colorMap  = p.Results.colormap;
LineWidth = p.Results.LineWidth;
LineStyle = p.Results.LineStyle;
nLeg = p.Results.nLeg;
mtime = p.Results.mtime;
legendFontSize = p.Results.legendFontSize;
legendLocation = p.Results.legendLocation;
fontSize = p.Results.fontSize;
yLab = p.Results.yLab;
xLab = p.Results.xLab;
gridOnOff = p.Results.grid;
plotTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
ax = p.Results.plotAxes;
%
if( ~all(size(PRES)==size(Param)) )
    error( 'The size of PRES ~= the size of Param' )
end
nProf = size(PRES,2);
%
if( isempty(ax) )
    ax = axes;
else
    cla(ax)
end
if(ischar(colorMap))
    colorScale = colormap( feval( colorMap, nProf ) );
else
    colorScale = interp1(linspace(0,1,size(colorMap,1)),colorMap,linspace(0,1,nProf));
end
%
plot( ax, Param(:), PRES(:), 'Marker','.', 'Visible','off' )
hold on
hProf = gobjects(nProf,1);
for kProf = 1:nProf
    XY = [ PRES(:,kProf), Param(:,kProf) ];
    [~,indxSort] = sort( XY(:,1) );
    XY = XY(indxSort,:);
    hProf(kProf) = plot( ax, XY(:,2), XY(:,1), 'Color',colorScale(kProf,:),...
        'LineWidth',LineWidth, 'LineStyle',LineStyle );
end
set( ax, 'YDir','rev', 'FontSize',fontSize )
if( ~isempty(xLim) )
    xlim(xLim)
end
ylim(yLim)
ylabel(yLab)
xlabel(xLab)
grid(gridOnOff)
title(plotTitle, 'FontSize',titleFontSize )
nLeg = min([nLeg,nProf]);
if( (nLeg > 0) && (length(mtime)==nProf) )
    kInLeg = round(linspace(1,nProf,nLeg));
    hLeg = legend( hProf(kInLeg), {datestr(mtime(kInLeg),'DD-mmm-yyyy')},...
        'Location',legendLocation );
    hLeg.FontSize = legendFontSize;
end
%
end

