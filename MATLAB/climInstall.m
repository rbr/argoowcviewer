function climInstall( climDir, varargin )

% The function is a script automatically downloading specified clmatology 
% data required by Argo_viewer, the sources may come from:
% 
% 	World Ocean Atlas of 1-degree resolution (WOA1);
% 		Source: https://www.nodc.noaa.gov/OC5/woa18/woa18data.html
% 		The 2005-2017 decadal period. 
% 	World Ocean Atlas of 0.25-degree resolution (WOA4);
% 		Source: https://www.nodc.noaa.gov/OC5/woa18/woa18data.html
% 		Averaged decades. 
% 	Monthly Isopycnal & Mixed-layer Ocean Climatology (MIMOC)
% 		Source: https://www.pmel.noaa.gov/mimoc/
% 	CSIRO Atlas of Regional Seas (CARS2009)
% 		Source: http://www.marine.csiro.au/atlas/
%   Roemmich-Gilson Argo Climatology
%       Source: http://sio-argo.ucsd.edu/RG_Climatology.html
%
% The function will detect if the files already exist in target directory, 
% it will only start downloading when the files are absent.
%
% Input:
%      [Required] - climDir - directory of the downloaded clm data
%
%      [Optional] - clmData - which clm dataset to download, default is
%      'all', could be any of the {'WOA1','WOA4','MIMOC','CARS2009','RG_Argo_Climatology'}
%
% Example:
%
% downloadClmData('~/argo-viewer/data_clm/')
% OR
% downloadClmData('~/argo-viewer/data_clm/','clmData',{'WOA1','WOA4'});
%
% Author: Nikolay Nezlin and Rui Zhang (RBR Ltd. Ottawa ON, Canada)
% email: support@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-27

p = inputParser;
addRequired(p, 'climDir', @ischar);
addParameter(p, 'clmData', 'all');
parse(p, climDir, varargin{:})
climDir = p.Results.climDir;
clmData = p.Results.clmData;

defaultClm = {'WOA1','WOA4','MIMOC','CARS2009','RG'};
if strcmpi(clmData,'all')
    clmData = defaultClm;
end
if ~iscell(clmData)
    clmData = {clmData};
end

subfolderDir = strcat(defaultClm,'/data');
%
kClim = 1; % WOA1
if(any(ismember(defaultClm{kClim},clmData))) % WOA1
    % check existence of subfolder, if not, create it
    if ~exist([climDir,filesep,subfolderDir{kClim}], 'dir')
        mkdir([climDir,filesep,subfolderDir{kClim}])
    end
    woaDir = ' https://data.nodc.noaa.gov/thredds/fileServer/ncei/woa/';
    salDir = 'salinity/A5B7/1.00/';
    tempDir = 'temperature/A5B7/1.00/';
    h_wait = waitbar( 0, 'Downloading data from WOA1...' );
    woaList = 0:12;
    nWOAf = 2*length(woaList);
    for kFile = woaList
        waitbar( (2*kFile)/nWOAf, h_wait )
        sFn = ['woa18_A5B7_s' sprintf('%02d',kFile) '_01.nc'];
        URL = [woaDir,salDir,sFn];
        saveFn = [climDir,filesep,subfolderDir{kClim},filesep,sFn];
        try
            websave(saveFn,URL);
        catch  
            system(['curl -o ',saveFn,' ',URL]);
        end
        waitbar( (2*kFile+1)/nWOAf, h_wait )
        tFn = ['woa18_A5B7_t' sprintf('%02d',kFile) '_01.nc'];
        URL = [woaDir,tempDir,tFn];
        saveFn = [climDir,filesep,subfolderDir{kClim},filesep,tFn];
        try
            websave(saveFn,URL);
        catch  
            system(['curl -o ',saveFn,' ',URL]);
        end
    end
    close(h_wait)
end
%
kClim = 2; % WOA4
if(ismember(defaultClm{kClim},clmData)) % WOA4
    % check existence of subfolder, if not, create it
    if ~exist([climDir,filesep,subfolderDir{kClim}], 'dir')
        mkdir([climDir,filesep,subfolderDir{kClim}])
    end
    woaDir = ' https://data.nodc.noaa.gov/thredds/fileServer/ncei/woa/';
    salDir = 'salinity/decav/0.25/';
    tempDir = 'temperature/decav/0.25/';
    h_wait = waitbar( 0, 'Downloading data from WOA4...' );
    woaList = 0:12;
    nWOAf = 2*length(woaList);
    for kFile = woaList
        waitbar( (2*kFile)/nWOAf, h_wait )
        sFn = ['woa18_decav_s' sprintf('%02d',kFile) '_04.nc'];
        URL = [woaDir,salDir,sFn];
        saveFn = [climDir,filesep,subfolderDir{kClim},filesep,sFn];
        try
            websave(saveFn,URL);
        catch  
            system(['curl -o ',saveFn,' ',URL]);
        end
        waitbar( (2*kFile+1)/nWOAf, h_wait )
        tFn = ['woa18_decav_t' sprintf('%02d',kFile) '_04.nc'];
        URL = [woaDir,tempDir,tFn];
        saveFn = [climDir,filesep,subfolderDir{kClim},filesep,tFn];
        try
            websave(saveFn,URL);
        catch  
            system(['curl -o ',saveFn,' ',URL]);
        end
    end
    close(h_wait)
end
%
kClim = 3; % MIMOC
if(ismember(defaultClm{kClim},clmData)) % MIMOC
    % check existence of subfolder, if not, create it
    if ~exist([climDir,filesep,subfolderDir{kClim}], 'dir')
        mkdir([climDir,filesep,subfolderDir{kClim}])
    end
    mmDir = ' https://www.pmel.noaa.gov/mimoc/data/';
    h_wait = waitbar( 0, 'Downloading data from MIMOC...' );
    fList = 1:12;
    nFiles = length(fList);
    for kFile = fList
        waitbar( kFile/nFiles, h_wait )
        stFn = ['MIMOC_Z_GRID_v2.2_PT_S_month' sprintf('%02d',kFile) '.nc.gz'];
        URL = [mmDir,stFn];
        saveFn = [climDir,filesep,subfolderDir{kClim},filesep,stFn];
        try
            websave(saveFn,URL);
        catch  
            system(['curl -o ',saveFn,' ',URL]);
        end
        gunzip([climDir,filesep,subfolderDir{kClim},filesep,stFn])
        delete([climDir,filesep,subfolderDir{kClim},filesep,stFn])
    end
    close(h_wait)
end
%
kClim = 4; % CARS2009
if(ismember(defaultClm{kClim},clmData)) % CARS2009
    % check existence of subfolder, if not, create it
    if ~exist([climDir,filesep,subfolderDir{kClim}], 'dir')
        mkdir([climDir,filesep,subfolderDir{kClim}])
    end
    ccDir = ' http://www.marine.csiro.au/atlas/export/';
    h_wait = waitbar( 0, 'Downloading data from CARS2009...' );
    waitbar( 0.33, h_wait )
    sFn = 'salinity_cars2009a.nc.gz';
    URL = [ccDir,sFn];
    saveFn = [climDir,filesep,subfolderDir{kClim},filesep,sFn];
    try
        websave(saveFn,URL);
    catch  
        system(['curl -o ',saveFn,' ',URL]);
    end
    gunzip([climDir,filesep,subfolderDir{kClim},filesep,sFn])
    delete([climDir,filesep,subfolderDir{kClim},filesep,sFn])
    waitbar( 0.67, h_wait )
    tFn = 'temperature_cars2009a.nc.gz';
    URL = [ccDir,tFn];
    saveFn = [climDir,filesep,subfolderDir{kClim},filesep,tFn];
    try
        websave(saveFn,URL);
    catch  
        system(['curl -o ',saveFn,' ',URL]);
    end
    gunzip([climDir,filesep,subfolderDir{kClim},filesep,tFn])
    delete([climDir,filesep,subfolderDir{kClim},filesep,tFn])
    close(h_wait)
end
%
kClim = 5; % RG
if(ismember(defaultClm{kClim},clmData)) % RG
    % check existence of subfolder, if not, create it
    if ~exist([climDir,filesep,subfolderDir{kClim}], 'dir')
        mkdir([climDir,filesep,subfolderDir{kClim}])
    end
    h_wait = waitbar( 0, 'Downloading data from Roemmich-Gilson...' );
    kakapo = ftp('kakapo.ucsd.edu');
    cd( kakapo, '/pub/gilson/argo_climatology/');
    rgList = extractfield(dir(kakapo),'name')';
    indxSel = find(not(cellfun('isempty',strfind(rgList,'.nc.gz'))));
    rgList = rgList(indxSel);
    nFiles = length(rgList);
    cd( [climDir,filesep,subfolderDir{kClim}] )
    for kFile = 1:nFiles
        waitbar( kFile/nFiles, h_wait )
        rgFn = rgList{kFile};
        mget(kakapo,rgList{kFile});
        gunzip(rgFn);
        delete(rgFn)
   end
    close(h_wait)
end

end