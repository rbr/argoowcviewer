function guiArgoOWCviewer
%
%   GUI interface to analyze the output of OWC analysis
%
%   Syntax: guiArgoOWCviewer
%
%   Input: No
%
%   Output: No    
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-06-24
%
close all
% Set the structures for handles (hh), settings (ss), data (dd) and figure
%   (ff)
hh = struct; % Handles
ss = struct; % Settings (paths, file names, etc.)
dd = struct; % Data
ff = struct; % Figure settings
% Set useful variables
warndlg_opts = struct( 'WindowStyle','modal', 'Interpreter','tex');
fsp1 = '<HTML><FONT size=+1>';
colormapList = {'parula','jet','hsv','hot','cool','spring','summer',...
    'autumn','winter','gray','bone','copper','pink'};
colorList = {'k','w','b','g','r','c','m','y';...
    'black','white','blue','green','red','cyan','magenta','yellow'};
lineStyleList = {'-','--',':','-.';'solid','dashed','dotted','dash-dot'};
GSHHSlist = {'','c','l','i','h','f';'auto','coarse','low','intermedian','high','full'};
timeTicksList = {'auto','years','months','days'};
axPos = [0.1,0.1,0.8,0.8];
projectionList = {'Albers Equal-Area Conic',...
    'Lambert Conformal Conic','Mercator','Miller Cylindrical',...
    'Equidistant Cylindrical',...
    'Oblique Mercator','Transverse Mercator','Sinusoidal','Gall-Peters',...
    'Hammer-Aitoff','Mollweide','Robinson','UTM'};
gdacIndexFn = 'ar_index_global_prof.txt';
oceansList = {'','A','P','I';'Not selected','Atlantic','Pacific','Indian'};
%
% Anonymous Functions
is2lims = @(x) (all(isnumeric(x)) && (length(x)==2) && (x(1)<x(2)) );
%
% Find the code source directory
codeDir = which('guiArgoOWCviewer');
codeDir = strsplit(codeDir,filesep);
codeDir = strjoin(codeDir(1:end-1),filesep);
% Read the RBR logo
RBRlogo = imread([codeDir,filesep,'ArgoRBRlogo.png']);
% Load initial settings
ss.initSettingsFn = 'argoOWCinitSettings.mat';
if(exist([codeDir,filesep,ss.initSettingsFn],'file')==2)
    load( [codeDir,filesep,ss.initSettingsFn] )
else
    ss.paths.OWCdir = codeDir;
    ss.paths.gdacDir = codeDir;
end
ss.paths.codeDir = codeDir;
if( ~isfield(ss,'gdac') || ~isfield(ss.gdac,'indexFileDir') )
    ss.gdac.indexFileDir = ss.paths.codeDir;
elseif(exist(ss.gdac.indexFileDir,'dir')~=7)
    ss.gdac.indexFileDir = ss.paths.codeDir;
end
%
% Start the GUI window
hh.hFig = figure('Visible','on','Units','normalized','Position',[0.1,0.1,0.8,0.8],...
    'Toolbar','none', 'Menubar','none', 'Name','guiArgoOWCviewer', 'NumberTitle','off');
hMenuItem2add = uimenu( hh.hFig, 'Label',[fsp1, 'File']);
%
uimenu( hMenuItem2add, 'Label',[ fsp1, 'Set OWC source directory' ],...
    'Separator','off','Callback',@callback_set_dir_owc);
uimenu( hMenuItem2add, 'Label',[fsp1, 'Set climatology directory'],...
    'Separator','on','Callback',@callback_set_dir_clim );
uimenu( hMenuItem2add, 'Label',[fsp1, 'Install climatologies'],...
    'Separator','off','Callback',@callback_clim_install, 'Enable','off',...
    'Tag','Install_clim');
uimenu( hMenuItem2add, 'Label',[fsp1, 'Set GDAC index file'],...
    'Separator','on','Callback',@callback_gdac_index );
uimenu( hMenuItem2add, 'Label',[fsp1, 'Set directory for GDAC files'],...
    'Separator','off','Callback',@callback_dir_gdac );
uimenu( hMenuItem2add, 'Label',[fsp1, 'Load Argo OWC .mat file'],...
    'Callback',@callback_open_argo_mat, 'Separator','on' );
uimenu( hMenuItem2add, 'Label',[fsp1, 'Exit'],...
    'Separator','on','Callback',@callback_exit );
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[fsp1, 'Figure'],...
    'Tag','Figure','Enable','off');
menuFtbl = cell2table( {...
    'Map',              'Map of profiles',                                  'on',   1,1,...
        @callback_map;... 
    'prof_PTMP',        'Profiles of theta',                          'on',   1,1,...
        @callback_prof_PTMP;...            
    'prof_SAL',         'Profiles of salinity',                             'off',  1,1,...
        @callback_prof_SAL;...              
    'TS_diagr',         'Theta-S diagram',                                  'off',  1,1,...
        @callback_TS_diagr;...          
    'ts_SAL_PRES',      'Timeseries SAL / PRES',                            'on',   1,1,... 
        @callback_ts_SAL_PRES;...
    'ts_PTMP_PRES',     'Timeseries Theta / PRES',                          'off',  1,1,...
        @callback_ts_PTMP_PRES;...
    'ts_dPTMP_PRES',    'Timeseries Theta gradient / PRES',                 'off',  1,1,...
        @callback_ts_dPTMP_PRES;...
    'ts_SAL_PTMP',      'Timeseries SAL / Theta',                           'on',  1,1,...
        @callback_ts_SAL_PTMP;...          
    'ts_SALa_PTMP_med', 'Timeseries SAL anomaly (vs median) / Theta',       'off',  1,1,...
        @callback_ts_SALa_PTMP_med;...      
    'ts_SALa_PTMP_clim','Timeseries SAL anomaly (vs climatology) / Theta',  'off',  0,1,...
        @callback_ts_SALa_PTMP_clim;...    
    'OWC_ts_offsets',   'OWC fit with errors',                             'on',   1,0,...
        @callback_owc_ts_offsets;...
    'OWC_ts_bb',        'OWC fit: timeseries with "bubbles"',              'off',   1,0,...
        @callback_owc_ts_bb;...
    'OWC_bb_map',       'OWC fit: map with "bubbles"',                     'off',   1,0,...
        @callback_owc_bb_map;...
    'OWC_ts_ref_clim',      'OWC reference-climatology',                     'off',   0,0,...
        @callback_owc_clim;...
    'OWC_ts_off_ref_clim',      'OWC fit and reference-climatology',             'off',   0,0,...
        @callback_owc_ts_clim;...
    'Fig_properties',   'Figure properties',                                'on',   1,1,...
        @callback_fig_properties;...
    'Export_figure',    'Export plot',                                      'on',   1,1,...
        @callback_export_plot;...
    'new_figure',       'Open plot in new figure',                          'on',   1,1,...
        @callback_new_figure;...
     }, 'VariableNames',{'Tag','Label','sep','clim','owc','callback'} );         
%
for kmF1 = 1:height(menuFtbl)
    uimenu( hMenuItem2add, 'Label',[ fsp1, menuFtbl.Label{kmF1} ],...
        'Tag',menuFtbl.Tag{kmF1}, 'Separator',menuFtbl.sep{kmF1},...  '
        'Callback',menuFtbl.callback{kmF1}, 'Enable','on','Checked','off' );
end
% 
hMenuItem2add = uimenu( hh.hFig, 'Label',[fsp1, 'OWC results'],...
    'Tag','OWC_results','Enable','off');
uimenu( hMenuItem2add, 'Label',[fsp1,'OWC calibration table'],...
    'Tag','OWC_calibration','Enable','on', 'Callback',@callback_OWCtable,...
    'Checked','off', 'Separator','on');
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[fsp1, 'Climatology'],...
    'Tag','Climatology','Enable','off');
uimenu( hMenuItem2add, 'Label',[fsp1,'Get climatology'],...
    'Tag','getClimatology','Enable','on', 'Callback',@callback_getClim,...
    'Checked','off', 'Separator','on');
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[fsp1, 'Theta levels'],...
    'Tag','Theta_levels','Enable','off');
uimenu( hMenuItem2add, 'Label',[fsp1, 'Select thetas'],...
    'Tag','Thetas','Enable','on', 'Callback',@callback_thetas, 'Checked','off');
%
hMenuItem2add = uimenu( hh.hFig, 'Label',[fsp1, 'Floats from GDAC'],...
    'Tag','GDAC','Enable','off');
uimenu( hMenuItem2add, 'Label',[fsp1, 'Contour polygon and click Enter'],...
    'Tag','GDAC_rectangle','Enable','on', 'Callback',@callback_GDAC_polygon);
uimenu( hMenuItem2add, 'Label',[fsp1, 'Find Argo floats'],...
    'Tag','GDAC_find_floats','Enable','off', 'Callback',@callback_GDAC_find_floats);
%
hh.staticText_argoFn = uicontrol( hh.hFig, 'Style','text',...
    'Units','normalized','Position',[0.02 0.88 0.28 0.1],...
    'FontSize',16, 'HorizontalAlignment','left',...
    'String','No Argo NetCDF file selected' );
hh.staticText_argoid = uicontrol( hh.hFig, 'Style','text',...
    'Units','normalized','Position',[0.02 0.82 0.28 0.1],...
    'FontSize',16, 'HorizontalAlignment','left',...
    'String','' );
%
hh.staticText_data_mode = uicontrol( hh.hFig, 'Style','text',...
    'Units','normalized','Position',[0.015 0.73 0.5 0.1],...
    'FontSize',16, 'HorizontalAlignment','left',...
    'String','DATA MODE:', 'Visible','off' );
hh.uitable_data_mode = uitable( hh.hFig, 'FontSize',14,...
    'Units','normalized','Position',[0.01 0.65 0.3 0.15],...
    'Visible','off', 'ColumnWidth',{60,50,120,120},...
    'ColumnFormat',{'char','char',[],[]} );
%
hh.staticText_Thetas = uicontrol( hh.hFig, 'Style','text',...
    'Units','normalized','Position',[0.05 0.41 0.15 0.1],...
    'FontSize',16, 'HorizontalAlignment','left',...
    'String','Selected PTMP levels', 'Visible','off' );
hh.utable_thetas = uitable( hh.hFig, 'FontSize',12, 'Visible','off',...
    'Units','normalized','Position',[0.025 0.12 0.25 0.33] );
%
if( isfield(ss.paths,'climDir') && (exist(ss.paths.climDir,'dir')==7) )
    try
        dd.climTbl = climTblSet( ss.paths.climDir );
        dd.climSel = dd.climTbl.code{1};
    catch
    end
    set( findobj( 'Tag','Install_clim' ), 'Enable','on' )
end
%
hh.hpax1 = uipanel( hh.hFig, 'Position',[0.3 0 0.7 1],'BackgroundColor','w' );
delete(findobj(hh.hpax1, 'type', 'axes'))
hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',[0.1, 0.1, 0.8, 0.8 ] );
image( RBRlogo )
set( hh.hax1, 'XTick',[], 'YTick',[], 'Box','on' )
%
% Nested functions - callbacks
% Set directory with OWC mat files
function callback_set_dir_owc( hObject, eventdata )
    if(~isfield(ss.paths,'owcDir') || (~ischar(ss.paths.owcDir)))
        ss.paths.owcDir = ss.paths.codeDir;
    end
    data_dir1 = uigetdir( ss.paths.owcDir );
    if( data_dir1 )
        ss.paths.owcDir = data_dir1;
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
    end
end
%
% Set directory with climatology
function callback_set_dir_clim( hObject, eventdata )
    if(~isfield(ss.paths,'climDir') || (~ischar(ss.paths.climDir)))
        ss.paths.climDir = ss.paths.codeDir;
    end
    clm_dir1 = uigetdir( ss.paths.climDir );
    if( clm_dir1 )
        ss.paths.climDir = clm_dir1;
        dd.climTbl = climTblSet( ss.paths.climDir );
        dd.climSel = dd.climTbl.code{1};
        set( findobj( 'Tag','Install_clim' ), 'Enable','on' )
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
    end
end
%
function callback_clim_install( hObject, eventdata )
    hFigModal = figure( 'Position',[200,350,650,250],'WindowStyle','normal',...%modal',...
        'Name','Install climatologies', 'NumberTitle','off');
    uicontrol( hFigModal, 'Style','text', 'String','Select climatologies to install',...
        'Position',[150 200 300 30], 'FontSize',16 );
    uitable_clim = uitable( hFigModal, 'FontSize',14,...
        'Position',[20 70 600 120],'ColumnEditable', [false,true],...
        'Visible','on', 'ColumnWidth',{500,50}, 'ColumnName',{'Climatology','select'},...
        'ColumnFormat',{'char','logical'} );
    uitable_clim.Data = table2cell(table(dd.climTbl.climatology,~dd.climTbl.exist));
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Install',...
        'Position',[120 20 100 30],...
        'FontSize',16, 'Callback',@callback_install_clim_start );        
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Cancel',...
        'Position',[400 20 100 30],...
        'FontSize',16, 'Callback',@callback_install_clim_cancel );   
    %
    function callback_install_clim_start( hObject, eventdata )
        indxSel = [uitable_clim.Data{:,2}]';
        close(hFigModal)
        clm2install = dd.climTbl.code(indxSel);
        climInstall(ss.paths.climDir,'clmData',clm2install);
        dd.climTbl = climTblSet( ss.paths.climDir );
    end
    %
    function callback_install_clim_cancel( hObject, eventdata )
        close(hFigModal)
    end
end
%
function callback_dir_gdac( hObject, eventdata )
    if(~isfield(ss.paths,'gdacDir') || (~ischar(ss.paths.gdacDir)))
        ss.paths.gdacDir = ss.paths.codeDir;
    end
    gdac_dir1 = uigetdir( ss.paths.gdacDir );
    if( gdac_dir1 )
        ss.paths.gdacDir = gdac_dir1;
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
    end
end
%   Set GDAC index file
function callback_gdac_index( hObject, eventdata )
    if( ~isfield(ss,'gdac') || ~isfield(ss.gdac,'indexFileDir') )
        ss.gdac.indexFileDir = ss.paths.codeDir;
    elseif(exist(ss.gdac.indexFileDir,'dir')~=7)
        ss.gdac.indexFileDir = ss.paths.codeDir;
    end
    [~,gdacIndxDirR] = uigetfile( [ss.gdac.indexFileDir,gdacIndexFn] );
    if( gdacIndxDirR )
        ss.gdac.indexFileDir = gdacIndxDirR;
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
    end
end
%
% Load Argo mat file
function callback_open_argo_mat( hObject, eventdata )
    [argoFnR,owcDirR] = uigetfile([ss.paths.owcDir,filesep,'*.mat']);
    if( argoFnR ~= 0 )
        argoFn = argoFnR;
        ss.paths.owcDir = owcDirR;
        save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
        [dd.trajTbl,dd.dataStr] = loadArgoMat([ss.paths.owcDir,filesep,argoFn]);
        dd.nProf = height(dd.trajTbl);
        dd.nLayers = size(dd.dataStr.PRES,1);
        ff = setFig; % Figure settings
        % Remove the profiles with no coordinates or time
        indxFin = isfinite(dd.trajTbl.latitude)&isfinite(dd.trajTbl.longitude)&isfinite(dd.trajTbl.mtime);
        if( ~all(indxFin) )
            [dd.trajTbl,dd.dataStr,dd.metaPar] = dataProfSel(dd.trajTbl,dd.dataStr,dd.metaPar,indxFin);
            dd.nProf = height(dd.trajTbl);
        end
        if( isempty(dd.trajTbl.latitude) || isempty(dd.trajTbl.longitude) )
            warndlg('\fontsize {16} Coordinates contain only NaN',...
                'Corrupted data', warndlg_opts);
            return
        end
        % Display file name
        hh.staticText_argoFn.String = ['File: ',argoFn];
        % The string with profiles and layers
        argoid_str = [ 'Argo # ', dd.trajTbl.platform_number{1},' / ',...
            num2str( dd.nProf ),' profiles / ',...
            num2str( dd.nLayers ),' layers' ];
        set(hh.staticText_argoid,'String', argoid_str );
        % Plot trajectory map
        delete(findobj(hh.hpax1, 'type','axes'))
        hh.hax1 = axes('Parent',hh.hpax1, 'Units','normalized', 'Position',axPos);
        plotTrajMap(dd.trajTbl, 'plotAxes',hh.hax1,'projection',ff.map.projection,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
            'colormap',ff.colormap,...
            'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
            'axesFontSize',ff.map.axesFontSize,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize );
        menu_figure_uncheck()
        set( findobj( 'Tag','Map' ), 'Checked','on' )
        if( ismember('data_mode',dd.trajTbl.Properties.VariableNames) )
            % The table with data mode 
            hh.staticText_data_mode.Visible = 'on';
            hh.uitable_data_mode.Visible = 'on';
            hh.uitable_data_mode.Data = setDataModeCell(dd.trajTbl);
            hh.uitable_data_mode.ColumnName = { [fsp1,'Mode'],...
                [fsp1,'N'],[fsp1,'Start'],[fsp1,'End'] };
            hh.uitable_data_mode.RowName = [];
        else
            hh.staticText_data_mode.Visible = 'off';
        end
        % Max depth with SAL
        PRES1 = dd.dataStr.PRES;
        PRES1(isnan(dd.dataStr.SAL)) = NaN;
        dd.trajTbl.maxPRESwPSAL = max(PRES1)';
        % The number of valid salinity scans in profiles
        dd.trajTbl.nvPSAL = sum(isfinite(dd.dataStr.SAL))';
        set( findobj( 'Tag','Figure' ), 'Enable','on' )
        set( findobj( 'Tag','Theta_levels' ), 'Enable','on' )
        set( findobj( 'Tag','GDAC' ), 'Enable','on' )
        if(isfield(dd,'climTbl'))
            set( findobj( 'Tag','Climatology' ), 'Enable','on' )
        end
        PTMP1 = dd.dataStr.PTMP;
        PTMP1(isnan(dd.dataStr.SAL)) = NaN;
        dd.thetaTbl = table( ceil(max(min(PTMP1))), NaN, NaN,   'VariableNames',...
            {'Theta','P_level','var_S'} );
        hh.staticText_Thetas.Visible = 'on';
        hh.utable_thetas.Visible = 'on';
        hh.utable_thetas.Data = table2array(dd.thetaTbl);
        hh.utable_thetas.ColumnName = {'Theta'};
        %
        % Is it OWC output? Check the folders and files
        dd.OWCoutput = false;
        dd.climRead = false;
        dd.owcStr = [];
        pathSel = strsplit(ss.paths.owcDir,filesep);
        if(isempty(pathSel{end}))
            pathSel(end) = [];
        end
        set( findobj( 'Tag', 'OWC_results' ), 'Enable','off' )
        if(strcmp(pathSel{end-1},'float_source') && strcmp(pathSel{end-2},'data'))
            owcPath = strjoin(pathSel(1:end-3),filesep);
            projDir = pathSel{end};
            if((exist([owcPath,filesep,'data',filesep,'float_mapped',...
                    filesep,projDir,filesep,'map_',argoFn],'file')==2) &&...
               (exist([owcPath,filesep,'data',filesep,'float_calib',...
                   filesep,projDir,filesep,'cal_',argoFn],'file')==2) &&...
               (exist([owcPath,filesep,'data',filesep,'float_calib',...
                   filesep,projDir,filesep,'calseries_',argoFn],'file')==2) )
                dd.OWCoutput = true;
                [ ~, ~, dd.owcStr ] = loadOWC( owcPath, projDir, argoFn );
                [Theta10, Theta_plevels10, ~, var_s_Thetalevels, Thetalevels] =...
                    find_10thetas( dd.dataStr.SAL, dd.dataStr.PTMP, dd.dataStr.PRES,...
                    dd.owcStr.la_ptmp, [], [], [], [], 0.5 );
                dd.thetaTbl = table( Theta10, Theta_plevels10, NaN(size(Theta10)),...
                    'VariableNames', {'Theta','P_level','var_S'} );
                for kTheta = 1:length(Theta10)
                    if(isfinite(Theta10(kTheta)))
                        dd.thetaTbl.var_S(kTheta) = var_s_Thetalevels(find(Thetalevels==Theta10(kTheta),1,'first'));
                    end
                end
                [~,indxSort] = sort(dd.thetaTbl.P_level,1,'ascend');
                dd.thetaTbl = dd.thetaTbl(indxSort,:);
                hh.staticText_Thetas.Visible = 'on';
                hh.utable_thetas.Visible = 'on';
                hh.utable_thetas.Data = table2array(dd.thetaTbl);
                hh.utable_thetas.ColumnName = {'Theta','P_level','var_S'};
                set( findobj( 'Tag', 'OWC_results' ), 'Enable','on' )
            end
        end
        menu_figure_enable()
    end
end
%
% Exit
function callback_exit( hObject, eventdata )
    save( [ss.paths.codeDir,filesep,ss.initSettingsFn], 'ss' )
    close( hh.hFig )
end
%
% Trajectory map
function callback_map( hObject, eventdata )
    delete(findobj(hh.hpax1, 'type','axes'))
    hh.hax1 = axes('Parent',hh.hpax1, 'Units','normalized','Position',axPos );
    plotTrajMap(dd.trajTbl, 'plotAxes',hh.hax1,'projection',ff.map.projection,...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
        'colormap',ff.colormap,...
        'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
        'axesFontSize',ff.map.axesFontSize,...
        'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize );
    menu_figure_uncheck()
    set( findobj( 'Tag','Map' ), 'Checked','on' )
end
%
% Theta profiles
function callback_prof_PTMP( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.PTMP(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotProf( dd.dataStr.PRES, dd.dataStr.PTMP, 'plotAxes',hh.hax1,...
            'xLab','Potential temperature (^oC)','mtime',dd.trajTbl.mtime,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize, 'fontSize',ff.fontSize,...
            'colormap',ff.colormap, 'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'yLim',ff.axis.presLim, 'xLim',ff.axis.ptmpLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','prof_PTMP' ), 'Checked','on' )
    end
end
%
% Salinity profiles
function callback_prof_SAL( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotProf( dd.dataStr.PRES, dd.dataStr.SAL, 'plotAxes',hh.hax1,...
            'xLab','Salinity','mtime',dd.trajTbl.mtime,... 
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'fontSize',ff.fontSize, 'colormap',ff.colormap,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'yLim',ff.axis.presLim, 'xLim',ff.axis.salLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','prof_SAL' ), 'Checked','on' )
    end
end
%
% TS diagram   
function callback_TS_diagr( hObject, eventdata )
    if((sum(isfinite(dd.dataStr.PRES(:)))==0)||(sum(isfinite(dd.dataStr.TEMP(:)))==0)||...
            (sum(isfinite(dd.dataStr.SAL(:)))==0))
        warndlg('\fontsize {16} No data!','No data',warndlg_opts);
    else
        delete(findobj(hh.hpax1, 'type', 'axes'))
        hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
        plotTS(dd.dataStr, 'plotAxes',hh.hax1, 'mtime',dd.trajTbl.mtime,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'fontSize',ff.fontSize, 'colormap',ff.colormap,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize,...
            'ptLim',ff.axis.ptmpLim, 'sLim',ff.axis.salLim );
        menu_figure_uncheck()
        set( findobj( 'Tag','TS_diagr' ), 'Checked','on' )
    end
end
%
%   Timeseries SAL / PRES
function callback_ts_SAL_PRES( hObject, eventdata )
    [ SALq, Xq1, Yq1 ] = calcTimeSeriesPres( dd.dataStr.SAL, dd.dataStr.PRES,...
        dd.trajTbl.mtime );
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    plotTimeSeriesPres( SALq, Xq1, Yq1, 'plotAxes',hh.hax1, 'vLab','Salinity',...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'fontSize',ff.fontSize, 'colormap',ff.colormap,...
        'xLim',[ff.time.startTime ff.time.endTime], 'yLim',ff.axis.presLim,...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval,...
        'vColLim',ff.col.salLim );
    if(ff.thetaShow)
        add2plotTimeSeriesPresContours( dd.dataStr.PTMP, dd.dataStr.PRES, dd.trajTbl.mtime,...
            dd.thetaTbl.Theta, 'plotAxes',hh.hax1,...
            'lineColor',ff.line.color, 'lineWidth',ff.line.width, 'lineStyle',ff.line.style )
    end
    if(ff.samplesShow)
        add2plotTimeSeriesSamples( dd.dataStr.SAL, dd.dataStr.PRES, dd.trajTbl.mtime,...
            'plotAxes',hh.hax1, 'markerSize',ff.marker.size,...
            'markerEdgeColor',ff.marker.edgeColor, 'markerFaceColor',ff.marker.faceColor );
    end
    menu_figure_uncheck()
    set( findobj( 'Tag','ts_SAL_PRES' ), 'Checked','on' )
end
%
%   Timeseries PTMP / PRES
function callback_ts_PTMP_PRES( hObject, eventdata )
    [ PTMPq, Xq1, Yq1 ] = calcTimeSeriesPres( dd.dataStr.PTMP, dd.dataStr.PRES,...
        dd.trajTbl.mtime );
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    plotTimeSeriesPres( PTMPq, Xq1, Yq1, 'plotAxes',hh.hax1,...
        'vLab','Potential temperature (^oC)',...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'fontSize',ff.fontSize, 'colormap',ff.colormap,...
        'xLim',[ff.time.startTime ff.time.endTime], 'yLim',ff.axis.presLim,...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval,...
        'vColLim',ff.col.ptmpLim );
    if(ff.thetaShow)
        add2plotTimeSeriesPresContours( dd.dataStr.PTMP, dd.dataStr.PRES, dd.trajTbl.mtime,...
            dd.thetaTbl.Theta, 'plotAxes',hh.hax1,...
            'lineColor',ff.line.color, 'lineWidth',ff.line.width, 'lineStyle',ff.line.style )
    end
    menu_figure_uncheck()
    set( findobj( 'Tag','ts_PTMP_PRES' ), 'Checked','on' )
end
%
%   Timeseries Theta gradient / PRES
function callback_ts_dPTMP_PRES( hObject, eventdata )
    [ PTMPq, Xq2, Yq2 ] = calcTimeSeriesPres( dd.dataStr.PTMP, dd.dataStr.PRES,...
        dd.trajTbl.mtime );
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    plotTimeSeriesPresGrad( PTMPq, Xq2, Yq2, 'plotAxes',hh.hax1,...
        'vLab','PTMP gradient (^oC / dbar)',...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'fontSize',ff.fontSize, 'vColLim',[-ff.col.ptmpGrLimA ff.col.ptmpGrLimA],...
        'contourLevels',ff.col.ptmpGrLineInt,...
        'xLim',[ff.time.startTime ff.time.endTime], 'yLim',ff.axis.presLim,...
        'timeTickStyle',ff.time.timeTickStyle, 'timeTickInterval',ff.time.timeTickInterval);
    if(ff.thetaShow)
        add2plotTimeSeriesPresContours( dd.dataStr.PTMP, dd.dataStr.PRES, dd.trajTbl.mtime,...
            dd.thetaTbl.Theta, 'plotAxes',hh.hax1,...
            'lineColor',ff.line.color, 'lineWidth',ff.line.width, 'lineStyle',ff.line.style )
    end
    menu_figure_uncheck()
    set( findobj( 'Tag','ts_dPTMP_PRES' ), 'Checked','on' )
end
%
%   Timeseries SAL / Theta
function callback_ts_SAL_PTMP( hObject, eventdata )
    [ SalAtPtmpInt, xMtime, yPtmp ] = calcSalAtPtmpInt( dd.dataStr.SAL, dd.dataStr.PTMP,...
        dd.dataStr.PRES, dd.trajTbl.mtime );
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    hh.hax1 = plotTimeSeriesPtmp( SalAtPtmpInt, xMtime, yPtmp, 'vLab','Salinity',...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'colormap',ff.colormap,...
        'fontSize',ff.fontSize, 'ptmpLim',ff.axis.ptmpLim,...
        'xLim',[ff.time.startTime ff.time.endTime],...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval,...
        'vColLim',ff.col.salLim );
    if(ff.thetaShow)
        for kAx = 1:length(hh.hax1)
            add2plotTimeSeriesPtmpLevels( dd.dataStr.PTMP, dd.trajTbl.mtime,...
                dd.thetaTbl.Theta, 'plotAxes',hh.hax1(kAx),...
                'lineColor',ff.line.color, 'lineWidth',ff.line.width, 'lineStyle',ff.line.style )
        end
    end
    menu_figure_uncheck()
    set( findobj( 'Tag','ts_SAL_PTMP' ), 'Checked','on' )   
end
%
% Timeseries SAL anomaly (vs median) / Theta
function callback_ts_SALa_PTMP_med( hObject, eventdata )
    [ SalAtPtmpInt, xMtime, yPtmp ] = calcSalAtPtmpInt( dd.dataStr.SAL, dd.dataStr.PTMP,...
        dd.dataStr.PRES, dd.trajTbl.mtime );
    % Calculate and subtract median SAL at each theta level
    SalAtPtmpInt_med = nanmedian( SalAtPtmpInt, 2 );
    SatThq_med2d = repmat( SalAtPtmpInt_med, 1, length(xMtime) );
    SalAtPtmpInt = SalAtPtmpInt - SatThq_med2d;
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    hh.hax1 = plotTimeSeriesPtmp( SalAtPtmpInt, xMtime, yPtmp, 'vLab','Salinity',...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'fontSize',ff.fontSize, 'ptmpLim',ff.axis.ptmpLim,...
        'vLines',ff.col.salAlineInt, 'vColLim',[-ff.col.salAlimA ff.col.salAlimA],...
        'xLim',[ff.time.startTime ff.time.endTime],...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval);
    if(ff.thetaShow)
        for kAx = 1:length(hh.hax1)
            add2plotTimeSeriesPtmpLevels( dd.dataStr.PTMP, dd.trajTbl.mtime,...
                dd.thetaTbl.Theta, 'plotAxes',hh.hax1(kAx),...
                'lineColor',ff.line.color, 'lineWidth',ff.line.width, 'lineStyle',ff.line.style )
        end
    end
    menu_figure_uncheck()
    set( findobj( 'Tag','ts_SALa_PTMP_med' ), 'Checked','on' )   
end
%
%   Timeseries SAL anomaly (vs climatology) / Theta
function callback_ts_SALa_PTMP_clim( hObject, eventdata )
    [ SalAtPtmpInt, xMtime, yPtmp ] = calcSalAtPtmpInt( dd.dataStr.SAL, dd.dataStr.PTMP,...
        dd.dataStr.PRES, dd.trajTbl.mtime );
    SalAtPtmpIntCLM = calcSalAtPtmpInt( dd.CLMdata.SAL,...
        dd.CLMdata.PTMP, dd.CLMdata.PRES, dd.trajTbl.mtime, 'yPtmp',yPtmp );
    SalAtPtmpInt = SalAtPtmpInt - SalAtPtmpIntCLM;
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    hh.hax1 = plotTimeSeriesPtmp( SalAtPtmpInt, xMtime, yPtmp, 'vLab','Salinity',...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'fontSize',ff.fontSize, 'ptmpLim',ff.axis.ptmpLim,...
        'vLines',ff.col.salAlineInt, 'vColLim',[-ff.col.salAlimA ff.col.salAlimA],...
        'xLim',[ff.time.startTime ff.time.endTime],...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval);
    if(ff.thetaShow)
        for kAx = 1:length(hh.hax1)
            add2plotTimeSeriesPtmpLevels( dd.dataStr.PTMP, dd.trajTbl.mtime,...
                dd.thetaTbl.Theta, 'plotAxes',hh.hax1(kAx),...
                'lineColor',ff.line.color, 'lineWidth',ff.line.width, 'lineStyle',ff.line.style )
        end
    end
    menu_figure_uncheck()
    set( findobj( 'Tag','ts_SALa_PTMP_clim' ), 'Checked','on' )   
end
%
%   OWC fit coefficients
function callback_owc_ts_offsets( hObject, eventdata )
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    plotOWCsOffsets( dd.owcStr.owcTbl, 'plotAxes',hh.hax1,...
        'title',ff.title, 'titleFontSize',ff.titleFontSize, 'fontSize',ff.fontSize,...
        'xLim',[ff.time.startTime ff.time.endTime],...
        'yLim',ff.axis.salAlim,...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval);
    menu_figure_uncheck()
    set( findobj( 'Tag','OWC_ts_offsets' ), 'Checked','on' )   
end
%
% OWC fit: timeseries with "bubbles"
function callback_owc_ts_bb( hObject, eventdata )
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    plotOWCsOffsetsBB( dd.owcStr.owcTbl, 'plotAxes',hh.hax1, ...
        'title',ff.title, 'titleFontSize',ff.titleFontSize, 'fontSize',ff.fontSize,...
        'xLim',[ff.time.startTime ff.time.endTime],...
        'yLim',ff.axis.salAlim,...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval);
    menu_figure_uncheck()
    set( findobj( 'Tag','OWC_ts_bb' ), 'Checked','on' )   
end
%
%   OWC fit: map with "bubbles"
function callback_owc_bb_map( hObject, eventdata )
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    plotOWCsOffsetsBBmap( dd.owcStr.owcTbl, 'plotAxes',hh.hax1,'projection',ff.map.projection,...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
        'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
        'axesFontSize',ff.map.axesFontSize );
    menu_figure_uncheck()
    set( findobj( 'Tag','OWC_bb_map' ), 'Checked','on' )   
end
%
% OWC reference-climatology
function callback_owc_clim( hObject, eventdata )
    [ SalAtPtmpInt, xMtime, yPtmp ] = calcSalAtPtmpInt( dd.owcStr.la_mapped_sal,...
        dd.owcStr.la_ptmp, dd.dataStr.PRES, dd.owcStr.owcTbl.mtime );
    SalAtPtmpIntClim = calcSalAtPtmpInt( dd.CLMdata.SAL, dd.CLMdata.PTMP, dd.CLMdata.PRES,...
        dd.owcStr.owcTbl.mtime, 'yPtmp',yPtmp );
    aSalAtPtmpInt = SalAtPtmpInt - SalAtPtmpIntClim;
    %
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    hh.hax1 = plotTimeSeriesPtmp( aSalAtPtmpInt, xMtime, yPtmp, 'vLab','Salinity',...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'fontSize',ff.fontSize, 'ptmpLim',ff.axis.ptmpLim,...
        'vLines',ff.col.salAlineInt, 'vColLim',[-ff.col.salAlimA ff.col.salAlimA],...
        'xLim',[ff.time.startTime ff.time.endTime],...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval);
    if(ff.thetaShow)
        for kAx = 1:length(hh.hax1)
            add2plotTimeSeriesPtmpLevels( dd.dataStr.PTMP, dd.trajTbl.mtime,...
                dd.thetaTbl.Theta, 'plotAxes',hh.hax1(kAx),...
                'lineColor',ff.line.color, 'lineWidth',ff.line.width, 'lineStyle',ff.line.style )
        end
    end
    menu_figure_uncheck()
    set( findobj( 'Tag','OWC_ts_ref_clim' ), 'Checked','on' )   
end
%
%   OWC fit and reference-climatology
function callback_owc_ts_clim( hObject, eventdata )
    if(is2lims(ff.axis.ptmpLim))
        yPtmp = linspace(ff.axis.ptmpLim(1),ff.axis.ptmpLim(2),100);
    else
        yPtmp = [];
    end
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    hh.hax1 = plotOWCsOffsetsClim( dd.owcStr, dd.dataStr.PRES, dd.CLMdata,...
        'title',ff.title, 'titleFontSize',ff.titleFontSize,...
        'fontSize',ff.fontSize,...
        'yLim',ff.axis.salAlim,...
        'yPtmp',yPtmp,...
        'vColLim',[-ff.col.salAlimA ff.col.salAlimA],...
        'vLineInt',ff.col.salAlineInt,...
        'xLim',[ff.time.startTime ff.time.endTime],...
        'timeTickStyle',ff.time.timeTickStyle,...
        'timeTickInterval',ff.time.timeTickInterval);
    add2plotTimeSeriesPtmpLevels( dd.dataStr.PTMP, dd.trajTbl.mtime,...
        dd.thetaTbl.Theta, 'plotAxes',hh.hax1(2),...
        'lineColor',ff.line.color, 'lineWidth',ff.line.width, 'lineStyle',ff.line.style )
    menu_figure_uncheck()
    set( findobj( 'Tag','OWC_ts_off_ref_clim' ), 'Checked','on' )   
end
%
%   Export the plot to png file
function callback_export_plot( hObject, eventdata )
    [save_fn,save_path] = uiputfile( '*.png' );
    if( save_fn )
        imageData = screencapture( hh.hpax1 );       
        imwrite( imageData,  [ save_path, save_fn ] )
    end
end
%
%   Open the plot in new figure
function callback_new_figure( hObject, eventdata )
    h_new_fig = figure( 'Position',[200 200 672 560] );
    copyobj( hh.hax1, h_new_fig ) 
    colormap(h_new_fig,ff.colormapA)
end
% Demonstrate the table with OWC results 
function callback_OWCtable( hObject, eventdata )
    hFigModal = figure( 'Position',[100,200,550,700],'WindowStyle','modal',...
        'Name','OWC calibration', 'NumberTitle','off');
    uitable_OWC_results = uitable( hFigModal,...
            'Position',[20 70 500 620],...
            'FontSize',14, 'Visible','on', ...
            'ColumnWidth',{100,80,80,80,80} );
    uitable_OWC_results.Data = table2cell(dd.owcStr.owcTbl(:,[2,7:10]));
    uitable_OWC_results.ColumnName = dd.owcStr.owcTbl.Properties.VariableNames([2,7:10]);
    uitable_OWC_results.ColumnEditable = false(1,5);
    uicontrol( hFigModal,'Style','pushbutton','FontSize',16,...
            'String','Close', 'Position',[400, 25, 100, 30],...
            'Callback',@callback_OWCtable_close );
    function callback_OWCtable_close( hObject, eventdata )
        close( hFigModal )
    end
end
% Get climatology along the float's trajectory
function callback_getClim( hObject, eventdata )
    hFigModal = figure( 'Position',[200,350,750,150],'WindowStyle','modal',...
        'Name','Extract climatology', 'NumberTitle','off');
    uicontrol( hFigModal, 'Style','text',...
        'Position',[20 80 150 30],...
        'FontSize',16, 'HorizontalAlignment','left',...
        'String','Climatology source' );
    kClim = find(strcmp(dd.climTbl.code,dd.climSel),1,'first');
    hpop_clim_source = uicontrol( hFigModal,'Style','popupmenu',...
        'Position',[180 80 560 30],...
        'String',dd.climTbl.climatology(dd.climTbl.exist), 'FontSize',16, ...
        'Value',kClim );
    %
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Select',...
        'Position',[100 20 100 30],...
        'FontSize',16, 'Callback',@callback_accept_clim );        
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Cancel',...
        'Position',[400 20 100 30],...
        'FontSize',16, 'Callback',@callback_cancel_clim );  
    function callback_accept_clim( hObject, eventdata )
        dd.climSel = dd.climTbl.code{hpop_clim_source.Value};
        dd.CLMdata = climAlongTraj( ss.paths.climDir, dd.climTbl(dd.climTbl.exist,:),...
            dd.climSel, dd.trajTbl );
        close( hFigModal )
        dd.climRead = true;
        menu_figure_enable()
        feval( get(findobj('Checked','on'),'callback') )
    end
    function callback_cancel_clim( hObject, eventdata )
        close( hFigModal )
    end
end
%
function callback_thetas( hObject, eventdata )
%
    hFigModal = figure( 'Position',[200,350,750,180],'WindowStyle','modal',...
            'Name','Select theta levels', 'NumberTitle','off');
    %
    uicontrol( hFigModal, 'Style','text',...
        'Position',[20 120 100 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Theta levels' );
    hed_theta_levels = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',sprintf('%.4f ',dd.thetaTbl.Theta'), 'Position',[20,80,700,30],...
        'Callback',@callback_theta_level, 'HorizontalAlignment','left' );
    thetasOWC = true;
    function callback_theta_level( hObject, eventdata )
        Theta_levels = str2double( strsplit( strtrim( hed_theta_levels.String ) ) );
        if( isempty( Theta_levels ) || any( isnan( Theta_levels ) ) )
            hed_theta_levels.String = sprintf('%.4f ',dd.thetaTbl.Theta');
        end
        thetasOWC = false;
    end
    %
    hpb_10thetas = uicontrol( hFigModal, 'Style','pushbutton', 'String','10 OWC thetas',...
        'Position',[100 20 200 30],...
        'FontSize',16, 'Callback',@callback_10OWthetas, 'Enable','off' );
    if(dd.OWCoutput)
        set(hpb_10thetas,'Enable','on');
    end
    function callback_10OWthetas( hObject, eventdata )
%         try
            [Theta10, Theta_plevels10, ~, var_s_Thetalevels, Thetalevels] =...
                find_10thetas( dd.dataStr.SAL, dd.dataStr.PTMP, dd.dataStr.PRES,...
                dd.owcStr.la_ptmp, [], [], [], [], 0.5 );
            dd.thetaTbl = table( Theta10, Theta_plevels10, NaN(size(Theta10)),...
                    'VariableNames', {'Theta','P_level','var_S'} );
            for kTheta = 1:length(Theta10)
                dd.thetaTbl.var_S(kTheta) = var_s_Thetalevels(find(Thetalevels==Theta10(kTheta),1,'first'));
            end
            hed_theta_levels.String = sprintf('%.4f ',dd.thetaTbl.Theta');
            thetasOWC = true;
%         catch
%             thetasOWC = false;
%         end
    end
    %
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Select',...
        'Position',[400 20 100 30],...
        'FontSize',16, 'Callback',@callback_accept_thetas );        
    function callback_accept_thetas( hObject, eventdata )
        Theta_levels = str2double( strsplit( strtrim( hed_theta_levels.String ) ) );
        close(hFigModal)
        if( ~thetasOWC )
            if( ~( isempty(Theta_levels)||any(isnan(Theta_levels)) ) )
                dd.thetaTbl = table( Theta_levels','VariableNames',{'Theta'} );
            end
        end
        hh.utable_thetas.Data = table2array(dd.thetaTbl);
        ff.thetaShow = true;
        feval( get(findobj('Checked','on'),'callback') )
    end
    %
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Cancel',...
        'Position',[600 20 100 30],...
        'FontSize',16, 'Callback',@callback_cancel_thetas );  
    function callback_cancel_thetas( hObject, eventdata )
        close( hFigModal )
    end
end
%
%   Select polygon from the map
function callback_GDAC_polygon( hObject, eventdata )
    delete(findobj(hh.hpax1, 'type', 'axes'))
    hh.hax1 = axes( 'Parent',hh.hpax1, 'Units','normalized','Position',axPos);
    menu_figure_uncheck()
    if(dd.OWCoutput)
        hh.hax1 = plotOWCsOffsetsBBmap( dd.owcStr.owcTbl, 'plotAxes',hh.hax1,...
            'projection',ff.map.projection,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
            'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
            'axesFontSize',ff.map.axesFontSize );
        set( findobj( 'Tag','OWC_bb_map' ), 'Checked','on' ) 
    else
        hh.hax1 = plotTrajMap(dd.trajTbl, 'plotAxes',hh.hax1,'projection',ff.map.projection,...
            'title',ff.title, 'titleFontSize',ff.titleFontSize,...
            'latLim',ff.map.latLim, 'lonLim',ff.map.lonLim,...
            'colormap',ff.colormap,...
            'landcolor',ff.map.landcolor, 'GSHHS',ff.map.GSHHS,...
            'axesFontSize',ff.map.axesFontSize,...
            'nLeg',ff.nLeg, 'legendFontSize',ff.legendFontSize );
        set( findobj( 'Tag','Map' ), 'Checked','on' ) 
    end
    %
    [X,Y] = ginput;
    [ff.gdac.longitude,ff.gdac.latitude]= m_xy2ll(X,Y);
    m_line(ff.gdac.longitude([1:end,1]),ff.gdac.latitude([1:end,1]),...
        'Color',[0 0.5 0],'LineWidth',3,...
        'Marker','o','MarkerSize',10,...
        'MarkerFaceColor',[0 0.5 0], 'MarkerEdgeColor','k');
    set( findobj( 'Tag','GDAC_find_floats' ), 'Enable','on' )
    indxIn = inpolygon(dd.trajTbl.longitude,dd.trajTbl.latitude,...
        ff.gdac.longitude,ff.gdac.latitude);
    ff.gdac.startTime = min(dd.trajTbl.mtime(indxIn));
    ff.gdac.endTime = max(dd.trajTbl.mtime(indxIn));
end
%
function callback_GDAC_find_floats( hObject, eventdata )
    hFigModal = figure( 'Position',[100,50,450,660],'WindowStyle','modal',...
            'Name','Find Argo floats', 'NumberTitle','off');
    %
    uicontrol( hFigModal, 'Style','text', 'Position',[20 610 250 30],...
        'FontSize',16, 'HorizontalAlignment','left', 'String','Start time' );
    hed_gdac_start = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',datestr(ff.gdac.startTime,24), 'Position',[100,615,100,30],...
        'Callback',@callback_Date_start, 'Enable','on' );
    uicontrol( hFigModal, 'Style','text', 'Position',[220 610 250 30],...
        'FontSize',16, 'HorizontalAlignment','left', 'String','End time' );
    hed_gdac_end = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',datestr(ff.gdac.endTime,24), 'Position',[300,615,100,30],...
        'Callback',@callback_Date_end, 'Enable','on' );
    uicontrol( hFigModal, 'Style','text', 'Position',[20 570 250 30],...
        'FontSize',16, 'HorizontalAlignment','left', 'String','Ocean' );
    kOcean = find(strcmp({oceansList{1,:}},ff.gdac.ocean ),1,'first');
    hpop_ocean = uicontrol( hFigModal,'Style','popupmenu',...
        'Position',[100 570 200 30], 'String',{oceansList{2,:}}, 'FontSize',16, ...
        'Value',kOcean );
    GDACindFileExists = (exist([ss.gdac.indexFileDir,gdacIndexFn],'file')==2);
    if( GDACindFileExists )
        useGDAC = 'on';
    else
        useGDAC = 'off';
    end
    hcb_useGDAC = uicontrol( hFigModal,'Style','checkbox', 'FontSize',16,...
        'Position',[20,535,250,30], 'String','Use existing index file',...
        'Value',GDACindFileExists, 'Enable',useGDAC );
    uicontrol( hFigModal, 'Style','pushbutton','Position',[20 490 380 30],......
        'String','Find Argo floats in the contoured area',...
        'FontSize',16, 'Callback',@callback_find_in_GDAC ); 
    uicontrol( hFigModal, 'Style','pushbutton','Position',[20 450 380 30],......
        'String','Set directory for GDAC files',...
        'FontSize',16, 'Callback',@callback_dir_gdac );
    hpb_gdac_get = uicontrol( hFigModal, 'Style','pushbutton','Position',[20 410 380 30],......
        'String','Download files from GDAC', 'Enable','off',...
        'FontSize',16, 'Callback',@callback_gdac_get );
    htext_gdac_floats = uicontrol( hFigModal, 'Style','text', 'Position',[20 370 250 30],...
        'FontSize',16, 'HorizontalAlignment','left', 'String','', 'Visible','off');
    utable_gdac = uitable( hFigModal, 'FontSize',16, 'Visible','off',...
        'Position',[20 60 400 300], 'ColumnWidth',{100,60,100,50},...
        'ColumnEditable', [false,false,false,true ] );
    %
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Close',...
        'Position',[300 20 100 30],...
        'FontSize',16, 'Callback',@callback_close_GDAC );  
    function callback_find_in_GDAC( hObject, eventdata )
        ff.gdac.startTime = datenum( hed_gdac_start.String,'dd/mm/yyyy' );
        ff.gdac.endTime = datenum( hed_gdac_end.String,'dd/mm/yyyy' );
        ff.gdac.ocean = oceansList{1,hpop_ocean.Value};
        if(hcb_useGDAC.Value)
            dd.floatTbl = gdacFloatsFind( ff.gdac.latitude, ff.gdac.longitude,...
                [ff.gdac.startTime ff.gdac.endTime], 'indxFilePath',...
                [ss.gdac.indexFileDir,gdacIndexFn], 'ocean',ff.gdac.ocean );
        else
            dd.floatTbl = gdacFloatsFind( ff.gdac.latitude, ff.gdac.longitude,...
                [ff.gdac.startTime ff.gdac.endTime],'saveDir',ss.paths.gdacDir,...
                'ocean',ff.gdac.ocean );
        end
        htext_gdac_floats.Visible = 'on';
        htext_gdac_floats.String = [num2str(height(dd.floatTbl)),' floats found'];
        utable_gdac.Visible = 'on';
        floatTbl = dd.floatTbl;
        floatTbl.nProf = num2str(floatTbl.nProf);
        floatTbl.sel = true(height(floatTbl),1);
        kFloat = find(strcmp(dd.floatTbl.PLATFORM_NO,dd.trajTbl.platform_number{1}));
        floatTbl.sel(kFloat) = false;
        utable_gdac.Data = table2cell(floatTbl);
        utable_gdac.ColumnName = floatTbl.Properties.VariableNames;
        if(height(dd.floatTbl)>0)
            hpb_gdac_get.Enable = 'on';
        end
    end
    function callback_gdac_get( hObject, eventdata )
        floatTbl = cell2table(utable_gdac.Data,...
            'VariableNames',[dd.floatTbl.Properties.VariableNames,'sel']);
        dd.floatTbl = floatTbl(floatTbl.sel,1:3);
        gdacFloatsGet( dd.floatTbl, 'saveDir',ss.paths.gdacDir )
    end
    function callback_close_GDAC( hObject, eventdata )
        close( hFigModal )
    end
    %
end
%   Other nested functions
function menu_figure_uncheck()
    for kmF2 = 1:height(menuFtbl)
        set( findobj( 'Tag',menuFtbl.Tag{kmF2} ), 'Checked','off' )
    end
end
%
function menu_figure_enable
% Enable menu elements
    for k_obj = 1:height(menuFtbl)
        menu_item = findobj( 'Tag', menuFtbl.Tag{k_obj} );
        set( menu_item, 'Enable','on' )
        if(~dd.OWCoutput && ~menuFtbl.owc(k_obj) )
            set( menu_item, 'Enable','off' )
        end
        if( ~dd.climRead && ~menuFtbl.clim(k_obj) )
            set( menu_item, 'Enable','off' )
        end
    end
end
%
function dataModeCell = setDataModeCell(trajTbl)
    dataModeCell = { 'D',0,'',''; 'A',0,'',''; 'R',0,'','' };
    for kMode = 1:3
        indxSel = strcmpi(dataModeCell{kMode,1}, cellstr(trajTbl.data_mode) );
        dataModeCell{kMode,2} = sum(indxSel);
        startTime = min(trajTbl.mtime(indxSel));
        if( isempty(startTime) )
            dataModeCell{kMode,3} = '';
        else
            dataModeCell{kMode,3} = datestr(startTime,1);
        end
        endTime = max( trajTbl.mtime(indxSel) );
        if( isempty(endTime) )
            dataModeCell{kMode,4} = '';
        else
            dataModeCell{kMode,4} = datestr(endTime,1);
        end
    end
end
%
% Figure properties
function callback_fig_properties( hObject, eventdata )
    hFigModal = figure( 'Position',[100,100,400,600],'WindowStyle','modal',...
            'Name','Map properties', 'NumberTitle','off');
    uicontrol( hFigModal, 'Style','text',...
        'Position',[20 550 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Title' );
    hed_argo_title = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',ff.title, 'Position',[70,555,300,30] );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 510 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Title font size' );
    hed_titleFontSize = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',num2str(ff.titleFontSize), 'Position',[180,515,100,30],...
        'Callback',@callback_numeric );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 470 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Axes font size' );
    hed_axFontSize = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',num2str(ff.fontSize), 'Position',[180,475,100,30],...
        'Callback',@callback_numeric );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 430 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','colormap' );
    kColormap = find(strcmp(colormapList,ff.colormap));
    hpum_colormap = uicontrol( hFigModal, 'Style','popupmenu',...
        'Position',[180 430 150 30],...
        'FontSize',16, 'String',colormapList, 'Value',kColormap );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 390 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Items in legend' );
    hed_nLeg = uicontrol( hFigModal,'Style','edit','FontSize',16,...
        'String',num2str(ff.nLeg), 'Position',[180,395,100,30],...
        'Callback',@callback_numeric );
    uicontrol( hFigModal, 'Style','text',...
        'Position',[50 350 250 30],'FontSize',16, 'HorizontalAlignment','left',...
        'String','Legend font size' );
    hed_legendFontSize = uicontrol( hFigModal,'Style','edit','FontSize',16,...
            'String',num2str(ff.legendFontSize), 'Position',[180,355,100,30],...
            'Callback',@callback_numeric );
   %
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Map',...
        'Position',[50 300 150 30],...
        'FontSize',16, 'Callback',@callback_map_properties );
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Y axis limits',...
        'Position',[50 260 150 30],...
        'FontSize',16, 'Callback',@callback_profiles_properties );
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Time axis',...
        'Position',[50 220 150 30],...
        'FontSize',16, 'Callback',@callback_timeseries_properties );
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Color limits',...
        'Position',[50 180 150 30],...
        'FontSize',16, 'Callback',@callback_color_limits );
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Theta levels',...
        'Position',[50 140 150 30],...
        'FontSize',16, 'Callback',@callback_theta_levels );
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Samples',...
        'Position',[50 100 150 30],...
        'FontSize',16, 'Callback',@callback_samples );
    %
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Select',...
        'Position',[80 20 100 30],...
        'FontSize',16, 'Callback',@callback_accept_properties );        
    uicontrol( hFigModal, 'Style','pushbutton', 'String','Cancel',...
        'Position',[200 20 100 30],...
        'FontSize',16, 'Callback',@callback_cancel_properties ); 
    %
    function callback_map_properties( hObject, eventdata )
        hFigMapPr = figure( 'Position',[300,150,350,400],'WindowStyle','modal',...
            'Name','Map properties', 'NumberTitle','off');
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 300 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Projection' );
        kProj = find(strcmp(projectionList,ff.map.projection),1,'first');
        hpum_projection = uicontrol( hFigMapPr, 'Style','popupmenu',...
            'Position',[100 300 230 30],...
            'FontSize',16, 'String',projectionList, 'Value',kProj );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 260 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','latLim' );
        hed_latLim = uicontrol( hFigMapPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.map.latLim), 'Position',[100,265,120,30],'Callback',@callback_is2lims );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 220 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','lonLim' );
        hed_lonLim = uicontrol( hFigMapPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.map.lonLim), 'Position',[100,225,120,30],'Callback',@callback_is2lims );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 180 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','landcolor' );
        hed_landcolor = uicontrol( hFigMapPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.map.landcolor), 'Position',[120,185,200,30],...
            'Callback',@callback_landcolor );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 140 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Coastline resolution' );
        kGSHHS = find(strncmp({GSHHSlist{1,:}},ff.map.GSHHS,1));
        hpum_GSHHS = uicontrol( hFigMapPr, 'Style','popupmenu',...
            'Position',[180 140 150 30],...
            'FontSize',16, 'String',{GSHHSlist{2,:}}, 'Value',kGSHHS );
        uicontrol( hFigMapPr, 'Style','text',...
            'Position',[20 100 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Map axes font size' );
        hed_mAxFontSize = uicontrol( hFigMapPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.map.axesFontSize), 'Position',[180,105,100,30],...
            'Callback',@callback_numeric );
        %
        uicontrol( hFigMapPr, 'Style','pushbutton', 'String','Select',...
            'Position',[50 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_map_properties );        
        uicontrol( hFigMapPr, 'Style','pushbutton', 'String','Cancel',...
            'Position',[200 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_map_properties ); 
        function callback_accept_map_properties( hObject, eventdata )
            ff.map.projection = projectionList{hpum_projection.Value};
            ff.map.latLim = str2num(hed_latLim.String);
            ff.map.lonLim = str2num(hed_lonLim.String);
            lcnum = str2num(hed_landcolor.String);
            if(~isempty(lcnum))
                ff.map.landcolor = lcnum;
            else
                ff.map.landcolor = hed_landcolor.String;
            end
            ff.map.GSHHS = GSHHSlist{1,hpum_GSHHS.Value};
            ff.map.axesFontSize = str2double(hed_mAxFontSize.String);
            close( hFigMapPr )
        end
        function callback_cancel_map_properties( hObject, eventdata )
            close( hFigMapPr )
        end
        function callback_landcolor( hObject, eventdata )
            if(~iscolor(hObject.String))
                hObject.String = num2str(ff.map.landcolorDef);
            end
        end
    end
    %
    function callback_profiles_properties( hObject, eventdata )
        hFigPrfPr = figure( 'Position',[120,150,400,310],'WindowStyle','modal',...
            'Name','Profile properties', 'NumberTitle','off');
        %
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 260 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Pressure' );
        hed_presLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.axis.presLim), 'Position',[200,265,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 220 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Theta' );
        hed_ptmpLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.axis.ptmpLim), 'Position',[200,225,120,30],...
            'Callback',@callback_is3lims );
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 180 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Salinity' );
        hed_salLim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.axis.salLim), 'Position',[200,185,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigPrfPr, 'Style','text',...
            'Position',[20 140 250 30],'FontSize',16, 'HorizontalAlignment','left',...
            'String','Salinity anomaly' );
        hed_salAlim = uicontrol( hFigPrfPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.axis.salAlim), 'Position',[200,145,120,30],...
            'Callback',@callback_is2lims );
        %
        uicontrol( hFigPrfPr, 'Style','pushbutton', 'String','Select',...
            'Position',[60 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_prof_properties );        
        uicontrol( hFigPrfPr, 'Style','pushbutton', 'String','Cancel',...
            'Position',[240 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_prof_properties ); 
        function callback_accept_prof_properties( hObject, eventdata )
            ff.axis.presLim = str2num(hed_presLim.String);
            ff.axis.ptmpLim = str2num(hed_ptmpLim.String);
            ff.axis.salLim = str2num(hed_salLim.String);
            ff.axis.salAlim = str2num(hed_salAlim.String);
            close( hFigPrfPr )
        end
        function callback_cancel_prof_properties( hObject, eventdata )
            close( hFigPrfPr )
        end
    end
    %
    function callback_timeseries_properties( hObject, eventdata )
        hFigTsPr = figure( 'Position',[100,150,350,280],'WindowStyle','modal',...
            'Name','Time series properties', 'NumberTitle','off');
        %
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 220 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Start time' );
        hed_Date_start = uicontrol( hFigTsPr,'Style','edit','FontSize',16,...
            'String',datestr(ff.time.startTime,24), 'Position',[150,225,100,30],...
            'Callback',@callback_Date_start, 'Enable','on' );
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 180 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','End time' );
        hed_Date_end = uicontrol( hFigTsPr,'Style','edit','FontSize',16,...
            'String',datestr(ff.time.endTime,24), 'Position',[150,185,100,30],...
            'Callback',@callback_Date_end, 'Enable','on' );
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 140 150 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','timeTickStyle' );
        kTimeTickStyle = find(strcmp(timeTicksList,ff.time.timeTickStyle));
        hpum_TimeTickStyle = uicontrol( hFigTsPr, 'Style','popupmenu',...
            'Position',[180 140 150 30],...
            'FontSize',16, 'String',timeTicksList, 'Value',kTimeTickStyle );
        uicontrol( hFigTsPr, 'Style','text', 'Position',[20 100 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','timeTickInterval' );
        hed_timeTickInterval = uicontrol( hFigTsPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.time.timeTickInterval), 'Position',[180,105,120,30],...
            'Callback',@callback_numeric );
        uicontrol( hFigTsPr, 'Style','pushbutton', 'String','Select',...
            'Position',[50 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_ts_properties );        
        uicontrol( hFigTsPr, 'Style','pushbutton', 'String','Cancel',...
            'Position',[200 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_ts_properties ); 
        function callback_accept_ts_properties( hObject, eventdata )
            ff.time.startTime = datenum( hed_Date_start.String,'dd/mm/yyyy' );
            ff.time.endTime = datenum( hed_Date_end.String,'dd/mm/yyyy' );
            ff.time.timeTickStyle = timeTicksList{hpum_TimeTickStyle.Value};
            ff.time.timeTickInterval = str2num(hed_timeTickInterval.String);
            if(isempty(ff.time.timeTickInterval))
                ff.time.timeTickInterval = 1;
            end
%             ff.time.maxPRESwPSALlim = str2num(hed_maxPRESwPSALlim.String);
%             ff.time.nvPSALlim = str2num(hed_nvPSALlim.String);
            close( hFigTsPr )
        end
        function callback_cancel_ts_properties( hObject, eventdata )
            close( hFigTsPr )
        end
    end
    %
    function callback_color_limits( hObject, eventdata )
        hFigColPr = figure( 'Position',[100,150,450,280],'WindowStyle','modal',...
            'Name','Color properties', 'NumberTitle','off');
        %
        uicontrol( hFigColPr, 'Style','text', 'Position',[20 220 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Salinity' );
        hed_salLim = uicontrol( hFigColPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.col.salLim), 'Position',[180,225,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigColPr, 'Style','text', 'Position',[20 180 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Theta' );
        hed_ptmpLim = uicontrol( hFigColPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.col.ptmpLim), 'Position',[180,185,120,30],...
            'Callback',@callback_is2lims );
        uicontrol( hFigColPr, 'Style','text', 'Position',[20 140 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Theta gradient (abs)' );
        hed_ptmpGrLim = uicontrol( hFigColPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.col.ptmpGrLimA), 'Position',[180,145,80,30],...
            'Callback',@callback_numeric );
        uicontrol( hFigColPr, 'Style','text', 'Position',[280 140 100 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Interval' );
        hed_ptmpGrLineInt = uicontrol( hFigColPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.col.ptmpGrLineInt), 'Position',[350,145,80,30],...
            'Callback',@callback_numeric );
        
        uicontrol( hFigColPr, 'Style','text', 'Position',[20 100 250 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Salinity anomaly (abs)' );
        hed_salAlim = uicontrol( hFigColPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.col.salAlimA), 'Position',[180,105,80,30],...
            'Callback',@callback_numeric );
        uicontrol( hFigColPr, 'Style','text', 'Position',[280 100 100 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Interval' );
        hed_salAlineInt = uicontrol( hFigColPr,'Style','edit','FontSize',16,...
            'String',num2str(ff.col.salAlineInt), 'Position',[350,105,80,30],...
            'Callback',@callback_numeric );
        
        
        uicontrol( hFigColPr, 'Style','pushbutton', 'String','Select',...
            'Position',[50 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_col_properties ); 
        
        uicontrol( hFigColPr, 'Style','pushbutton', 'String','Cancel',...
            'Position',[200 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_col_properties ); 
        %
        function callback_accept_col_properties( hObject, eventdata )
            ff.col.salLim = str2num(hed_salLim.String);
            ff.col.ptmpLim = str2num(hed_ptmpLim.String);
            ff.col.ptmpGrLimA = abs(str2num(hed_ptmpGrLim.String));
            ff.col.ptmpGrLineInt = str2num(hed_ptmpGrLineInt.String);
            ff.col.salAlimA = str2num(hed_salAlim.String);
            ff.col.salAlineInt = str2num(hed_salAlineInt.String);
            close( hFigColPr )
        end
        function callback_cancel_col_properties( hObject, eventdata )
            close( hFigColPr )
        end
    end
    %
    function callback_theta_levels( hObject, eventdata )
        hFigThetas = figure( 'Position',[100,150,450,280],'WindowStyle','modal',...
            'Name','Theta levels', 'NumberTitle','off');
        %
        hcb_thetaShow = uicontrol( hFigThetas,'Style','checkbox',...
            'FontSize',16,  'Position',[20,230,150,30], 'String','Show theta levels',...
            'Value',ff.thetaShow );
        uicontrol( hFigThetas, 'Style','text', 'Position',[20 190 100 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Line color' );
        kColor = find(strcmp(colorList(1,:),ff.line.color),1,'first');
        hpum_lineColor = uicontrol( hFigThetas, 'Style','popupmenu',...
            'Position',[180 190 150 30],...
            'FontSize',16, 'String',colorList(2,:), 'Value',kColor );
        uicontrol( hFigThetas, 'Style','text', 'Position',[20 150 100 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Line width' );
        hed_lineWidth = uicontrol( hFigThetas,'Style','edit','FontSize',16,...
            'String',num2str(ff.line.width), 'Position',[180,155,80,30],...
            'Callback',@callback_lineWidth );
        uicontrol( hFigThetas, 'Style','text', 'Position',[20 110 100 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Line style' );
        kStyle = find(strcmp(lineStyleList(1,:),ff.line.style),1,'first');
        hpum_lineStyle = uicontrol( hFigThetas, 'Style','popupmenu',...
            'Position',[180 110 150 30],...
            'FontSize',16, 'String',lineStyleList(2,:), 'Value',kStyle );
        uicontrol( hFigThetas, 'Style','pushbutton', 'String','Select',...
            'Position',[50 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_theta_show ); 
        uicontrol( hFigThetas, 'Style','pushbutton', 'String','Cancel',...
            'Position',[200 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_theta_show ); 
        function callback_accept_theta_show( hObject, eventdata )
            ff.thetaShow = hcb_thetaShow.Value;
            ff.line.color = colorList{1,hpum_lineColor.Value};
            ff.line.width = str2num(hed_lineWidth.String);
            ff.line.style = lineStyleList{1,hpum_lineStyle.Value};
            close(hFigThetas)
        end
        function callback_cancel_theta_show( hObject, eventdata )
            close(hFigThetas)
        end
    end
    %
    function callback_samples( hObject, eventdata )
        hFigSamples = figure( 'Position',[100,150,450,280],'WindowStyle','normal',...%modal',...
            'Name','Theta levels', 'NumberTitle','off');
        %
        hcb_samplesShow = uicontrol( hFigSamples,'Style','checkbox',...
            'FontSize',16,  'Position',[20,230,150,30], 'String','Show samples',...
            'Value',ff.samplesShow );
        uicontrol( hFigSamples, 'Style','text', 'Position',[20 190 100 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Marker size' );
        hed_markerSize = uicontrol( hFigSamples,'Style','edit','FontSize',16,...
            'String',num2str(ff.marker.size), 'Position',[180,195,80,30],...
            'Callback',@callback_lineWidth );
        uicontrol( hFigSamples, 'Style','text', 'Position',[20 150 100 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Edge color' );
        kColor1 = find(strcmp(colorList(1,:),ff.marker.edgeColor),1,'first');
        hpum_edgeColor = uicontrol( hFigSamples, 'Style','popupmenu',...
            'Position',[180 150 150 30],...
            'FontSize',16, 'String',colorList(2,:), 'Value',kColor1 );
        uicontrol( hFigSamples, 'Style','text', 'Position',[20 110 100 30],...
            'FontSize',16, 'HorizontalAlignment','left', 'String','Face color' );
        kColor2 = find(strcmp(colorList(1,:),ff.marker.faceColor),1,'first');
        hpum_faceColor = uicontrol( hFigSamples, 'Style','popupmenu',...
            'Position',[180 110 150 30],...
            'FontSize',16, 'String',colorList(2,:), 'Value',kColor2 );
        uicontrol( hFigSamples, 'Style','pushbutton', 'String','Select',...
            'Position',[50 20 100 30],...
            'FontSize',16, 'Callback',@callback_accept_samples_show ); 
        uicontrol( hFigSamples, 'Style','pushbutton', 'String','Cancel',...
            'Position',[200 20 100 30],...
            'FontSize',16, 'Callback',@callback_cancel_samples_show ); 
        function callback_accept_samples_show( hObject, eventdata )
            ff.samplesShow = hcb_samplesShow.Value;
            ff.marker.size = str2num(hed_markerSize.String);
            ff.marker.edgeColor = colorList{1,hpum_edgeColor.Value};
            ff.marker.faceColor = colorList{1,hpum_faceColor.Value};
            close(hFigSamples)
        end
        function callback_cancel_samples_show( hObject, eventdata )
            close(hFigSamples)
        end
    end
    %
    function callback_accept_properties( hObject, eventdata )
        ff.title = hed_argo_title.String;
        ff.titleFontSize = str2num(hed_titleFontSize.String);
        if(isempty(ff.titleFontSize))
            ff.titleFontSize = 20;
        end
        ff.fontSize = str2num(hed_axFontSize.String);
        if(isempty(ff.fontSize))
            ff.fontSize = 12;
        end
        ff.colormap = colormapList{hpum_colormap.Value};
        ff.nLeg = str2num(hed_nLeg.String);
        if(isempty(ff.nLeg))
            ff.nLeg = 10;
        end
        ff.legendFontSize = str2num(hed_legendFontSize.String);
        if(isempty(ff.legendFontSize))
            ff.legendFontSize = 12;
        end
        close( hFigModal )
        feval( get(findobj('Checked','on'),'callback') )
    end
    %
    function callback_cancel_properties( hObject, eventdata )
        close( hFigModal )
    end
end
%
function callback_is2lims( hObject, eventdata )
    if(~is2lims(str2num(hObject.String)))
        hObject.String = [];
    end
end
%
function callback_is3lims( hObject, eventdata )
    if(~is3lims(str2num(hObject.String)))
        hObject.String = [];
    end
end
%
function callback_numeric( hObject, eventdata )
    x = str2double(hObject.String);
    if(isnan(x))
        hObject.String = [];
    end
end
%
function callback_Date_start( hObject, eventdata )
    try
        Date_start = datenum( hObject.String,'dd/mm/yyyy' );
    catch
        Date_start = dd.trajTbl.mtime(1);
    end
    hObject.String = datestr(Date_start,24);
end
%
function callback_Date_end( hObject, eventdata )
    try
        Date_end = datenum( hObject.String,'dd/mm/yyyy' );
    catch
        Date_end = dd.trajTbl.mtime(end);
    end
    hObject.String = datestr(Date_end,24);
end
%
function callback_lineWidth( hObject, eventdata )
    lineWidth = str2num(hObject.String);
    if(isempty(lineWidth))
        hObject.String = num2str(1);
    end
end
%
function yes = iscolor( x )
    if(ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) )
        yes = true;
    else
        x = str2num(x);
        if(all(isnumeric(x)) && (length(x)==3) && all(x<=1) && all(x>=0))
            yes = true;
        else
            yes = false;
        end
    end
end
%
function yes = is3lims(x)
    yes = false;
    if(all(isnumeric(x)))
        if( (length(x)==2) && (x(1)<x(2)) )
            yes = true;
        elseif( (length(x)==3) && (x(1)<x(2)) && (x(2)<x(3)) )
            yes = true;
        end
    end
end
%   Initial figure settings
function ff = setFig
    ff.title = dd.trajTbl.platform_number{1};
    ff.titleFontSize = 20;
    ff.fontSize = 12;
    ff.nLeg = 10;
    ff.legendFontSize = 12;
    ff.thetaShow = false;
    ff.samplesShow = false;
    ff.map.projection = 'Lambert Conformal Conic';
    ff.map.latLim = [];
    ff.map.lonLim = [];
    ff.colormap = 'jet';
    ff.colormapA = makeColormap4anom(64);
    ff.map.landcolorDef = [0.8 0.8 0.8];
    ff.map.landcolor = ff.map.landcolorDef;
    ff.map.GSHHS = '';
    ff.map.axesFontSize = 16;
    ff.axis.presLim = [];
    ff.axis.ptmpLim = [];
    ff.axis.salLim = [];
    ff.axis.salAlim = [];
    ff.time.startTime = min(dd.trajTbl.mtime);
    ff.time.endTime = max(dd.trajTbl.mtime);
    ff.time.timeTickStyle = timeTicksList{1};
    ff.time.timeTickInterval = 1;
    ff.col.salLim = [];
    ff.col.ptmpLim = [];
    ff.col.salAlimA = [];
    ff.col.salAlineInt = 0.01;
    ff.col.ptmpGrLimA = [];
    ff.col.ptmpGrLineInt = 0.05;
    ff.line.color = 'k';
    ff.line.width = 1;
    ff.line.style = '-';
    ff.marker.size = 1;
    ff.marker.edgeColor = 'k';
    ff.marker.faceColor = 'w';
    ff.gdac.latitude = [];
    ff.gdac.longitude = [];
    ff.gdac.startTime = min(dd.trajTbl.mtime);
    ff.gdac.endTime = max(dd.trajTbl.mtime);
    ff.gdac.ocean = '';
end
%
end

