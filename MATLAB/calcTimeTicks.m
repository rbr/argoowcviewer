function xTicks = calcTimeTicks( mtime, timeTickStyle, timeTickInterval )
%
%   Calculates ticks for X axis in datenum format
%
%   Syntax: xTicks = calcTimeTicks( mtime, timeTickStyle, timeTickInterval );
%
%   Input [Required]:
%       mtime - values X axis in datenum format
%       timeTickStyle - the style of X-axis (mtime) ticks:
%           {'years','months','days'(default)}
%       timeTickInterval - the interval between the time ticks 
%           (numeric, defailt 1)
%
%   Output:     
%       xTicks - numeric vector of ticks (datenum format)
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-25
%
p = inputParser;
addRequired(p,'mtime',@isnumeric);
addRequired(p,'timeTickStyle',@ischar);
addRequired(p,'timeTickInterval',@isnumeric);
parse(p, mtime, timeTickStyle, timeTickInterval )
mtime = p.Results.mtime;
timeTickStyle = p.Results.timeTickStyle;
timeTickInterval = p.Results.timeTickInterval;
%
switch timeTickStyle
    case 'days'
        xTicks = floor(min(mtime)) : timeTickInterval : ceil(max(mtime));
    case 'months'
        [YY1,MM1,~] = ymd(datetime(min(mtime),'ConvertFrom','datenum'));
        xTicks = NaN(ceil((max(mtime)-min(mtime))./30)+2,1);
%         xTicks = NaN(ceil((months(min(mtime),max(mtime))+1)/timeTickInterval)+2,1);
        for kTick = 1:length(xTicks)
            xTicks(kTick) = datenum(YY1,MM1+(kTick-1).*timeTickInterval,1);
        end
    case 'years'
        [YY1,~,~] = ymd(datetime(min(mtime),'ConvertFrom','datenum'));
        [YY2,~,~] = ymd(datetime(max(mtime),'ConvertFrom','datenum'));
        xTicks = NaN(ceil((YY2-YY1+1)/timeTickInterval)+2,1);
        for kTick = 1:length(xTicks)
            xTicks(kTick) = datenum(YY1+(kTick-1).*timeTickInterval,1,1);
        end
    otherwise
        error('Unknown timeTickStyle (years,months,days)')    
end
    
end


