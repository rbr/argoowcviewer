function ax = plotTimeSeriesPtmp( vAtPtmp, xMtime, yPtmp, varargin )
%
%   Plot the 2D image of the time series with potential temperature as Y axis
%       The image is divided into two with broken Y-axis
%
%   Syntax: plotTimeSeriesPtmp( vAtPtmp, xMtime, yPtmp,... )
%
%   Input [Required]:
%       vAtPtmp - parameter interpolated on 2D matrix with xMtime as X-axis 
%           and yPtmp as Y-axis (size [nY,nX]) 
%       xMtime - regular datenum vector, size nX
%       yPtmp - regular numeric vector of potential temeperature, size nY
%   Input [Optional]:
%       plotAxes - Axes object. Created by default. 
%           Used only when length(ptmpLim)==2
%       ptmpLim - vector of size 2 or 3. Sets the limits of Y-axes of 1 or 2
%           subplots
%       ptmpYfactor - the ratio between Y-scales in the upper and lower
%           subplots. Ignored when length(ptmpLim)==2. Default 4.
%       colormap - predefined colormap: 'jet' (default),'parula',etc.)
%           When length(vLines)==1, default colormap is 'anomalies' (b-w-r)
%       vColLim - the color range (for caxis) (numeric 2x1) (default auto)
%       xLim - X-axis limits (datenum, default min/max)
%       fontSize - font size of the axes (numeric) (default 12)
%       yLab - Y label (char) (default '\theta (^oC)')
%       yLabFontSize - font size of Y label (default = fontSize)
%       vLab - V (colorbar) label (char) (default '')
%       vLabFontSize - font size of the colorbar label (default = fontSize)
%       timeTickStyle - the style of X-axis (mtime) ticks:
%           {'auto' (default), 'years','months','days'}
%       timeTickInterval - the interval between the time ticks 
%           (numeric, defailt 1)
%       yAxesDiff - the vertical 'gap' between the two plots (ignored at
%           ptmpLim=2) (default 0.005)
%       title - figure title (char)
%       titleFontSize - title font size (numeric, default 20)
%       ptmpLevels - the potential temeprature levels to be shown (default [])
%       ptmpColor - the potential temeprature levels color (default 'k')
%       ptmpLineWidth - the potential temeprature levels line width
%           (default 1)
%       ptmpLineStyle - the potential temeprature levels line style
%           (default '-')
%       vLines - the levels of contour lines. Default [] - no lines. 
%           When length(vLines)==1, it is the interval beween lines,
%               both directions from 0, red (positive) and blue (negative).
%           When length(vLines)>1, vLines mean absolume values of contours
%       vLineColor - color of vLines (default 'k')
%       vLineWidth - width of vLines (default 1)
%       vLineStyle - type of vLines (default '-')
%
%   Output:     
%       ax - One or two axes objects (depending on ptmpLim). 
%           Can be used to make future modifications to the legend.
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-10
%
colormapList = {'parula','jet','hsv','hot','cool','spring','summer',...
    'autumn','winter','gray','bone','copper','pink'};
iscolormap = @(x) (all(isnumeric(x)) && (size(x,2)==3)) ||...
    (ischar(x) && (ismember(x,colormapList)) );
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
istimetick = @(x) (ischar(x) && (ismember(x,{'auto','years','months','days'})));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'vAtPtmp',@ismatrix);
addRequired(p,'xMtime',@isnumeric); 
addRequired(p,'yPtmp',@isnumeric); 
addParameter(p,'plotAxes',[],isaxes);
addParameter(p,'ptmpLim',[],@isnumeric);
addParameter(p,'ptmpYfactor',4,@isnumeric);
addParameter(p,'colormap','',iscolormap);
addParameter(p,'vColLim',[],is2nums);
addParameter(p,'xLim',[],is2nums);
addParameter(p,'fontSize',12,@isnumeric);
addParameter(p,'yLab','\theta (^oC)',@ischar);
addParameter(p,'yLabFontSize',[],@isnumeric);
addParameter(p,'vLab','',@ischar);
addParameter(p,'vLabFontSize',[],@isnumeric);
addParameter(p,'timeTickStyle','auto',istimetick);
addParameter(p,'timeTickInterval',1,@isnumeric);
addParameter(p,'yAxesDiff',0.005,@isnumeric);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
addParameter(p,'ptmpLevels',[],@isnumeric);
addParameter(p,'ptmpColor','k',iscolor);
addParameter(p,'ptmpLineWidth',1,@isnumeric);
addParameter(p,'ptmpLineStyle','-',@ischar);
addParameter(p,'vLines',[],@isnumeric);
addParameter(p,'vLineColor','k',iscolor);
addParameter(p,'vLineWidth',1,@isnumeric);
addParameter(p,'vLineStyle','-',@ischar);
parse(p, vAtPtmp, xMtime, yPtmp, varargin{:} )
vAtPtmp = p.Results.vAtPtmp;
xMtime = p.Results.xMtime;
yPtmp = p.Results.yPtmp;
ax = p.Results.plotAxes;
ptmpLim = p.Results.ptmpLim;
ptmpYfactor = p.Results.ptmpYfactor;
colorMap = p.Results.colormap;
vColLim = p.Results.vColLim;
xLim = p.Results.xLim;
if( isempty(xLim) )
    xLim = [min(xMtime) max(xMtime)];
end
fontSize = p.Results.fontSize;
yLab = p.Results.yLab;
yLabFontSize = p.Results.yLabFontSize;
if(isempty(yLabFontSize))
    yLabFontSize = fontSize;
end
vLab = p.Results.vLab;
vLabFontSize = p.Results.vLabFontSize;
if(isempty(vLabFontSize))
    vLabFontSize = fontSize;
end
timeTickStyle = p.Results.timeTickStyle;
timeTickInterval = p.Results.timeTickInterval;
yAxesDiff = p.Results.yAxesDiff;
plotTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
ptmpLevels = p.Results.ptmpLevels;
ptmpColor = p.Results.ptmpColor;
ptmpLineWidth = p.Results.ptmpLineWidth;
ptmpLineStyle = p.Results.ptmpLineStyle;
vLines = p.Results.vLines;
vLineColor = p.Results.vLineColor;
vLineWidth = p.Results.vLineWidth;
vLineStyle = p.Results.vLineStyle;
%
[nY,nX] = size(vAtPtmp);
if( length(xMtime)~=nX || length(yPtmp)~=nY )
    error('The size of input matrix and two vectors is incorrect')
end
if(~isempty(ptmpLim) && ~ismember(length(ptmpLim),[2,3]))
    error('The length of ptmpLim must be 2 or 3')
end
if(isempty(ptmpLim))
    ptmpLim = [min(yPtmp) max(yPtmp)];
end
if( isempty(vColLim) )
    indxY = yPtmp>=min(ptmpLim) & yPtmp<=max(ptmpLim);
    if( length(vLines)==1 )
        vAbsMax = max(max(abs(vAtPtmp(indxY,:))));
        vColLim = [-vAbsMax vAbsMax];
    else
        vColLim = [min(min(vAtPtmp(indxY,:))) max(max(vAtPtmp(indxY,:)))];
    end
end
%
if( length(vLines)==1 )
    vContP = vLines:vLines:vColLim(2);
    vContN = -vLines:-vLines:vColLim(1);
    vAnomMax = max(abs([vContP,vContN]));
    if( length(vContP) == 1 )
        vContP = repmat(vContP,1,2);
    end
    if( length(vContN) == 1 )
        vContN = repmat(vContN,1,2);
    end
end
if(isempty(colorMap))
    if(length(vLines)==1)
        colorMap = makeColormap4anom(64);
    else
        colorMap = 'jet';
    end
end
%
if( length(ptmpLim)==2) 
    % One subplot
    if( isempty(ax) )
        ax = subplot('Position',[0.1 0.1 0.78 0.8]);
    else
        axes(ax)
        cla(ax)
    end
%     cla reset
%     ax = subplot('Position',[0.1 0.1 0.78 0.8]);
    surf( xMtime, yPtmp, zeros(size(vAtPtmp))-1, vAtPtmp, 'EdgeColor','none' ); view(2)
    if(~isempty(vLines))
        addContours(vLines,xMtime,yPtmp,vAtPtmp,vContP,vContN,vLineColor,vLineWidth,vLineStyle)
    end
    ylim(ptmpLim)
    colormap(colorMap)
    caxis( vColLim );
    xlim(xLim)
    grid off
    set(gca,'FontSize',fontSize)
    ylabel(yLab,'FontSize',yLabFontSize)
    switch timeTickStyle
        case 'auto'
            xTicks = get(gca,'XTick' );
            dateFmt = 'ddmmmyy';
        case 'days'
            xTicks = calcTimeTicks( xMtime, timeTickStyle, timeTickInterval );
            dateFmt = 'ddmmmyy';
        case {'months','years'}
            xTicks = calcTimeTicks( xMtime, timeTickStyle, timeTickInterval );
            dateFmt = 'mmmyy';
    end
    set( gca, 'XTick',xTicks, 'XTickLabel',datestr(xTicks,dateFmt) )
    set( gca,'TickLength',[0.005, 0.005], 'TickDir','out', 'LineWidth',1.5)
    box( gca, 'on' )
    title( plotTitle, 'FontSize',titleFontSize )
    set(gca,'Layer','top')
    if( ~isempty(ptmpLevels) )
        hold on
        for kTheta = 1:length(ptmpLevels)
            plot( xLim,[ ptmpLevels(kTheta) ptmpLevels(kTheta) ],...
                'color',ptmpColor, 'lineWidth',ptmpLineWidth, ...
                'lineStyle',ptmpLineStyle )
        end
        hold off
    end
    
else % Two subplots
    ax = gobjects(2,1);
    y1 = ptmpYfactor * 0.8 * (ptmpLim(2)-ptmpLim(1)) /...
        ( ptmpLim(3) + ptmpLim(2)*(ptmpYfactor-1) - ptmpYfactor*ptmpLim(1) );
    ax(1) = subplot('Position',[0.1 0.1 0.78 y1]);
    cla reset
    surf( xMtime, yPtmp, zeros(size(vAtPtmp))-1, vAtPtmp, 'EdgeColor','none' ); view(2)
    if(~isempty(vLines))
        addContours(vLines,xMtime,yPtmp,vAtPtmp,vContP,vContN,vLineColor,vLineWidth,vLineStyle)
    end
    xlim( [ min(xMtime) max(xMtime) ] )
    ylim( ptmpLim([1,2]) )
    colormap(colorMap)
    caxis( vColLim );
    xlim(xLim)
    grid off
    set(gca,'FontSize',fontSize)
    hyl = ylabel(yLab);
    set( hyl, 'Units','normalized', 'FontSize',yLabFontSize );
    hyl.Position(2) = 1;
    switch timeTickStyle
        case 'auto'
            xTicks = get(gca,'XTick' );
            dateFmt = 'ddmmmyy';
        case 'days'
            xTicks = calcTimeTicks( xMtime, timeTickStyle, timeTickInterval );
            dateFmt = 'ddmmmyy';
        case {'months','years'}
            xTicks = calcTimeTicks( xMtime, timeTickStyle, timeTickInterval );
            dateFmt = 'mmmyy';
    end
    set( gca, 'XTick',xTicks, 'XTickLabel',datestr(xTicks,dateFmt) )
    set( gca,'TickLength',[0.005, 0.005], 'TickDir','out', 'LineWidth',1.5)
    box( gca, 'on' )
    set(gca,'Layer','top')
    if( ~isempty(ptmpLevels) )
        hold on
        for kTheta = 1:length(ptmpLevels)
            plot( xLim,[ ptmpLevels(kTheta) ptmpLevels(kTheta) ],...
                'color',ptmpColor, 'lineWidth',ptmpLineWidth, ...
                'lineStyle',ptmpLineStyle )
        end
        hold off
    end
    %
    ax(2) = subplot('Position',[0.1 0.1+y1+yAxesDiff 0.78 0.8-y1]);
    cla reset
    surf( xMtime, yPtmp, zeros(size(vAtPtmp))-1, vAtPtmp, 'EdgeColor','none' ); view(2)
    if(~isempty(vLines))
        addContours(vLines,xMtime,yPtmp,vAtPtmp,vContP,vContN,vLineColor,vLineWidth,vLineStyle)
    end
    xlim( [ min(xMtime) max(xMtime) ] )
    ylim( ptmpLim([2,3]) )
    colormap(colorMap)
    caxis( vColLim );
    xlim(xLim)
    grid off
    set(gca,'FontSize',fontSize)
    set( gca, 'XTick',[], 'TickLength',[0.005, 0.005], 'TickDir','out', 'LineWidth',1.5)
    y_ticks = get( gca, 'YTick' );
    set( gca, 'YTick',y_ticks(2:end) )
    box( gca, 'on' )
    title( plotTitle, 'FontSize',titleFontSize )
    set(gca,'Layer','top')
    if( ~isempty(ptmpLevels) )
        hold on
        for kTheta = 1:length(ptmpLevels)
            plot( xLim,[ ptmpLevels(kTheta) ptmpLevels(kTheta) ],...
                'color',ptmpColor, 'lineWidth',ptmpLineWidth, ...
                'lineStyle',ptmpLineStyle )
        end
        hold off
    end

end
if( length(ptmpLim)==3) 
    hc = colorbar('Position',[0.9 0.1 0.02 0.8]);
else
    hc = colorbar;
end
ylabel( hc, vLab, 'FontSize',vLabFontSize );
if( length(vLines)==1 )
    vColBticks = -vAnomMax : vLines : vAnomMax;
    set( hc, 'YTick', vColBticks )
end
end

function addContours(vLines,xMtime,yPtmp,vAtPtmp,vContP,vContN,vLineColor,vLineWidth,vLineStyle)
%
if(~isempty(vLines))
    hold on
    if(length(vLines)==1)
        contour( xMtime, yPtmp, vAtPtmp, [0 0], 'Color',[0.5 0.5 0.5],...
                'LineWidth',vLineWidth, 'LineStyle',vLineStyle )
        contour( xMtime, yPtmp, vAtPtmp, vContP, 'Color','r',...
                'LineWidth',vLineWidth, 'LineStyle',vLineStyle )
        contour( xMtime, yPtmp, vAtPtmp, vContN, 'Color','b',...
                'LineWidth',vLineWidth, 'LineStyle',vLineStyle )
    else
        contour( xMtime, yPtmp, vAtPtmp, vLines, 'Color',vLineColor,...
                'LineWidth',vLineWidth, 'LineStyle',vLineStyle )
    end
    hold off
end
%
end
