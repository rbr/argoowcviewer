function ax = plotTimeSeriesPres( Vq, Xq1, Yq1, varargin )
%
%   Plot the 2D image of the time series with Pressure as Y axis
%
%   Syntax: plotTimeSeriesPres( Vq, Xq1, Yq1,... )
%
%   Input [Required]:
%       Vq - matrix (double, size [nYq*nXq])
%       Xq1 - X-axis (datenum, size nXq), time
%       Yq1 - Y-axis (double, size nYq), pressure
%   Input [Optional]:
%       xLim - X-axis limits (datenum, default min/max)
%       yLim - PRES limits (numeric 2x1)
%       yLab - Y label (char) (default 'Pressure (dbar)')
%       vLab - V (colorbar) label (char) (default '')
%       yDir - Y-axis direction (char) (default 'rev')
%       fontSize - font size of the axes (numeric) (default 12)
%       title - figure title (char)
%       titleFontSize - title font size (numeric, default 20)
%       colormap - predefined colormap: 'jet' (default),'parula',etc.)
%       vColLim - the color range (for caxis) (numeric 2x1) (default auto)
%       vColTicks - the ticks at colorbar (numeric vector) (default auto)
%       plotAxes - Axes object. Created by default
%       timeTickStyle - the style of X-axis (mtime) ticks:
%           {'auto' (default), 'years','months','days'}
%       timeTickInterval - the interval between the time ticks 
%           (numeric, defailt 1)
%
%   Output:     
%       ax - Axes object. Can be used to make future modifications to the legend.
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-21
%
colormapList = {'parula','jet','hsv','hot','cool','spring','summer',...
    'autumn','winter','gray','bone','copper','pink'};
iscolormap = @(x) (all(isnumeric(x)) && (size(x,2)==3)) ||...
    (ischar(x) && (ismember(x,colormapList)) );
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
p = inputParser;
addRequired(p,'Vq',@ismatrix);
addRequired(p,'Xq1',@isvector); 
addRequired(p,'Yq1',@isvector);
addParameter(p,'xLim',[],is2nums);
addParameter(p,'yLim',[],is2nums);
addParameter(p,'yLab','Pressure (dbar)',@ischar);
addParameter(p,'vLab','',@ischar);
addParameter(p,'yDir','rev',@ischar);
addParameter(p,'fontSize',12,@isnumeric);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
addParameter(p,'colormap','jet',iscolormap);
addParameter(p,'vColLim',[],is2nums);
addParameter(p,'vColTicks',[],@isvector);
istimetick = @(x) (ischar(x) && (ismember(x,{'auto','years','months','days'})));
addParameter(p,'timeTickStyle','auto',istimetick);
addParameter(p,'timeTickInterval',1,@isnumeric);
addParameter(p,'plotAxes',[],isaxes);
parse(p, Vq, Xq1, Yq1, varargin{:} )
Vq = p.Results.Vq;
Xq1 = p.Results.Xq1;
Yq1 = p.Results.Yq1;
xLim = p.Results.xLim;
if( isempty(xLim) )
    xLim = [min(Xq1) max(Xq1)];
end
yLim = p.Results.yLim;
if( isempty(yLim) )
    yLim = [min(Yq1) max(Yq1)];
end
yLab = p.Results.yLab;
vLab = p.Results.vLab;
yDir = p.Results.yDir;
fontSize = p.Results.fontSize;
plotTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
colorMap = p.Results.colormap;
vColLim = p.Results.vColLim;
vColTicks = p.Results.vColTicks;
timeTickStyle = p.Results.timeTickStyle;
timeTickInterval = p.Results.timeTickInterval;
ax = p.Results.plotAxes;
%
[nYq,nXq] = size(Vq);
if(length(Xq1)~=nXq)
    error('The size of X-axis is not equal to the matrix size')
end
if(length(Yq1)~=nYq)
    error('The size of Y-axis is not equal to the matrix size')
end
if( isempty(ax) )
    ax = axes;
else
    cla(ax)
end
% 
if( isempty(vColLim) )
    indxPres = (Yq1>=yLim(1)) & (Yq1<=yLim(2));
    vColLim = [min(min(Vq(indxPres,:))) max(max(Vq(indxPres,:)))];
end
colormap(ax,colorMap)
surf( ax, Xq1, Yq1, zeros(size(Vq)), Vq, 'EdgeColor','none' ); view(ax,2)
xlim(ax,xLim)
ylim(ax,yLim )
%
hc = colorbar('peer',ax);
caxis( vColLim );
ylabel( hc, vLab );
if( ~isempty( vColTicks ) )
    set( hc, 'YTick', vColTicks )
end
grid(ax,'off')
set( ax, 'FontSize',fontSize, 'YDir',yDir )
ylabel(ax,yLab )
%
switch timeTickStyle
    case 'auto'
        xTicks = get( ax, 'XTick' );
        dateFmt = 'ddmmmyy';
    case 'days'
        xTicks = calcTimeTicks( Xq1, timeTickStyle, timeTickInterval );
        dateFmt = 'ddmmmyy';
    case {'months','years'}
        xTicks = calcTimeTicks( Xq1, timeTickStyle, timeTickInterval );
        dateFmt = 'mmmyy';
end
set( ax, 'XTick',xTicks, 'XTickLabel',datestr(xTicks,dateFmt) )
set( ax,'TickLength',[0.01, 0.01], 'TickDir','out','LineWidth',2)
box( ax, 'on' )
title( ax, plotTitle, 'FontSize', titleFontSize )
set(ax,'Layer','top')
%
end

