function ax = plotOWCsOffsetsClim( owcStr, PRES, CLMdata, varargin )
%
% % Combine the plot Plot the salinity offsets calculated by OWC method 
%       with the differences between reference salinity and climatology
%
%   Stntax: plotOWCsOffsetsClim( owcStr, PRES, CLMdata, ... )
%
%   Input [Required]:
%       owcStr - the structure with OWC output. Includes the fields:
%           owcTbl - includes the trajTbl columns and the OWC results characterizing profiles:
%                   (avg_Soffset,avg_Soffset_err,avg_Staoffset,avg_Staoffset_err)
%                   and the column profFitCoef ( avg_Staoffset-avg_Soffset)
%           la_mapped_sal - OI mapped salinity (matrix [nLayers,nProf])         
%           la_ptmp - OI mapped salinity (matrix [nLayers,nProf]) 
%       PRES - pressure, matrix [nLayers,nProf]
%       CLMdata - climatology data. A structure with the fields - arrays of 
%               size(nLayers,nProf):
%                   TEMP - temperature
%                   SAL - salinity
%                   PRES - pressure
%                   PTMP - potential temperature
%   Input [Optional]:
%           Upper plot:
%       xLim - X-axis limits (datenum, default min/max)
%       yLim - Y (deltaS) limits (numeric 2x1) (default auto)
%       timeTickStyle - the style of X-axis (mtime) ticks:
%           {'auto' (default), 'years','months','days'}
%       timeTickInterval - the interval between the time ticks 
%           (numeric, defailt 1)
%       fontSize - font size of the axes (numeric) (default 12)
%       yLab - Y label (default '\Delta S')
%       yLabFontSize - font size of the Y label
%       title - figure title (char)
%       titleFontSize - title font size (numeric, default 20)
%       sOffColor - the color of S offset (default 'b')
%       sOffLineWidth - line width of S offset (default 1)
%       sOffMarker - marker of S offset (default 'o')
%       sOffFaceColor - face color of the S offset symbols (default [0 0.5 1])
%       sOffMarkerSize - marker size of the S offset symbols (default 6)
%       profFitColor - the color of profile fit coefficients (default 'r')
%       profFitLineWidth - the line width of profile fit coefficients (default 2)
%       profFitErrEdgeColor - the edge color of profile fit coefficient errors 
%                               (default 'k')
%       profFitErrEdgeWidth - the edge width of profile fit coefficient
%                               errors (default 1)
%       profFitErrFaceColor - the face color of profile fit coefficient errors
%                               (default [1 0.8 0.8])
%       zeroColor - the color of zero (default 'k')
%       zeroLineWidth - the width of zero line (default 1)
%
%           Lower plot:
%       yPtmp - the array of potential temperatures for interpolation,
%           default linspace(min(PTMP(:)),max(PTMP(:)),100)
%       colormap - predefined colormap: 'jet' (default),'parula',etc.)
%           When length(vLineInt)==1, default colormap is 'anomalies' (b-w-r)
%       vColLim - the color range (for caxis) (numeric 2x1) (default auto)
%       yLab2 - Y label (char) (default '\theta (^oC)')
%       yLab2FontSize - font size of Y label (default = fontSize)
%       vLab - V (colorbar) label (char) (default '\Delta S')
%       vLabFontSize - font size of the colorbar label (default = fontSize)

%       ptmpLevels - the potential temeprature levels to be shown (default [])
%       ptmpColor - the potential temeprature levels color (default 'k')
%       ptmpLineWidth - the potential temeprature levels line width
%           (default 1)
%       ptmpLineStyle - the potential temeprature levels line style
%           (default '-')
%       vLineInt - the levels of contour lines. Default [] - no lines. 
%           When length(vLineInt)==1, it is the interval beween lines,
%               both directions from 0, red (positive) and blue (negative).
%           When length(vLineInt)>1, vLineInt mean absolume values of contours
%
%   Output:     
%       ax - Two axes objects. Can be used to make future modifications to the legend.
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-04
%
colormapList = {'parula','jet','hsv','hot','cool','spring','summer',...
    'autumn','winter','gray','bone','copper','pink'};
iscolormap = @(x) (all(isnumeric(x)) && (size(x,2)==3)) ||...
    (ischar(x) && (ismember(x,colormapList)) );
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'owcStr',@isstruct);
addRequired(p,'PRES',@ismatrix);
addRequired(p,'CLMdata',@isstruct);
addParameter(p,'plotAxes',[],isaxes);
addParameter(p,'xLim',[],is2nums);
istimetick = @(x) (ischar(x) && (ismember(x,{'auto','years','months','days'})));
addParameter(p,'timeTickStyle','auto',istimetick);
addParameter(p,'timeTickInterval',1,@isnumeric);
addParameter(p,'fontSize',12,@isnumeric);
addParameter(p,'yLim',[],is2nums);
addParameter(p,'yLab','\Delta S',@ischar);
addParameter(p,'yLabFontSize',16,@isnumeric);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
addParameter(p,'sOffColor','b',iscolor);
addParameter(p,'sOffLineWidth',1,@isnumeric);
addParameter(p,'sOffMarker','o',@ischar);
addParameter(p,'sOffFaceColor',[0 0.5 1],iscolor);
addParameter(p,'sOffMarkerSize',6,@isnumeric);
addParameter(p,'profFitColor','r',iscolor);
addParameter(p,'profFitLineWidth',2,@isnumeric);
addParameter(p,'profFitErrEdgeColor','k',iscolor);
addParameter(p,'profFitErrEdgeWidth',1,@isnumeric);
addParameter(p,'profFitErrFaceColor',[1 0.8 0.8],iscolor);
addParameter(p,'zeroColor','k',iscolor);
addParameter(p,'zeroLineWidth',1,@isnumeric);
addParameter(p,'yPtmp',[],@isnumeric);
addParameter(p,'colormap','',iscolormap);
addParameter(p,'vColLim',[],is2nums);
addParameter(p,'yLab2','\theta (^oC)',@ischar);
addParameter(p,'yLab2FontSize',[],@isnumeric);
addParameter(p,'vLab','\Delta S',@ischar);
addParameter(p,'vLabFontSize',[],@isnumeric);
addParameter(p,'ptmpLevels',[],@isnumeric);
addParameter(p,'ptmpColor','k',iscolor);
addParameter(p,'ptmpLineWidth',1,@isnumeric);
addParameter(p,'ptmpLineStyle','-',@ischar);
addParameter(p,'vLineInt',[],@isnumeric);
parse(p, owcStr, PRES, CLMdata, varargin{:} )
owcStr = p.Results.owcStr;
PRES = p.Results.PRES;
CLMdata = p.Results.CLMdata;
xLim = p.Results.xLim;
timeTickStyle = p.Results.timeTickStyle;
timeTickInterval = p.Results.timeTickInterval;
fontSize = p.Results.fontSize;
yLim = p.Results.yLim;
yLab = p.Results.yLab;
yLabFontSize = p.Results.yLabFontSize;
plotTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
sOffColor = p.Results.sOffColor;
sOffLineWidth = p.Results.sOffLineWidth;
sOffMarker = p.Results.sOffMarker;
sOffFaceColor = p.Results.sOffFaceColor;
sOffMarkerSize = p.Results.sOffMarkerSize;
profFitColor = p.Results.profFitColor;
profFitLineWidth = p.Results.profFitLineWidth;
profFitErrEdgeColor = p.Results.profFitErrEdgeColor;
profFitErrEdgeWidth = p.Results.profFitErrEdgeWidth;
profFitErrFaceColor = p.Results.profFitErrFaceColor; 
zeroColor = p.Results.zeroColor;
zeroLineWidth= p.Results.zeroLineWidth;
yPtmpIn = p.Results.yPtmp;
colorMap = p.Results.colormap;
vColLim = p.Results.vColLim;
yLab2 = p.Results.yLab2;
yLab2FontSize = p.Results.yLab2FontSize;
vLab = p.Results.vLab;
vLabFontSize = p.Results.vLabFontSize;
ptmpLevels = p.Results.ptmpLevels;
ptmpColor = p.Results.ptmpColor;
ptmpLineWidth = p.Results.ptmpLineWidth;
ptmpLineStyle = p.Results.ptmpLineStyle;
vLineInt = p.Results.vLineInt;
%
% Check the field names in owcStr
owcStnNames = {'owcTbl','la_mapped_sal','la_ptmp'};
iFields = ismember(owcStnNames,fieldnames(owcStr));
if(~all(iFields))
    error(['No fields in owcStr: ',strjoin(owcStnNames(~iFields),',')])
end
% Check the column names in owcTbl
owcColNames = {'mtime','avg_Soffset','avg_Soffset_err','avg_Staoffset','avg_Staoffset_err'};
iCol = ismember(owcColNames,owcStr.owcTbl.Properties.VariableNames);
if(~all(iCol))
    error(['No columns in owcTbl: ',strjoin(owcColNames(~iCol),',')])
end
[nLayers,nProf] = size(owcStr.la_mapped_sal);
if(any([nLayers,nProf]~=size(owcStr.la_ptmp)))
    error('The size of la_mapped_sal ~= la_ptmp')
end
if(nProf~=height(owcStr.owcTbl))
    error('The size of la_mapped_sal ~= height(owcTbl)')
end
if( isempty(xLim) )
    xLim = [min(owcStr.owcTbl.mtime) max(owcStr.owcTbl.mtime)];
end
%
if(isempty(yPtmpIn))
    indx_fin_sal = logical( sum(isfinite(owcStr.la_mapped_sal),2) > 0 );
    la_ptmp1 = owcStr.la_ptmp(indx_fin_sal,:);
    nPtmp = 100;
    yPtmpIn = linspace(min(la_ptmp1(:)),max(la_ptmp1(:)),nPtmp);
end
% Check the fields and size of the CLMdata elements
CLMdataNames = {'PRES','SAL','TEMP','PTMP'};
iFields = ismember(CLMdataNames,fieldnames(CLMdata));
if(~all(iFields))
    error(['No fields in CLMdata: ',strjoin(CLMdataNames(~iFields),',')])
end
[nLayers,nProf] = size(CLMdata.PRES);
if( (size(CLMdata.SAL)~=[nLayers,nProf]) | (size(CLMdata.TEMP)~=[nLayers,nProf]) |...
        (size(CLMdata.PTMP)~=[nLayers,nProf]) )
    error('The matrices in the CLMdata are not equal in size')
end
%
ax = gobjects(2,1);
ax(1) = subplot('Position',[0.1 0.6 0.735 0.35]);
plotOWCsOffsets( owcStr.owcTbl, 'plotAxes',ax(1), 'xLim',xLim, 'yLim',yLim,...
    'timeTickStyle',timeTickStyle, 'timeTickInterval',timeTickInterval,...
    'fontSize',fontSize, 'yLim',yLim, 'yLab',yLab, 'yLabFontSize',yLabFontSize,...
    'title',plotTitle, 'titleFontSize', titleFontSize,...
    'sOffColor',sOffColor, 'sOffLineWidth',sOffLineWidth,...
    'sOffMarker',sOffMarker, 'sOffFaceColor',sOffFaceColor,'sOffMarkerSize',sOffMarkerSize,...
    'profFitColor',profFitColor, 'profFitLineWidth',profFitLineWidth,...
    'profFitErrEdgeColor',profFitErrEdgeColor, 'profFitErrEdgeWidth',profFitErrEdgeWidth,...
    'profFitErrFaceColor',profFitErrFaceColor, 'zeroColor',zeroColor,...
    'zeroLineWidth',zeroLineWidth );
%
ax(2) = subplot('Position',[0.1 0.10 0.8 0.45]);
%
[ SalAtPtmpInt, xMtime, yPtmp ] = calcSalAtPtmpInt( owcStr.la_mapped_sal, owcStr.la_ptmp, PRES,...
    owcStr.owcTbl.mtime, 'yPtmp',yPtmpIn );
SalAtPtmpIntClim = calcSalAtPtmpInt( CLMdata.SAL, CLMdata.PTMP, CLMdata.PRES,...
    owcStr.owcTbl.mtime, 'yPtmp',yPtmp );
aSalAtPtmpInt = SalAtPtmpInt - SalAtPtmpIntClim;
if(isempty(vColLim))
    aSmax = max(abs(aSalAtPtmpInt(:)));
    vColLim = [-aSmax aSmax];
end
if(isempty(vLineInt))
    vLineInt = 0.01;
end
if(isempty(colorMap))
    if(length(vLineInt)==1)
        colorMap = makeColormap4anom(64);
    end
end
ax(2) = plotTimeSeriesPtmp( aSalAtPtmpInt, xMtime, yPtmp, 'ptmpLim',yPtmp([1,end]),...
    'plotAxes',ax(2), 'colorMap',colorMap, 'vColLim',vColLim,...
    'yLab',yLab2, 'yLabFontSize',yLab2FontSize, 'vLab',vLab, 'vLabFontSize',vLabFontSize,...
    'timeTickStyle',timeTickStyle, 'timeTickInterval',timeTickInterval,...
    'ptmpLevels',ptmpLevels, 'ptmpColor',ptmpColor,...
    'ptmpLineWidth',ptmpLineWidth, 'ptmpLineStyle',ptmpLineStyle,...
    'vLines',vLineInt, 'fontSize',fontSize );




end


