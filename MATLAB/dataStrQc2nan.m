function dataStrOut = dataStrQc2nan( dataStrIn, qcField, QC2remove )
%
%   In the Argo data structure dataStr, fill with NaNs the data with QC = QC2remove
%
%   Syntax: dataStr = dataStrQc2nan( dataStr, QC2remove )
%
%   Input [Required]:
%       dataStrIn - the structure with Argo data (PRES, SAL, PTMP, etc.), 
%           each a double matrix of size [nLayers*nProf]
%       qcField - (char or cell) - QC structure fields (
%       QC2remove - (char or cell) - QC codes to remove
%   Output
%       dataStrOut - the structure with Argo data
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-16
%
qcField = cellstr(qcField);
QC2remove = cellstr(QC2remove);
p = inputParser;
addRequired(p,'dataStrIn',@isstruct);
addRequired(p,'qcField',@iscellstr);
addRequired(p,'QC2remove',@iscellstr);
parse(p, dataStrIn, qcField, QC2remove );
dataStrIn = p.Results.dataStrIn;
qcField = p.Results.qcField;
QC2remove = p.Results.QC2remove;
%
dataStrOut = dataStrIn;
%
nFields = length(qcField);
nQC = length(QC2remove);
indx_rem = false( size(dataStrIn.(qcField{1}) ) );
for kField = 1:nFields
    for kQC = 1:nQC
        indx_rem = indx_rem | ( dataStrIn.(qcField{kField}) == QC2remove{kQC} );
    end
end
%
strFieldNames = fieldnames( dataStrOut );
nFields = length(strFieldNames);
for kField = 1:nFields
    if( isnumeric(dataStrOut.(strFieldNames{kField})) )
        dataStrOut.(strFieldNames{kField})(indx_rem) = NaN;
    else
        dataStrOut.(strFieldNames{kField})(indx_rem) = ' ';
    end
end

end

