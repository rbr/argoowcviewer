function QCtbl = calcQcTbl( QC )
%
%   Calculate the table of the number of values with different Argo QC 
%
%   Syntax: QCtbl = calcQcTbl( QC );
%
%   Input [Required]:
%       QC - the char matrix of size [nLayers*nProf] with Argo Quality
%           Control codes
%   Output
%       QCtbl - the table with the numbers of QC values ('0'-'9') in QC
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-16
%
p = inputParser;
addRequired(p,'QC',@ischar);
parse(p, QC );
QC = p.Results.QC;
%
QCtbl = table( num2str((0:9)'), {'No QC','Good','Probably good','Potentially correctable',...
    'Bad','Value changed','Not used','Not used','Estimated value','Missing value'}',...
    NaN(10,1), true(10,1), 'VariableNames',{'code','meaning','n','sel'} );
for kQCtbl = 1:height(QCtbl)
    QCtbl.n(kQCtbl) = sum(sum( QC(:)==QCtbl.code(kQCtbl) ) );
    QCtbl.sel(kQCtbl) = (QCtbl.n(kQCtbl)>0);
end

end
