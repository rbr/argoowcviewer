function ax = plotOWCsOffsetsBBmap( owcTbl, varargin )
%
%   Plot the salinity offsets calculated by OWC method as 'bubbles' as 
%       trajectory map
%
%   Stntax: ax = plotOWCsOffsetsBBmap( owcStr, ... )
%
%   Input [Required]:
%           owcTbl - includes the columns latitude, longitude, mtime and the 
%               OWC results characterizing profiles:
%               (avg_Soffset,avg_Soffset_err,avg_Staoffset,avg_Staoffset_err)
%   Input [Optional]:
%       plotAxes - Axes object. Created by default
%       latLim, lonLim - the map limits. If [] - detected autimatically. 
%       projection - Lambert (default), Mercator, etc. - see m_proj('set')
%       box - ( 'on' (default) | 'fancy' | 'off' )
%       XaxisLocation - 'bottom' (default) | 'middle' | 'top' ) 
%       YaxisLocation - 'left' (default) | 'middle' | 'right' ) 
%       xTick - ( num (number of ticks) | [value1 value2 ...] | auto (default) )
%       yTick - ( num  (number of ticks)| [value1 value2 ...] | auto (default) )
%       landcolor - color of land (e.g., [0.8 0.8 0.8](default) or 'g','k',etc.)
%       GSHHS - use GSHHS coastline of resolution 'c','l','i','h','f'
%           (use m_coast as default)
%       axesFontSize - map axis font size (default 16)
%       lineColor - color of the trajectory line (default 'k')
%       lineWidth - width of the trajectory line (default 1)
%       profFitMarkerSize - the coefficient of profile fit marker size
%           (default 10000)
%       profFitMarkerEdgeColor - profile fit marker edge color (default 'k')
%       profFitMarkerColor - colorscale (matrix N*3) (default [])
%       cLim - the limits of the profile fit coefficients color scale (numeric 2x1) (default auto)
%       title - map title (default '')
%       titleFontSize - map title font size (default 20) 
%
%   Output:     
%       ax - Axes object. Can be used to make modifications to the legend.
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-26
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
isgshhs = @(x) ismember(lower(x),{'','c','l','i','h','f'});
p = inputParser;
addRequired(p,'owcTbl',@istable);
addParameter(p,'plotAxes',[],isaxes);
addParameter(p,'latLim',[],is2nums);
addParameter(p,'lonLim',[],is2nums);
addParameter(p,'projection','Lambert',@ischar);
addParameter(p,'box','on',@ischar);
addParameter(p,'XaxisLocation','bottom',@ischar);
addParameter(p,'YaxisLocation','left',@ischar);
addParameter(p,'xTick',[],@isnumeric);
addParameter(p,'yTick',[],@isnumeric);
addParameter(p,'landcolor',[0.8 0.8 0.8],iscolor);
addParameter(p,'GSHHS',[],isgshhs);
addParameter(p,'axesFontSize',16,@isnumeric);
addParameter(p,'lineColor','k',iscolor);
addParameter(p,'lineWidth',1,@isnumeric);
addParameter(p,'profFitMarkerSize',[],@isnumeric);
addParameter(p,'profFitMarkerEdgeColor','k',iscolor);
addParameter(p,'profFitMarkerColor',[],@ismatrix);
addParameter(p,'cLim',[],is2nums);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
parse(p, owcTbl, varargin{:} )
owcTbl = p.Results.owcTbl;
ax = p.Results.plotAxes;
latLim  = p.Results.latLim;
lonLim  = p.Results.lonLim;
projection = p.Results.projection;
box = p.Results.box;
XaxisLocation = p.Results.XaxisLocation;
YaxisLocation = p.Results.YaxisLocation;
xTick = p.Results.xTick;
yTick = p.Results.yTick;
landColor = p.Results.landcolor;
GSHHS = p.Results.GSHHS;
axesFontSize = p.Results.axesFontSize;
lineColor = p.Results.lineColor;
lineWidth = p.Results.lineWidth;
profFitMarkerSize = p.Results.profFitMarkerSize;
profFitMarkerEdgeColor = p.Results.profFitMarkerEdgeColor;
profFitMarkerColor = p.Results.profFitMarkerColor;
cLim = p.Results.cLim;
mapTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
%
% Check the column names in owcTbl
owcColNames = {'mtime','latitude','longitude','avg_Soffset','avg_Soffset_err',...
    'avg_Staoffset','avg_Staoffset_err'};
iCol = ismember(owcColNames,owcTbl.Properties.VariableNames);
if(~all(iCol))
    error(['No columns in owcTbl: ',strjoin(owcColNames(~iCol),',')])
end
%
if( isempty(ax) )
    ax = axes;
else
    axes(ax)
    cla(ax)
end
%
%
if( isempty(latLim) ) 
    latLim = axis_set( owcTbl.latitude,[-90 90] );
end
if(isempty(yTick))
    yTick = tick_set( latLim, 5 );
end
if( isempty(lonLim) )
    lonLim = axis_set( owcTbl.longitude,[-180 360] );
end
if(isempty(xTick))
    xTick = tick_set( lonLim, 5 );
end
if(isempty(profFitMarkerSize))
    profFitMarkerSize = 200./max(abs(owcTbl.profFitCoef));
end
if(isempty(profFitMarkerColor))
    profFitMarkerColor = makeColormap4anom(64);
end
colormap(profFitMarkerColor)
%
m_proj( projection,'lat',latLim,'long',lonLim );
if( isempty( GSHHS ) )
    m_coast('patch',landColor);
else
    m_gshhs(GSHHS,'patch',landColor);
end
m_grid('box',box, 'linestyle',':','linewidth',2,'tickdir','out','fontsize',axesFontSize,...
    'XaxisLocation',XaxisLocation, 'YaxisLocation',YaxisLocation,...
    'yTick',yTick, 'xTick',xTick);
m_line( owcTbl.longitude, owcTbl.latitude, 'Color',lineColor, 'lineWidth',lineWidth );
hold on
m_scatter( owcTbl.longitude, owcTbl.latitude, abs(owcTbl.profFitCoef).*profFitMarkerSize,...
    owcTbl.profFitCoef, 'filled', 'MarkerEdgeColor',profFitMarkerEdgeColor )
if(isempty(cLim))
    maxFC = max(abs(owcTbl.profFitCoef));
    cLim = [-maxFC maxFC];
end
caxis(cLim)
title( mapTitle, 'FontSize',titleFontSize )

end

function [ x_lim, n_ticks ] = axis_set( x, xLim )
%
x_min = min( x ); x_max = max( x );
d_x = x_max - x_min;
if( d_x > 180 )
    indx_w = ( x < 0 );
    x(indx_w) = x(indx_w) + 360;
    x_min = min( x ); x_max = max( x );
    d_x = x_max - x_min;
end
n_ticks = tick_number( d_x );
x_lim = [ max(x_min-d_x/2,xLim(1)) min(x_max+d_x/2,xLim(2)) ];

end

function n_ticks = tick_number( d_xy )
%
mult_f = [2,2.5,2];
n_ticks = 1;
k_mult = 0;
while( d_xy/n_ticks > 5 )
    k_mult = k_mult + 1;
    if( k_mult>3 )  
        k_mult = k_mult-3;
    end
    n_ticks = n_ticks .* mult_f(k_mult);
end
%
k_mult = 0;
while( d_xy/n_ticks < 2 )
    k_mult = k_mult + 1;
    if( k_mult>3 )  
        k_mult = k_mult-3;
    end
    n_ticks = n_ticks ./ mult_f(k_mult);
end
%
end
%
function x_ticks = tick_set( x_lim, n_tick_max )
    x_int = [0.5,1,2,5,10,20];
    for k = 1:length(x_int)
        x_ticks = -180:x_int(k):360;
        indx_in = (x_ticks>=x_lim(1)) & (x_ticks<=x_lim(2));
        if(sum(indx_in)<n_tick_max)
            break
        end
    end
end


