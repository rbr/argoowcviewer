function [ SalAtPtmp, yPtmpOut ] = calcSalAtPtmp( SAL, PTMP, PRES, varargin )
%
%   Calculates salinity at potential temperature levels
%       Temperature inversions are removed
%
%   Syntax: [ SalAtPtmp, yPtmpOut ] = calcSalAtPtmp( SAL, PTMP, PRES,... )
%
%   Input [Required]:
%       SAL - salinity, numeric matrix of size nLayers*nProf
%       PTMP - potential temperature, numeric matrix of size nLayers*nProf
%       PRES - pressure, numeric matrix of size nLayers*nProf
%   Input [Optional]:
%       yPtmp - the array of potential temperatures for interpolation,
%           default linspace(min(PTMP(:)),max(PTMP(:)),100)
%
%   Output:  
%       SalAtPtmp - salinities at potential temperature levels, size (length(yPtmpIn),nProf)
%       yPtmpOut - the array of potential temperatures for interpolation
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-22
%
p = inputParser;
addRequired(p,'SAL',@ismatrix);
addRequired(p,'PTMP',@ismatrix);
addRequired(p,'PRES',@ismatrix);
addParameter(p,'yPtmp',[],@isnumeric);
parse(p, SAL, PTMP, PRES, varargin{:} )
SAL = p.Results.SAL;
PTMP = p.Results.PTMP;
PRES = p.Results.PRES;
yPtmpIn = p.Results.yPtmp;
%
[nLayers,nProf] = size(SAL);
if( size(PTMP,1)~=nLayers || size(PRES,1)~=nLayers || size(PTMP,2)~=nProf ||...
        size(PRES,2)~=nProf )
    error('The three input matrices must be of equal size')
end
if(isempty(yPtmpIn))
    nPtmp = 100;
    yPtmpOut = linspace(min(PTMP(:)),max(PTMP(:)),nPtmp);
else
    yPtmpOut = yPtmpIn;
    nPtmp = length(yPtmpOut);
end
% 
SalAtPtmp = NaN(nPtmp,nProf);
% h_wait = waitbar( 0, 'Calculating SAL at PTMP...' );
for kProf = 1:nProf
%     waitbar( kProf / nProf, h_wait )
    PR_PT_SAL = [ PRES(:,kProf), PTMP(:,kProf), SAL(:,kProf) ];
    PR_PT_SAL = sortrows( PR_PT_SAL, 1 );
    iPTinv = [ ( diff(PR_PT_SAL(:,2)) >= 0 ); true ];
    PR_PT_SAL = PR_PT_SAL( ~iPTinv, : );
    igood = ~isnan(PR_PT_SAL(:,2)) & ~isnan(PR_PT_SAL(:,3));
    if( sum( igood ) > 2 )
        SalAtPtmp(:,kProf) = interp1(PR_PT_SAL(igood,2),PR_PT_SAL(igood,3),yPtmpOut(:));
    end
end
% close( h_wait )

end


