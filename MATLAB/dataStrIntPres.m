function dataStrOut = dataStrIntPres( dataStrIn, pres2Int )
%
% In the Argo data structure, interpolate profiles to fixed pressure levels
%
%   Syntax: dataStrOut = dataStrIntPres( dataStrIn, pres2Int )
%
%   Input [Required]:
%       dataStrIn - the structure with Argo data (PRES, PRES_QC, TEMP, etc), 
%           each a double matrix of size [nLayers*nProf]
%       pres2Int - pressure levels for interpolation (numeric vector) 
%
%   Output:     
%       dataStrOut - the structure with Argo data (PRES, TEMP, etc), 
%           each a double matrix of size [nLayers*nProfNew]
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-20
%
p = inputParser;
addRequired(p,'dataStrIn',@isstruct);
addRequired(p,'pres2Int',@isnumeric);
parse(p, dataStrIn, pres2Int )
dataStrIn = p.Results.dataStrIn;
pres2Int = p.Results.pres2Int;
%
strFieldNames = fieldnames( dataStrIn );
kPres = find(strcmp(strFieldNames,'PRES'), 1);
if( isempty(kPres) )
    error('The data structure has no field PRES')
end
kTSPT = find(ismember( strFieldNames, {'TEMP','SAL','PTMP'} ));
%
dataStrOut = struct( 'PRES',repmat( reshape(pres2Int,[],1), 1, size(dataStrIn.PRES,2) ) );
for kField = 1:length(kTSPT)
    nProf = size(dataStrIn.(strFieldNames{kTSPT(kField)}),2);
    VVout = NaN(length(pres2Int),nProf);
    for kProf = 1:nProf
        indxFin = isfinite(dataStrIn.PRES(:,kProf));
        PrVV = [ dataStrIn.PRES(indxFin,kProf),...
            dataStrIn.(strFieldNames{kTSPT(kField)})(indxFin,kProf)];
        [~,indxSort] = unique(PrVV(:,1));
        PrVV = PrVV(indxSort,:);
        VVout(:,kProf) = interp1( PrVV(:,1), PrVV(:,2), pres2Int );
    end
    dataStrOut.(strFieldNames{kTSPT(kField)}) = VVout;
end

end



