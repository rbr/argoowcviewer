% codeExampleArgoFromGDAC.m
%   Get Argo files from GDAC
clear variables
timeLim = [ datenum(2020,1,1), datenum(2020,4,1) ]; % Time limits
latLim = [ 30 33 ];   % Latitude limits
lonLim = [ -172 -167 ]; % Longitude limits
saveDir = '/Users/nikolaynezlin/Documents/Projects/2020/argoOWCviewer/temp/';
indxFilePath = '/Users/nikolaynezlin/Documents/Projects/2020/argoOWCviewer/temp/ar_index_global_prof.txt';
% Get the file list
floatTbl = gdacFloatsFind( latLim, lonLim, timeLim, 'saveDir',saveDir,...
    'indxFilePath',indxFilePath, 'ocean','P' );
% Download the files
disp(['Argo floats in the area: Latitude ', num2str(latLim),...
    ' Longitude ',num2str(lonLim)])
disp(floatTbl)
gdacFloatsGet( floatTbl, 'saveDir',saveDir );


