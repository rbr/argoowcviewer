function [ trajTbl, dataRawStr, dataAdjStr, metaPar ] = readArgoNc( argoNcPath, varargin )
%
%   Reads Argo NetCDF file and returns 
%       - the table with Argo float trajectory
%       - two structures with Argo data (PRES, PRES_QC, TEMP, TEMP_QC, SAL, SAL_QC, 
%           PTMP, PTMP_QC): raw and adjusted
%       - the structure with metadata (parameters)
%   For profiles with the same mtime, the second profile is shifted 1h later 
%
%   Syntax: [trajTbl,dataRaw,dataAdj,metaPar] = readArgoNc(argoNcPath);
%
%   Input [Required]:
%       argoNcPath - the path to Argo NetCDF file (GDAC format)
%           If the NetCDF file does not exist, removes empty table and 3 []s.
%   Input [Optional]:
%       ShiftDuplMtime - If several profiles have equal mtime, each next profile is
%           shifted for ShiftDuplMtime. Default 1/24 day (=1h). 
%
%   Output:
%       trajTbl - the float trajectory table (height nProf) with columns:
%           platform_number - cell of char (size nProf*1)
%           cycle_number - numeric (size nProf*1)
%           latitude - numeric (size nProf*1)
%           longitude - numeric (size nProf*1)
%           mtime - datenum format (size nProf*1) 
%           data_mode - char {'R','A','D'}  (size nProf*1)
%       dataRawStr - the structure with Argo raw data (PRES, PRES_QC, TEMP, TEMP_QC,
%           SAL, SAL_QC, PTMP, PTMP_QC), each a matrix of size [nLayers*nProf]
%       dataAdjStr - the structure with Argo adjusted data (PRES, PRES_QC, TEMP, TEMP_QC,
%           SAL, SAL_QC, PTMP, PTMP_QC), each a matrix of size [nLayers*nProf]
%       In both dataRawStr and dataAdjStr,
%           PTMP is calculated from PRES, TEMP and SAL
%           PTMP_QC = TEMP_QC;
%       metaPar - the array of [nProf] structures with metadata about Argo parameters.
%               Each structure includes the char fields: 
%               parameter (nPar = the number of parameters)
%               scientific_calib_equation (nPar)
%               scientific_calib_coefficient (nPar)
%               scientific_calib_comment (nPar)
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-10
%
p = inputParser;
addRequired(p,'argoNcPath',@ischar);
addOptional(p,'ShiftDuplMtime',1/24,@isnumeric);
parse(p, argoNcPath )
argoNcPath = p.Results.argoNcPath;
ShiftDuplMtime = p.Results.ShiftDuplMtime;
%
if( exist( argoNcPath,'file' ) == 0 )
    trajTbl = array2table(zeros(0,6));
    trajTbl.Properties.VariableNames = {'platform_number','cycle_number','latitude',...
        'longitude','mtime','data_mode'};
    dataRawStr = [];
    dataAdjStr = [];
    metaPar = [];
    return
end
%
argo = read_argo_netcdf_wnans( argoNcPath );
indx_fin = isfinite(argo.latitude) & isfinite(argo.longitude) & isfinite(argo.mtime);
if( ~all(indx_fin) )
    argo = argo_remove_nan_coords( argo, indx_fin );
end
%
trajTbl = table( cellstr(argo.platform_number'), 'VariableNames',{'platform_number'} );
nProf = height(trajTbl);
trajTbl.cycle_number = argo.cycle_number;
trajTbl.latitude = argo.latitude;
trajTbl.longitude = argo.longitude;
trajTbl.mtime = argo.mtime;
trajTbl.data_mode = argo.data_mode;
% Move back for dHour hours the stations with duplicated mtime
trajTbl.mtime = mtime_dup_1hback( trajTbl.mtime, ShiftDuplMtime );
%
nLayers = size(argo.pres,1);
dataRawStr = struct( 'PRES',argo.pres, 'PRES_QC',argo.meta.pres_qc,...
    'TEMP',argo.temp, 'TEMP_QC',argo.meta.temp_qc,...
    'SAL',argo.psal, 'SAL_QC',argo.meta.psal_qc );
LAT = repmat(trajTbl.latitude,1,nLayers)';
LONG = repmat(trajTbl.longitude,1,nLayers)';
SA = gsw_SA_from_SP( dataRawStr.SAL, dataRawStr.PRES, LONG, LAT );
dataRawStr.PTMP = gsw_pt0_from_t( SA, dataRawStr.TEMP, dataRawStr.PRES );
dataRawStr.PTMP_QC = dataRawStr.TEMP_QC;
%
dataAdjStr = struct( 'PRES',argo.pres_adjusted, 'PRES_QC',argo.meta.pres_adjusted_qc,...
    'TEMP',argo.temp_adjusted, 'TEMP_QC',argo.meta.temp_adjusted_qc,...
    'SAL',argo.psal_adjusted, 'SAL_QC',argo.meta.psal_adjusted_qc );
SA = gsw_SA_from_SP( dataAdjStr.SAL, dataAdjStr.PRES, LONG, LAT );
dataAdjStr.PTMP = gsw_pt0_from_t( SA, dataAdjStr.TEMP, dataAdjStr.PRES );
dataAdjStr.PTMP_QC = dataAdjStr.TEMP_QC;
%
metaPar = struct;
for kProf = 1:nProf
    metaPar(kProf).parameter = cellstr(argo.meta.parameter(:,:,1,kProf)');
    metaPar(kProf).scientific_calib_equation = cellstr(squeeze(argo.meta.scientific_calib_equation(:,:,1,kProf))');
    metaPar(kProf).scientific_calib_coefficient = cellstr(squeeze(argo.meta.scientific_calib_coefficient(:,:,1,kProf))');
    metaPar(kProf).scientific_calib_comment = cellstr(squeeze(argo.meta.scientific_calib_comment(:,:,1,kProf))');
end
%
end

function mtime = mtime_dup_1hback( mtime, ShiftDuplMtime )
% Move back for ShiftDuplMtime days the stations with duplicated mtime
    while( any( diff(mtime) < 0 ) )
        dt = diff(mtime);
        k_neg = find( dt < 0, 1,'first' );
        mtime(k_neg+1) = mtime(k_neg);
    end
    while( any( diff(mtime) == 0 ) )
        dt = diff(mtime);
        indx0 = ( dt == 0 );
        mtime( indx0 ) = mtime( indx0 ) - ShiftDuplMtime;
    end
end
