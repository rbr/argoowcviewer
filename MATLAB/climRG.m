function [ CLM_TEMP, CLM_SAL, CLM_PRES ] = climRG( LAT, LONG, mtime, RGpath )
% 
%   Finds the Roemmich-Gilson Argo Climatology in the path RGpath and selects 
%   the profiles of climatic temperature and salinity for the locations and months 
%   determined by LAT, LONG and mtime.    
%
%   In argoOWCviewer, clim_get_RG is called from the function climAlongTraj
%
%   Syntax:  [ CLM_TEMP, CLM_SAL, CLM_PRES ] = clim_get_RG( LAT, LONG, mtime, RGpath );
% 
%   Finds the Roemmich-Gilson Argo Climatology in the path RGpath and selects 
%   the profiles of climatic temperature and salinity for each Argo profile 
%   determined by LAT, LONG and mtime.    
%
%   Input [Required]:
%       LAT, LONG, mtime - arrays of the length equal to the number of
%           profiles (nProf)
%       RGpath - location of RG database on local computer
%           (e.g., RGpath = 'my_dir.../RG_Argo_Climatology/data/')
%
%   Output:
%       CLM_TEMP, CLM_SAL, CLM_PRES - arrays of size(nLayers,nProf). 
%           For RG climatology, nLayers=58;
%       CLM_PRES - pressure 
%       CLM_TEMP - temperature
%       CLM_SAL - salinity  
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-25
%
p = inputParser;
addRequired(p,'LAT',@isnumeric);
addRequired(p,'LONG',@isnumeric);
addRequired(p,'mtime',@isnumeric);
addRequired(p,'RGpath',@ischar);
parse(p, LAT, LONG, mtime, RGpath )
% 
LAT = p.Results.LAT;
LONG = p.Results.LONG;
mtime = p.Results.mtime;
RGpath = p.Results.RGpath;
% Check the inputs, set defaults
if( (length(LAT)~=length(LONG)) && (length(LAT)~=length(mtime)))
    error('LAT, LONG and mtime must be the same length')
end
if( ~exist(RGpath,'dir') )
    error(['No dir ',RGpath] )
end
%
RGTfn = [ RGpath, 'RG_ArgoClim_Temperature_2019.nc' ];
RGSfn = [ RGpath, 'RG_ArgoClim_Salinity_2019.nc' ];
RGlon = ncread(RGSfn,'LONGITUDE');  % 20.5 to 379.5
RGlat = ncread(RGSfn,'LATITUDE');  % -64.5000 to 79.5000
RGpres = ncread(RGSfn,'PRESSURE');  % 2.5 to 1975
%
RGmonthsR = ncread(RGSfn,'TIME');
nRGmonths = length(RGmonthsR);
mtimeStart = datenum(ncreadatt( RGSfn, 'TIME', 'time_origin' ));
yStart = year( mtimeStart );
mStart = month( mtimeStart );
RGdatetime = datenum( repmat(yStart,nRGmonths,1), double(floor(mStart+RGmonthsR)),...
    ones(nRGmonths,1) );
RGyear = year( RGdatetime );
RGmonth = month( RGdatetime );
%
nProf = length( mtime );
nLayers = length( RGpres );
CLM_PRES = NaN( nLayers, nProf );
CLM_TEMP = NaN( nLayers, nProf );
CLM_SAL = NaN( nLayers, nProf );
%
h_wait = waitbar( 0, 'Extracting data from Roemmich-Gilson Argo Climatology...' );
wax = findobj(h_wait, 'type','axes');
tax = get(wax,'title');
set(tax,'fontsize',16)
%
warning('off','all')
for kProf = 1:nProf
    waitbar( kProf / nProf, h_wait )
    CLM_PRES(:,kProf) = RGpres;
    % Latitude, longitude
    lat1 = LAT(kProf);
    lon1 = LONG(kProf);
    dLON = diff(RGlon(1:2));
    if( lon1 < min(RGlon)-dLON/2 )
        lon1 = lon1 + 360;
    elseif( lon1 > max(RGlon)+dLON/2  )
        lon1 = lon1 - 360;
    end
    mtime1 = mtime(kProf);
    [~,kLat] = min(abs(RGlat-lat1));
    [~,kLon] = min(abs(RGlon-lon1));
    Bm = squeeze(ncread(RGTfn,'BATHYMETRY_MASK',[kLon,kLat,1],[1,1,Inf]));
    Bm( isnan(Bm) ) = 0;
    Tmean1 = squeeze(ncread(RGTfn,'ARGO_TEMPERATURE_MEAN',[kLon,kLat,1],[1,1,Inf]));
    Tmean1( ~Bm ) = NaN;
    Smean1 = squeeze(ncread(RGSfn,'ARGO_SALINITY_MEAN',[kLon,kLat,1],[1,1,Inf]));
    Smean1( ~Bm ) = NaN;
    % From which file we get anomalies?
    year1 = year( mtime1 );
    month1 = month( mtime1 );
    kTime = find( ( RGyear == year1 ) & ( RGmonth == month1 ), 1 );
    if( ~isempty(kTime) )
        % Read from the large files
        RGTa_fn = RGTfn;
        RGSa_fn = RGSfn;
    else
        RGTa_fn = [ RGpath, 'RG_ArgoClim_', num2str(year1),...
                num2str(month1,'%02.0f'), '_2019.nc' ];
        if( exist( RGTa_fn,'file' ) )
            RGSa_fn = RGTa_fn;
            kTime = 1;
        else
            RGTa_fn = [];
            RGSa_fn = [];
        end
    end
    if( ~isempty( RGTa_fn ) )
        Tanom1 = squeeze(ncread(RGTa_fn,'ARGO_TEMPERATURE_ANOMALY',[kLon,kLat,1,kTime],[1,1,Inf,1]));
        Sanom1 = squeeze(ncread(RGSa_fn,'ARGO_SALINITY_ANOMALY',[kLon,kLat,1,kTime],[1,1,Inf,1]));
        CLM_TEMP(:,kProf) = Tmean1 + Tanom1;
        CLM_SAL(:,kProf) = Smean1 + Sanom1;
    end
    
end   
warning('on','all')
close( h_wait )
% 
end

