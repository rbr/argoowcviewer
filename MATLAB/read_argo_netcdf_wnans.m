function argo = read_argo_netcdf_wnans(file_name)

%read_argo_netcdf - Read an Argo NetCDF file into a Matlab
%structure replacing _FillValue with NaN. 
%
% The data read from the file is determined by querying the file for
% variables, and then using the variable names as field names in a
% structure.  It stuffs most of the variables, including the QC flags,
% into a field called .meta.  It also adds a field with the file name,
% and a field containing the profile date and time in Matlab serial
% time format.
%
%   For all 'top_level' variables, the attribute _FillValue is obtained and
%   the variable values ==  _FillValue are changed to NaN.
%
% Syntax:  [argo] = read_argo_netcdf_wnans(file_name)
%
% Inputs:
%    file_name - Character string or cell array containing the full
%    path and file name.
%
% Outputs:
%    argo - Structure containing all of the variables in the NetCDF
%    file. 
%
% Example: 
%    [argo] = read_argo_netcdf_wnans('/full/path/to/file/file_name.nc')
%
% Other m-files required: none
% Subfunctions: none
%
% See also: reshape_argo_file.m
%
% Authors: Mark Halverson; Nikolay Nezlin
% email: mark.halverson@rbr-global.com
% Website: www.rbr-global.com
% Created: 28-May-2019 
% Last revision: 1-June-2019


file_name = char(file_name);
% disp(['reading: ' file_name])



%% use ncinfo to get list of all variables
f = ncinfo(file_name);


% read variable names with ncinfo and use as structure field names
varNames = {f.Variables.Name}';

argo = struct;
for k_var = 1:length( varNames )
    var = ncread( file_name, varNames{k_var} );
    vinfo = ncinfo( file_name, varNames{k_var} );
    if( ~isempty( vinfo.Attributes ) )
        indx_fill = find( strcmp( {vinfo.Attributes.Name}, '_FillValue' ), 1 );
        if( ~isempty(indx_fill ) )
            FillValue = ncreadatt( file_name, varNames{k_var}, '_FillValue' );
        end
    else
        FillValue = NaN;
    end
    %
    if( isnumeric( var ) )
        var( var == FillValue ) = NaN;
    end
    argo.(lower(varNames{k_var})) = var;
end
varNames = lower(varNames);


% stuff most of the metadata away into a separate field
top_level = {'platform_number' 'cycle_number' 'data_mode' 'platform_type'...
             'float_serial_no' 'latitude' 'longitude' 'pres' ...
             'pres_adjusted' 'temp' 'temp_adjusted' 'psal' ...
             'psal_adjusted'};

meta_vars = setdiff(varNames,top_level);

if( all(ismember(top_level,varNames)) )

% make a temporary meta data structure
meta = rmfield(argo,top_level);

% remove metadata from data 
argo = rmfield(argo,meta_vars);

% add metadata as another field
argo.meta = meta;

% now put in a few custom things
argo.file_name = file_name;

dayref = datenum(sscanf(argo.meta.reference_date_time(1:4),'%f'),...
                 sscanf(argo.meta.reference_date_time(5:6),'%f'),...
                 sscanf(argo.meta.reference_date_time(7:8),'%f'),0,0,0);
if( isempty( dayref ) )
    dayref = datenum( 1950,1,1,0,0,0 );
end
argo.mtime = argo.meta.juld + dayref;

else
    argo = [];
end    
    





