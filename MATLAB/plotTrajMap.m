function ax = plotTrajMap( trajTbl, varargin )
%
%   Plot the map of the float trajectory
%
%   Syntax: plotTrajMap( trajTbl,... )   
%
%   Input [Required]:
%       trajTbl - table of size n_prof with columns:
%               latitude - (-90 to 90), should start with lat','Lat','LAT', etc.
%               longitude - (-180 to 360), should start with lon','Lon','LON', etc.
%               either 'mtime' in datenum format or 'datetime' in datetime
%                   format
%   Input [Optional]:
%       latLim, lonLim - the map limits. If [] - detected autimatically. 
%       projection - Lambert (default), Mercator, etc. - see m_proj('set')
%       box - ( 'on' (default) | 'fancy' | 'off' )
%       XaxisLocation - 'bottom' (default) | 'middle' | 'top' ) 
%       YaxisLocation - 'left' (default) | 'middle' | 'right' ) 
%       xTick - ( num (number of ticks) | [value1 value2 ...] | auto (default) )
%       yTick - ( num  (number of ticks)| [value1 value2 ...] | auto (default) )
%       colormap - predefined colormap: 'jet' (default)','parula',etc.)
%       landcolor - color of land (e.g., [0.8 0.8 0.8](default) or 'g','k',etc.)
%       GSHHS - use GSHHS coastline of resolution 'c','l','i','h','f'
%           (use m_coast as default)
%       axesFontSize - map axes font size (default 16)
%       lineColor - color of the trajectory line (default 'k')
%       lineWidth - width of the trajectory line (default 1)
%       markerSize - size of profile markers (default 10)
%       MarkerEdgeColor - edge color of profile markers (default [0.5 0.5 0.5])
%       nLeg - the number of profiles in legend (default 10)
%       legendFontSize - legend font size (default 12)
%       legendLocation - legend location (default 'best')
%       title - map title (default '')
%       titleFontSize - map title font size (default 20) 
%       addLine - 2-column matrix (longitude,latitude) of the additional line(s),
%           may be segmented by pairs of NaN
%       addLineColor - the color of the additional line (default 'k')
%       addLineWidth - the width of the additional line (default 2)
%       addLineStyle - the style of the additional line (default '-')
%       addMarker - the additional line marker (default 'o')
%       addMarkerSize - the additional line marker size (default 10)
%       addMarkerFaceColor - the additional line marker face color (default addLineColor)
%       addMarkerEdgeColor - the additional line marker edge color (default addLineColor)
%       plotAxes - Axes object. Created by default
%
%   Output:     
%       ax - Axes object. Can be used to make modifications to the legend.
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-12
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
isgshhs = @(x) ismember(lower(x),{'','c','l','i','h','f'});
is2colmatrix = @(x) ismatrix(x) && (size(x,2)==2);
colormapList = {'parula','jet','hsv','hot','cool','spring','summer',...
    'autumn','winter','gray','bone','copper','pink'};
iscolormap = @(x) (all(isnumeric(x)) && (size(x,2)==3)) ||...
    (ischar(x) && (ismember(x,colormapList)) );
p = inputParser;
addRequired(p,'trajTbl',@istable);
addParameter(p,'latLim',[],is2nums);
addParameter(p,'lonLim',[],is2nums);
addParameter(p,'projection','Lambert',@ischar);
addParameter(p,'box','on',@ischar);
addParameter(p,'XaxisLocation','bottom',@ischar);
addParameter(p,'YaxisLocation','left',@ischar);
addParameter(p,'xTick',[],@isnumeric);
addParameter(p,'yTick',[],@isnumeric);
addParameter(p,'colormap','jet',iscolormap);
addParameter(p,'landcolor',[0.8 0.8 0.8],iscolor);
addParameter(p,'GSHHS',[],isgshhs);
addParameter(p,'axesFontSize',16,@isnumeric);
addParameter(p,'lineColor','k',iscolor);
addParameter(p,'lineWidth',1,@isnumeric);
addParameter(p,'markerSize',10,@isnumeric);
addParameter(p,'MarkerEdgeColor',[0.5 0.5 0.5],iscolor);
addParameter(p,'nLeg',10,@isnumeric);
addParameter(p,'legendFontSize',12,@isnumeric);
addParameter(p,'legendLocation','best',@ischar);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
addParameter(p,'addLine',[],is2colmatrix);
addParameter(p,'addLineColor','k',iscolor);
addParameter(p,'addLineWidth',2,@isnumeric);
addParameter(p,'addLineStyle','-',@ischar);
addParameter(p,'addMarker','o',@ischar);
addParameter(p,'addMarkerSize',10,@isnumeric);
addParameter(p,'addMarkerFaceColor',[],iscolor);
addParameter(p,'addMarkerEdgeColor',[],iscolor);
addParameter(p,'plotAxes',[],isaxes);
parse(p, trajTbl, varargin{:} )
trajTbl = p.Results.trajTbl;
latLim  = p.Results.latLim;
lonLim  = p.Results.lonLim;
projection = p.Results.projection;
box = p.Results.box;
XaxisLocation = p.Results.XaxisLocation;
YaxisLocation = p.Results.YaxisLocation;
xTick = p.Results.xTick;
yTick = p.Results.yTick;
colorMap  = p.Results.colormap;
landColor = p.Results.landcolor;
GSHHS = p.Results.GSHHS;
axesFontSize = p.Results.axesFontSize;
lineColor = p.Results.lineColor;
lineWidth = p.Results.lineWidth;
markerSize = p.Results.markerSize;
MarkerEdgeColor = p.Results.MarkerEdgeColor;
nLeg = p.Results.nLeg;
legendFontSize = p.Results.legendFontSize;
legendLocation = p.Results.legendLocation;
mapTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
addLine = p.Results.addLine;
addLineColor = p.Results.addLineColor;
addLineWidth = p.Results.addLineWidth;
addLineStyle = p.Results.addLineStyle;
addMarker = p.Results.addMarker;
addMarkerSize = p.Results.addMarkerSize;
addMarkerFaceColor = p.Results.addMarkerFaceColor;
if( isempty( addMarkerFaceColor ) )
    addMarkerFaceColor = addLineColor;
end
addMarkerEdgeColor = p.Results.addMarkerEdgeColor;
if( isempty( addMarkerEdgeColor ) )
    addMarkerEdgeColor = addLineColor;
end
ax = p.Results.plotAxes;
%
trajTbl.Properties.VariableNames = lower(trajTbl.Properties.VariableNames);
kLat = find(strncmp(trajTbl.Properties.VariableNames,'lat',3),1);
if( ~isempty(kLat) )
    trajTbl.Properties.VariableNames{kLat} = 'latitude';
else
    error('No latitude variable in trajTbl')
end
kLon = find(strncmp(trajTbl.Properties.VariableNames,'lon',3),1);
if( ~isempty(kLon) )
    trajTbl.Properties.VariableNames{kLon} = 'longitude';
else
    error('No longitude variable in trajTbl')
end
kDateTime = find(strncmp(trajTbl.Properties.VariableNames,'datetime',8), 1);
if( ~isempty(kDateTime) )
    trajTbl.mtime = datenum(trajTbl.datetime);
end
if( ~all(ismember( {'latitude','longitude','mtime'}, trajTbl.Properties.VariableNames ) ) )
    error( 'trajTbl must contain the columns latitude, longitude and mtime' )
end
%
if( isempty(latLim) ) 
    latLim = axis_set( trajTbl.latitude,[-90 90] );
end
if(isempty(yTick))
    yTick = tick_set( latLim, 5 );
end
if( isempty(lonLim) )
    lonLim = axis_set( trajTbl.longitude,[-180 360] );
end
if(isempty(xTick))
    xTick = tick_set( lonLim, 5 );
end
if( isempty(ax) )
    ax = axes;
else
    axes(ax)
    cla(ax)
end
% 
m_proj( projection,'lat',latLim,'long',lonLim );
if( isempty( GSHHS ) )
    m_coast('patch',landColor);
else
    m_gshhs(GSHHS,'patch',landColor);
end
m_grid( 'box',box, 'linestyle',':','linewidth',2,'tickdir','out','fontsize',axesFontSize,...
    'XaxisLocation',XaxisLocation, 'YaxisLocation',YaxisLocation,...
    'yTick',yTick, 'xTick',xTick );
m_line( trajTbl.longitude, trajTbl.latitude, 'Color',lineColor, 'lineWidth',lineWidth );
hold on
if( ~isempty(addLine) )
    m_line( addLine(:,1), addLine(:,2), 'Color',addLineColor,...
        'LineWidth',addLineWidth, 'LineStyle', addLineStyle,...
        'Marker',addMarker, 'MarkerSize',addMarkerSize,...
        'MarkerFaceColor',addMarkerFaceColor, 'MarkerEdgeColor',addMarkerEdgeColor );
end
nFloats = length(trajTbl.mtime);
if(ischar(colorMap))
    colorScale = colormap( feval( colorMap, nFloats ) );
else
    colorScale = interp1(linspace(0,1,size(colorMap,1)),colorMap,linspace(0,1,nFloats));
end
m_scatter( trajTbl.longitude, trajTbl.latitude, markerSize.^2, colorScale,'filled',...
    'MarkerEdgeColor',MarkerEdgeColor );
nLeg = min([nLeg,nFloats]);
if( nLeg > 0 )
    kInLeg = round(linspace(1,nFloats,nLeg));
    h_argo = gobjects(nLeg,1);
    for k_float = 1:nLeg 
        h_argo(k_float) = m_plot( trajTbl.longitude(kInLeg(k_float)), trajTbl.latitude(kInLeg(k_float)), ...
            'MarkerEdgeColor',MarkerEdgeColor,...
            'Marker','o','MarkerFaceColor',colorScale(kInLeg(k_float),:),'MarkerSize',markerSize,...
            'LineStyle','none', 'Visible','off');
    end
    hLeg = legend( h_argo, {datestr(trajTbl.mtime(kInLeg),'DD-mmm-yyyy')},...
        'Location',legendLocation );
    hLeg.FontSize = legendFontSize;
end
title( mapTitle, 'FontSize',titleFontSize )

end

function [ x_lim, x_ticks ] = axis_set( x, xLim )
%
x_min = min( x ); x_max = max( x );
d_x = x_max - x_min;
if( d_x > 180 )
    indx_w = ( x < 0 );
    x(indx_w) = x(indx_w) + 360;
    x_min = min( x ); x_max = max( x );
    d_x = x_max - x_min;
end
x_lim = [ max(x_min-d_x/2,xLim(1)) min(x_max+d_x/2,xLim(2)) ];
x_ticks = tick_set( x_lim, 5 );

end
%
function x_ticks = tick_set( x_lim, n_tick_max )
    x_int = [0.5,1,2,5,10,20];
    for k = 1:length(x_int)
        x_ticks = -180:x_int(k):360;
        indx_in = (x_ticks>=x_lim(1)) & (x_ticks<=x_lim(2));
        if(sum(indx_in)<n_tick_max)
            break
        end
    end
end


