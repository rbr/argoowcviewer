function [ Vq, Xq1, Yq1 ] = calcTimeSeriesPres( vInp, yPres, xMtime, varargin )
%
%   Interpolate data to 2D matrix (Vq) with xMtime as X-axis and yPres as Y-axis
%       The output to be used by the function plotTimeSeriesPres
%
%   Syntax: [ Vq, Xq1, Yq1 ] = calcTimeSeriesPres( vInp, yPres, xMtime,... )
% 
%   Input [Required]:
%       vInp - numeric 2D matrix of size nLayers*nProf
%       yPres - numeric 2D matrix of pressures (size nLayers*nProf)
%       xMtime - datenum vector of mtime (size nProf)
%   Input [Optional]:
%       presRange - the range of PRES (default [min(yPres) max(yPres)]
%       nXq (integer scalar) - X-size of the resulting matrix (default 100)
%       nYq (integer scalar) - Y-size of the resulting matrix (default 100)
%
%   Output:  
%       Vq - resulting matrix (double, size [nYq*nXq])
%       Xq1 - resulting X-axis (datenum, size nXq)
%       Yq1 - resulting Y-axis (double, size nYq)
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-20
%
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
p = inputParser;
addRequired(p,'vInp',@ismatrix);
addRequired(p,'yPres',@ismatrix);
addRequired(p,'xMtime',@isvector);
addParameter(p,'presRange',[],is2nums);
addParameter(p,'nXq',100,@isnumeric);
addParameter(p,'nYq',100,@isnumeric);
parse(p, vInp, yPres, xMtime, varargin{:} )
vInp = p.Results.vInp;
yPres = p.Results.yPres;
xMtime = p.Results.xMtime;
presRange = p.Results.presRange;
nXq = p.Results.nXq;
nYq = p.Results.nYq;
%
if(isempty(presRange))
    presRange = [min(yPres(:)) max(yPres(:))];
end
nProf = length( xMtime );
if(~all(size(vInp)==size(yPres)))
    error('The size of two input matrices is different')
end
if((size(vInp,2)~=nProf)||(size(yPres,2)~=nProf))
    error('The size of two matrices and the time vector are different')
end
%
VV = NaN( nYq, nProf );
Xq1 = linspace( min(xMtime), max(xMtime), nXq )';
Yq1 = linspace( presRange(1), presRange(2), nYq )'; 
for k_prof=1:nProf
    VV1 = vInp( :,k_prof );
    yPres1 = yPres( :,k_prof );
    indx_fin = isfinite(VV1) & isfinite(yPres1);
    if( sum( indx_fin ) > 1 )
        PP2 = yPres1(indx_fin);
        VV2 = VV1(indx_fin);
        while( any( diff( PP2 ) <= 0 ) )
            indx_sel = [ ( diff(PP2) > 0 ); true];
            PP2 = PP2( indx_sel );
            VV2 = VV2( indx_sel );
        end 
        VV(:,k_prof) = interp1( PP2, VV2, Yq1, 'pchip',NaN ); 
    end
end
[ Xq, Yq ] = meshgrid( Xq1, Yq1 );
% Move back for 1 hour the stations with duplicated xMtime
while( any( diff(xMtime) <= 0 ) )
    dt = diff(xMtime);
    indx0 = ( dt == 0 );
    xMtime( indx0 ) = xMtime( indx0 ) - 1/24;
end
Vq = interp2( xMtime, Yq1, VV, Xq, Yq, 'linear',NaN );
%
end


