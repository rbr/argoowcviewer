function [ WOA_TEMP, WOA_SAL, WOA_PRES ] = climWOA( LAT, LONG, mtime, WOA4,...
    WOA1path, WOA4path )
%
%   Finds the WOA1 (or WOA4) Climatology in the path WOA1path (WOA4path) and selects 
%   the profiles of climatic temperature and salinity for the locations and seasons
%   determined by LAT, LONG and mtime.    
%
%   In argoOWCviewer, clim_get_WOA is called from the function climAlongTraj
%
% Syntax: [ WOA_TEMP, WOA_SAL, WOA_PRES ] = clim_get_WOA( LAT, LONG, mtime, WOA4,...
%               WOA1path, WOA4path );
%
%   Input [Required]:
%       LAT - latitude (numeric vector of length nProf)
%       LONG - longitude (numeric vector of length nProf)
%       mtime - time in datenum format (numeric vector of length nProf)
%       WOA4 - logical, getting data from WOA4 (true) or WOA1 (false)
%       WOA1path, WOA4path - paths to WOA climatologies
%
%   Output:
%       WOA_TEMP, WOA_SAL, WOA_PRES - arrays of size(nLayers,nProf).
%       WOA_TEMP - temperature
%       WOA_SAL - salinity
%       WOA_PRES - pressure
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-25
%
p = inputParser;
addRequired(p,'LAT',@isnumeric);
addRequired(p,'LONG',@isnumeric);
addRequired(p,'mtime',@isnumeric);
addRequired(p,'WOA4',@islogical);
addRequired(p,'WOA1path',@ischar);
addRequired(p,'WOA4path',@ischar);
parse(p, LAT, LONG, mtime, WOA4, WOA1path, WOA4path )
% 
LAT = p.Results.LAT;
LONG = p.Results.LONG;
mtime = p.Results.mtime;
WOA4 = p.Results.WOA4;
WOA1path = p.Results.WOA1path;
WOA4path = p.Results.WOA4path;
% Check the inputs, set defaults
if( (length(LAT)~=length(LONG)) && (length(LAT)~=length(mtime)))
    error('LAT, LONG and mtime must be the same length')
end
if( ~WOA4 )
    if( ~exist(WOA1path,'dir') )
        error(['No dir ',WOA1path] )
    end
else
    if( ~exist(WOA4path,'dir') )
        error(['No dir ',WOA4path] )
    end
end
%
if( WOA4 )
    WOA_path = WOA4path;
    WOAt_path_an = [ WOA_path,'woa18_decav_t00_04.nc' ];
    WOAs_path_an = [ WOA_path,'woa18_decav_s00_04.nc' ];
else
    WOA_path = WOA1path;
    WOAt_path_an = [ WOA_path,'woa18_A5B7_t00_01.nc' ];
    WOAs_path_an = [ WOA_path,'woa18_A5B7_s00_01.nc' ];
end
% 
WOAlat = ncread( WOAt_path_an, 'lat' );
WOAlon = ncread( WOAt_path_an, 'lon' );
WOAdepth = ncread( WOAt_path_an, 'depth' );
nLayers = length( WOAdepth );
%
nProf = length( mtime );
WOA_TEMP = NaN( nLayers, nProf );
WOA_SAL = NaN( nLayers, nProf );
WOA_PRES = NaN( nLayers, nProf );
h_wait = waitbar( 0, 'Extracting data from WOA...' );
wax = findobj(h_wait, 'type','axes');
tax = get(wax,'title');
set(tax,'fontsize',16)
for k_prof = 1:nProf
    waitbar( k_prof / nProf, h_wait )
    lat1 = LAT(k_prof);
    lon1 = LONG(k_prof);
    if( lon1 > 180 )
        lon1 = lon1 - 360;
    end
    [~,kLat] = min(abs(WOAlat-lat1));
    [~,kLon] = min(abs(WOAlon-lon1));
    WOAtemp = squeeze(ncread( WOAt_path_an, 't_an',[kLon kLat 1 1],[1 1 Inf Inf] ) );
    WOAsal = squeeze(ncread( WOAs_path_an, 's_an',[kLon kLat 1 1],[1 1 Inf Inf] ) ); % Salinity interpolated
    %
    mtime1 = mtime(k_prof);
    [ ~, month1, ~ ] = ymd( datetime(mtime1,'ConvertFrom','datenum' ) );
    % WOA monthly temperature and salinity files
    if( WOA4 )
        WOAt_fn = [ 'woa18_decav_t',num2str(month1,'%02.0f'),'_04.nc' ];
        WOAs_fn = [ 'woa18_decav_s',num2str(month1,'%02.0f'),'_04.nc' ];
    else
        WOAt_fn = [ 'woa18_A5B7_t',num2str(month1,'%02.0f'),'_01.nc' ];
        WOAs_fn = [ 'woa18_A5B7_s',num2str(month1,'%02.0f'),'_01.nc' ];
    end
    WOAt_path = [ WOA_path, WOAt_fn ];
    WOAs_path = [ WOA_path, WOAs_fn ];
    temp_m = squeeze(ncread( WOAt_path, 't_an',[kLon kLat 1 1],[1 1 Inf Inf] ) );
    sal_m = squeeze(ncread( WOAs_path, 's_an',[kLon kLat 1 1],[1 1 Inf Inf] ) ); % Salinity interpolated
    n_depthm = length(temp_m);
    WOAtemp(1:n_depthm) = temp_m;
    WOAsal(1:n_depthm) = sal_m;
    WOA_TEMP( :,k_prof ) = WOAtemp;
    WOA_SAL( :,k_prof ) = WOAsal;
    WOA_PRES( :,k_prof ) = gsw_p_from_z( -WOAdepth, LAT(k_prof) );
end
close( h_wait )
%
end

