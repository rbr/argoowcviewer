function ax = plotOWCsOffsets( owcTbl, varargin )
%
%   Plot the salinity offsets calculated by OWC method
%
%   Syntax: ax = plotOWCsOffsets( owcStr, ... )
%
%   Input [Required]:
%           owcTbl - includes the columns mtime and the OWC results characterizing profiles:
%              (avg_Soffset,avg_Soffset_err,avg_Staoffset,avg_Staoffset_err)
%   Input [Optional]:
%       plotAxes - Axes object. Created by default
%       xLim - X-axis limits (datenum, default min/max)
%       timeTickStyle - the style of X-axis (mtime) ticks:
%           {'auto' (default), 'years','months','days'}
%       timeTickInterval - the interval between the time ticks 
%           (numeric, defailt 1)
%       fontSize - font size of the axes (numeric) (default 12)
%       yLim - Y (deltaS) limits (numeric 2x1) (default auto)
%       yLab - Y label (default '\Delta S')
%       yLabFontSize - font size of the Y label
%       title - figure title (char)
%       titleFontSize - title font size (numeric, default 20)
%       sOffColor - the color of S offset (default 'b')
%       sOffLineWidth - line width of S offset (default 1)
%       sOffMarker - marker of S offset (default 'o')
%       sOffFaceColor - face color of the S offset symbols (default [0 0.5 1])
%       sOffMarkerSize - marker size of the S offset symbols (default 6)
%       profFitColor - the color of profile fit coefficients (default 'r')
%       profFitLineWidth - the line width of profile fit coefficients (default 2)
%       profFitErrEdgeColor - the edge color of profile fit coefficient errors 
%                               (default 'k')
%       profFitErrEdgeWidth - the edge width of profile fit coefficient
%                               errors (default 1)
%       profFitErrFaceColor - the face color of profile fit coefficient errors
%                               (default [1 0.8 0.8])
%       zeroColor - the color of zero (default 'k')
%       zeroLineWidth - the width of zero line (default 1)
%
%   Output:     
%       ax - Axes object. 
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-05
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'owcTbl',@istable);
addParameter(p,'plotAxes',[],isaxes);
addParameter(p,'xLim',[],is2nums);
istimetick = @(x) (ischar(x) && (ismember(x,{'auto','years','months','days'})));
addParameter(p,'timeTickStyle','auto',istimetick);
addParameter(p,'timeTickInterval',1,@isnumeric);
addParameter(p,'fontSize',12,@isnumeric);
addParameter(p,'yLim',[],is2nums);
addParameter(p,'yLab','\Delta S',@ischar);
addParameter(p,'yLabFontSize',16,@isnumeric);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
addParameter(p,'sOffColor','b',iscolor);
addParameter(p,'sOffLineWidth',1,@isnumeric);
addParameter(p,'sOffMarker','o',@ischar);
addParameter(p,'sOffFaceColor',[0 0.5 1],iscolor);
addParameter(p,'sOffMarkerSize',6,@isnumeric);
addParameter(p,'profFitColor','r',iscolor);
addParameter(p,'profFitLineWidth',2,@isnumeric);
addParameter(p,'profFitErrEdgeColor','k',iscolor);
addParameter(p,'profFitErrEdgeWidth',1,@isnumeric);
addParameter(p,'profFitErrFaceColor',[1 0.8 0.8],iscolor);
addParameter(p,'zeroColor','k',iscolor);
addParameter(p,'zeroLineWidth',1,@isnumeric);
parse(p, owcTbl, varargin{:} )
owcTbl = p.Results.owcTbl;
ax = p.Results.plotAxes;
xLim = p.Results.xLim;
timeTickStyle = p.Results.timeTickStyle;
timeTickInterval = p.Results.timeTickInterval;
fontSize = p.Results.fontSize;
yLim = p.Results.yLim;
yLab = p.Results.yLab;
yLabFontSize = p.Results.yLabFontSize;
plotTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
sOffColor = p.Results.sOffColor;
sOffLineWidth = p.Results.sOffLineWidth;
sOffMarker = p.Results.sOffMarker;
sOffFaceColor = p.Results.sOffFaceColor;
sOffMarkerSize = p.Results.sOffMarkerSize;
profFitColor = p.Results.profFitColor;
profFitLineWidth = p.Results.profFitLineWidth;
profFitErrEdgeColor = p.Results.profFitErrEdgeColor;
profFitErrEdgeWidth = p.Results.profFitErrEdgeWidth;
profFitErrFaceColor = p.Results.profFitErrFaceColor; 
zeroColor = p.Results.zeroColor;
zeroLineWidth= p.Results.zeroLineWidth;
%
% Check the column names in owcTbl
owcColNames = {'mtime','avg_Soffset','avg_Soffset_err','avg_Staoffset','avg_Staoffset_err'};
iCol = ismember(owcColNames,owcTbl.Properties.VariableNames);
if(~all(iCol))
    error(['No columns in owcTbl: ',strjoin(owcColNames(~iCol),',')])
end
%
if( isempty(ax) )
    ax = axes;
else
    axes(ax)
    cla(ax)
end
if( isempty(xLim) )
    xLim = [min(owcTbl.mtime) max(owcTbl.mtime)];
end
%
plot( owcTbl.mtime, owcTbl.avg_Soffset, 'Color',sOffColor, 'LineWidth',sOffLineWidth,...
    'Marker',sOffMarker, 'MarkerFaceColor',sOffFaceColor, 'MarkerSize',sOffMarkerSize );
hold(ax,'on')
errorbar( owcTbl.mtime, owcTbl.avg_Soffset, 2*owcTbl.avg_Soffset_err,...
    'Color',sOffColor, 'LineWidth',sOffLineWidth, 'Marker',sOffMarker,...
    'MarkerFaceColor',sOffFaceColor, 'MarkerSize',sOffMarkerSize )
patch( [ owcTbl.mtime; flip(owcTbl.mtime) ],...
    [ owcTbl.avg_Staoffset+owcTbl.avg_Staoffset_err; flip(owcTbl.avg_Staoffset-owcTbl.avg_Staoffset_err) ],...
    profFitErrFaceColor, 'EdgeColor',profFitErrEdgeColor, 'LineWidth',profFitErrEdgeWidth );
plot( owcTbl.mtime, owcTbl.avg_Staoffset, 'Color',profFitColor, 'LineWidth',profFitLineWidth );
plot( [ owcTbl.mtime(1), owcTbl.mtime(end) ], [0,0], 'Color',zeroColor, 'LineWidth',zeroLineWidth )
xlim(xLim)
if(~isempty(yLim))
    ylim(yLim)
end
set( ax, 'FontSize',fontSize )
ylabel(yLab, 'FontSize',yLabFontSize)
grid('on')
box('on')
switch timeTickStyle
    case 'auto'
        xTicks = get( ax, 'XTick' );
        dateFmt = 'ddmmmyy';
    case 'days'
        xTicks = calcTimeTicks( owcTbl.mtime, timeTickStyle, timeTickInterval );
        dateFmt = 'ddmmmyy';
    case {'months','years'}
        xTicks = calcTimeTicks( owcTbl.mtime, timeTickStyle, timeTickInterval );
        dateFmt = 'mmmyy';
end
set( ax, 'XTick',xTicks, 'XTickLabel',datestr(xTicks,dateFmt) )
set( ax,'TickLength',[0.01, 0.01], 'TickDir','out','LineWidth',2)
title( ax, plotTitle, 'FontSize', titleFontSize )
set(ax,'Layer','top')

end


