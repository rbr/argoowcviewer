function misValTbl = calcMisValTbl( dataStr )
%
%   Calculate the table of the number of missing values in the Argo data structure 
%
%   Syntax: misValTbl = calcMisValTbl( dataStr )
%
%   Input [Required]:
%       dataStr - the structure with Argo data (PRES, TEMP, SAL, PTMP), 
%           each a double matrix of size [nLayers*nProf]
%
%   Output:
%       misValTbl - table with rows (PRES, TEMP, SAL, PTMP) 
%               and columns: nTot (total number of values), 
%                            nNaN (the number of NaNs), 
%                            pcNaN (the percent of NaNs)
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-16
%
p = inputParser;
addRequired(p,'dataStr',@isstruct);
parse(p, dataStr );
dataStr = p.Results.dataStr;
%
strFieldNames = fieldnames( dataStr );
nFields = length(strFieldNames);
%
misValTbl = array2table(NaN(nFields,3));
misValTbl.Properties.VariableNames = {'nTot','nNaN','pcNaN'};
misValTbl.Properties.RowNames = strFieldNames;
for kField = 1:nFields
    misValTbl.nTot(kField) = length(dataStr.(strFieldNames{kField})(:));
    misValTbl.nNaN(kField) = sum(isnan(dataStr.(strFieldNames{kField})(:)));
    misValTbl.pcNaN(kField) = 100 .* misValTbl.nNaN(kField) ./ misValTbl.nTot(kField);
end

end

