function ax = plotTSQC( dataStr, varargin )
%
%   Plot the TS diagram with QC (quality control) flags
%
%   Syntax: plotTSQC( dataStr, ... )
%
%   Input [Required]:
%       dataStr - the structure with Argo data (PRES, SAL, SAL_QC, PTMP), 
%           each a double matrix of size [nLayers*nProf]
%   Input [Optional]:
%       sLim - salinity limits (numeric, size(2,1))
%       ptLim - potential temperature limits (numeric, size(2,1))
%       longitude, latitude - used for density contours (default 180,35)
%       densLevels - density levels (input argument 'levels' for contour function) 
%           numeric scalar - the number of levels 
%           vector of monotonically increasing values - levels (default
%           0:40)
%       densLineSpec - density coutours line specifications, default ':k')
%       densLineWidth - density coutours line width (default 0.5)
%       densLabColor - density line labels color (default 'k')
%       densLabSpacing - density line labels spacing (point units;
%           default 1000)
%       densLabFontSize - density line labels font size (default 12)
%       lineWidth - the width of the lines (default 0.5)
%       lineStyle - the style of the lines (default '-')
%       lineColor - the color of the lines (default [0.8 0.8 0.8])
%       yLab - Y label (char) (default 'Potential temperature (^oC)')
%       xLab - X label (char) (default 'Salinity')
%       fontSize - font size of the axes (numeric; default 12)
%       title - figure title
%       titleFontSize - title font size (numeric, default 20)
%       colormap - predefined colormap: 'jet (default)','parula',etc.)
%       marker - marker (default 'o')
%       markerEdgeColor - marker edge color (default [0.5 0.5 0.5])
%       markerSize - marker size (default 6)
%       legendFontSize - legend font size (default 12)
%       legendLocation - legend location (default 'best')
%       plotAxes - Axes object. Created by default
%
%   Output:     
%       ax - Axes object. Can be used to make future modifications to the axes.
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-14
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'dataStr',@isstruct);
addParameter(p,'sLim',[],is2nums);
addParameter(p,'ptLim',[],is2nums);
addParameter(p,'longitude',180,@isnumeric);
addParameter(p,'latitude',35,@isnumeric);
addParameter(p,'densLevels',0:40,@isnumeric);
addParameter(p,'densLineSpec',':k',@ischar);
addParameter(p,'densLineWidth',0.5,@isnumeric);
addParameter(p,'densLabColor','k',iscolor);
addParameter(p,'densLabSpacing',1000,@isnumeric);
addParameter(p,'densLabFontSize',12,@isnumeric);
addParameter(p,'lineWidth',0.5,@isnumeric);
addParameter(p,'lineStyle','-',@ischar);
addParameter(p,'lineColor',[0.8 0.8 0.8],iscolor);
addParameter(p,'yLab','Potential temperature (^oC)',@ischar);
addParameter(p,'xLab','Salinity',@ischar);
addParameter(p,'fontSize',12,@isnumeric);
addParameter(p,'title','',@ischar);
addParameter(p,'titleFontSize',20,@isnumeric);
addParameter(p,'colormap','jet',@ischar);
addParameter(p,'marker','o',@ischar);
addParameter(p,'markerEdgeColor',[0.5 0.5 0.5],iscolor);
addParameter(p,'markerSize',10,@isnumeric);
addParameter(p,'legendFontSize',16,@isnumeric);
addParameter(p,'legendLocation','best',@ischar);
addParameter(p,'plotAxes',[],isaxes);
parse(p, dataStr, varargin{:} )
dataStr = p.Results.dataStr;
sLim = p.Results.sLim;
ptLim = p.Results.ptLim;
longitude = p.Results.longitude;
latitude = p.Results.latitude;
densLevels = p.Results.densLevels;
densLineSpec = p.Results.densLineSpec;
densLineWidth = p.Results.densLineWidth;
densLabColor = p.Results.densLabColor;
densLabSpacing = p.Results.densLabSpacing;
densLabFontSize = p.Results.densLabFontSize;
lineWidth = p.Results.lineWidth;
lineStyle = p.Results.lineStyle;
lineColor = p.Results.lineColor;
yLab = p.Results.yLab;
xLab = p.Results.xLab;
fontSize = p.Results.fontSize;
plotTitle = p.Results.title;
titleFontSize = p.Results.titleFontSize;
colorMap = p.Results.colormap;
dotMarker = p.Results.marker;
markerEdgeColor = p.Results.markerEdgeColor;
markerSize = p.Results.markerSize;
legendFontSize = p.Results.legendFontSize;
legendLocation = p.Results.legendLocation;
ax = p.Results.plotAxes;
%
if( ~all(isfield(dataStr,{'PRES','TEMP','SAL','PTMP'})) )
    error( 'The input structure does not contain all fields: PRES,TEMP,SAL,PTMP' )
end
if( any(size(dataStr.PRES)~=size(dataStr.TEMP)) || any(size(dataStr.PRES)~=size(dataStr.SAL)) ||...
        any(size(dataStr.PRES)~=size(dataStr.PTMP)) )
    error('The sizes of the structure fields are different')
end
%
if( isempty(sLim) )
    sLim = [min(dataStr.SAL(:)) max(dataStr.SAL(:))];
    dS = diff(sLim);
    sLim = [sLim(1)-dS/10 sLim(2)+dS/10];
end
if( isempty(ptLim) )
    ptLim = [min(dataStr.PTMP(:)) max(dataStr.PTMP(:))];
    dPT = diff(ptLim);
    ptLim = [ptLim(1)-dPT/10 ptLim(2)+dPT/10];
end
colormapList = {'parula','jet','hsv','hot','cool','spring','summer',...
    'autumn','winter','gray','bone','copper','pink'};
if( ~ismember( colorMap, colormapList ) )
    error( [ colorMap, ' is not a valid colormap' ] )
end
if( isempty(ax) )
    ax = axes;
else
    cla(ax)
end
% Calculate density matrix
x_dim = 50; y_dim = 50;
Theta_i = linspace( ptLim(1), ptLim(2), y_dim );  
Salin_i = linspace( sLim(1), sLim(2), x_dim );
Dens2d = zeros( y_dim, x_dim );
for j = 1:y_dim
    for i = 1:x_dim
        [ SA, ~ ] = gsw_SA_from_SP( Salin_i(i), 0, longitude, latitude );
        CT = gsw_CT_from_pt( SA, Theta_i(j) );
        Dens2d(j,i) = gsw_rho( SA, CT, 0 );
    end
end
Dens2d = Dens2d - 1000;
%
psalQCtbl = calcQcTbl( dataStr.SAL_QC );
psalQCtbl = psalQCtbl(psalQCtbl.n>0,:);
nQC = height( psalQCtbl );
colorScale = colormap( feval( colorMap, nQC ) );
%
plot( dataStr.SAL(:), dataStr.PTMP(:), '.', 'Visible','off' )
[c,h] = contour( Salin_i, Theta_i, Dens2d, densLevels, densLineSpec, 'LineWidth',densLineWidth );
clabel( c, h, 'LabelSpacing', densLabSpacing, 'FontSize',densLabFontSize, 'Color',densLabColor );
hold on
[~,nProf] = size(dataStr.PRES);
for kProf = 1:nProf
    XYZ = [ dataStr.PRES(:,kProf), dataStr.SAL(:,kProf), dataStr.PTMP(:,kProf) ];
    [~,indxSort] = sort( XYZ(:,1) );
    XYZ = XYZ(indxSort,:);
    plot( XYZ(:,2), XYZ(:,3), 'Color',lineColor,...
        'LineWidth',lineWidth, 'LineStyle',lineStyle );
end
hQC = gobjects(nQC,1);
for kQC = 1:nQC
    indxQC = ( dataStr.SAL_QC == psalQCtbl.code(kQC) ); 
    hQC(kQC) = plot( dataStr.SAL(indxQC), dataStr.PTMP(indxQC), 'Marker',dotMarker,...
        'LineStyle','none','MarkerSize',markerSize,...
        'MarkerFaceColor',colorScale(kQC,:),'MarkerEdgeColor',markerEdgeColor );
end
xlim(sLim)
ylim(ptLim)
ylabel(yLab)
xlabel(xLab)
set( gca, 'FontSize',fontSize )
title(plotTitle, 'FontSize',titleFontSize )
% Legend
hLeg = legend( hQC,...
    [ repmat('QC=''',nQC,1),char(psalQCtbl.code), repmat(''' (',nQC,1),...
    num2str(psalQCtbl.n), repmat(')',nQC,1) ],...
    'Location',legendLocation );
hLeg.FontSize = legendFontSize;
%
end



