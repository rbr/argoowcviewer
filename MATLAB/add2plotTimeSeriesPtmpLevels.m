function add2plotTimeSeriesPtmpLevels( PTMP, mtime, contLevels, varargin ) 
%
%   Adds to 2D image plot of time series (X) vs. potential temperature (Y) 
%       theta levels as horizontal lines 
%
%   Syntax: add2plotTimeSeriesPtmpLevels( PTMP, mtime, contLevels, ... )
%
%   Input [Required]:
%       PTMP - Potential temperature. Matrix (double), (nLayers,nProf) 
%       mtime - datenum format, size nProf
%       contLevels - potential temperature levels to be plotted
%   Input [Optional]:
%       plotAxes - Axes object. Created by default
%       ptmpRange - the range of potential temperature to plot (numeric 2x1)
%       lineColor - line color (default 'k')
%       lineWidth - line width (numeric) (default 1)
%       lineStyle - line style (default '-')
%   
%   Output:     no
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-13
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'PTMP',@ismatrix);
addRequired(p,'mtime',@isnumeric);
addRequired(p,'contLevels',@isnumeric);
addParameter(p,'plotAxes',[],isaxes);
addParameter(p,'ptmpRange',[],is2nums);
addParameter(p,'lineColor','k',iscolor);
addParameter(p,'lineWidth',1,@isnumeric);
addParameter(p,'lineStyle','-',@ischar);
parse(p, PTMP, mtime, contLevels, varargin{:} )
PTMP = p.Results.PTMP;
mtime = p.Results.mtime;
contLevels = p.Results.contLevels;
ax = p.Results.plotAxes;
ptmpRange = p.Results.ptmpRange;
lineColor = p.Results.lineColor;
lineWidth = p.Results.lineWidth;
lineStyle = p.Results.lineStyle;
%
if(isempty(lineWidth))
    lineWidth=1;
end
%
[nLayers,nProf] = size(PTMP);
if( length(mtime)~=nProf )
    error('The length of mtime is not equal to the size of matrix')
end
%
if( isempty(ax) )
    ax = gca;
end
if( isempty(ptmpRange) )
    ptmpRange = get(ax,'YLim');
end
hold(ax,'on')
%
for kLevel = 1:length(contLevels)
    plot(ax,[min(mtime) max(mtime)],repmat(contLevels(kLevel),2,1),...
        'Color',lineColor, 'lineWidth',lineWidth, 'lineStyle',lineStyle )
end
    
% [ PTMPq, Xq1, Yq1 ] = calcTimeSeriesPres( PTMP, PRES, mtime, 'presRange',presRange );
% if( length(contLevels) > 1 )
%     contLines = contour( Xq1, Yq1, PTMPq, contLevels, 'Visible','off' );
% else
%     contLines = contour( Xq1, Yq1, PTMPq, [contLevels contLevels], 'Visible','off' );
% end
% nCont = size(contLines,2);
% if( nCont>0 )
%     kCont = 1;
%     while(kCont<nCont)
%         kContStart = kCont+1;
%         kContEnd = kCont+contLines(2,kCont);
%         plot( ax, contLines(1,kContStart:kContEnd),contLines(2,kContStart:kContEnd),...
%             'Color',lineColor, 'lineWidth',lineWidth, 'lineStyle',lineStyle )
%         kCont = kContEnd+1;
%     end
% end
hold(ax,'off')
%
end


