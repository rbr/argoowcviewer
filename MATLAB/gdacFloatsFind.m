function floatTbl = gdacFloatsFind( latLim, lonLim, timeLim, varargin )
%
%   Finds in the IFREMER index file the Argo profiles within the provided 
%   lat, lon and time limits and returns the table of Argo floats with the numbers of
%   profiles in the area of interest
%
%   Syntax: floatTbl = gdacFloatsFind( latLim, lonLim, timeLim,... )
%
%   Input [Required]:
%       latLim - latitude limits (numeric vector)
%       lonLim - longitude limits (numeric vector)
%           If the length of latLim and lonLim is 2, they indicate the
%               limits of rectangular polygons
%           Otherwise, they mean the boundaries of the region (see 'inpolygon' function)
%       timeLim - time limits (datenum 2*1)
%   Input [Optional]:
%       ocean - ocean code {'A','P','I'} to speed up the search
%       minProf - minimum number of profiles (default 1)
%       maxFloats - maximum number of floats (default Inf)
%       saveDir - the folder to save the argo profile index file
%       indxFilePath - use this index file instead of downloading
%           
%   Output:
%       floatTbl - the table with columns:
%           PLATFORM_NO - the platform ID
%           nProf - the number of profiles in the region of interest
%           dac - the name of DAC
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-15
%
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
is1num = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==1));
p = inputParser;
addRequired(p,'latLim',@isnumeric);
addRequired(p,'lonLim',@isnumeric);
addRequired(p,'timeLim',is2nums);
addParameter(p,'ocean',[],@ischar);
addParameter(p,'minProf',1,is1num);  
addParameter(p,'maxFloats',Inf,is1num);
addParameter(p,'saveDir',[],@ischar);
addParameter(p,'indxFilePath',[],@ischar);
parse(p, latLim, lonLim, timeLim, varargin{:} )
latLim = p.Results.latLim;
lonLim = p.Results.lonLim;
timeLim = p.Results.timeLim;
ocean = p.Results.ocean;
minProf = p.Results.minProf;
maxFloats = p.Results.maxFloats; 
saveDir = p.Results.saveDir;
indxFilePath = p.Results.indxFilePath;
% 
if( length(latLim)~=length(lonLim) )
    error('Different length of latLim and lonLim')
end
if( length(latLim)==2 )
    lonLim = lonLim([1 2 2 1 1]);
    latLim = latLim([1 1 2 2 1]);
end
%
if( isempty(saveDir) )
    saveDir = pwd;
end
if( exist( saveDir, 'dir' ) ~= 7 )
    error(['Does not exist: ',saveDir])
end
%
if( ~isempty(indxFilePath) )
    if( exist(indxFilePath,'file') ~= 2 )
        error(['No file ',indxFilePath])
    end
else    % Connect to ftp
    indxFn = 'ar_index_global_prof.txt';
    disp(['Downloading the file',indxFn,'...wait...'])
    h_wait = waitbar( 0.5, ['Downloading the file',indxFn,'...wait...'] );
    startDir = pwd;
    cd( saveDir )
    ifremer = ftp('ftp.ifremer.fr');
    cd(ifremer,'/ifremer/argo/');
    mget(ifremer,[indxFn,'.gz']);
    gunzip([indxFn,'.gz']);
    delete([indxFn,'.gz'])
    indxFilePath = [saveDir,filesep,indxFn];
    cd(startDir)
    close( h_wait )
end
%
% Read the profile list as a table
h_wait = waitbar( 0.25, 'Reading the index table...' );
disp(['Reading the index table...'])
profTbl = readtable( indxFilePath, 'HeaderLines',8 );

% Select the profiles in the ocean and time period
h_wait = waitbar( 0.5, h_wait, 'Selecting the profiles...' );
disp(['Selecting the profiles'])
indxSel = ( profTbl.date >= str2double(datestr(timeLim(1),'yyyymmddHHMMSS')) )...
    & ( profTbl.date <= str2double(datestr(timeLim(2),'yyyymmddHHMMSS')) );
if( ~isempty(ocean) )
    indxSel = indxSel & strcmp(profTbl.ocean,ocean);
end
profTbl = profTbl(indxSel,:);
% Select the profiles in the region
profTbl.in = inpolygon( profTbl.longitude,profTbl.latitude, lonLim,latLim );
profTbl = profTbl(profTbl.in,:);
% In the profTbl, fill the columns 'PLATFORM_NO' and 'dir'
nProf = height( profTbl );
profTbl.PLATFORM_NO = cell( nProf, 1 );
profTbl.dir = cell( nProf, 1 );
for kProf = 1:nProf
    waitbar(0.5+0.5*kProf/nProf, h_wait );
    if(mod(kProf,1000)==0)
        disp([kProf,nProf])
    end
    prof_file = strsplit( profTbl.file{kProf},'/' );
    profTbl.dir{kProf} = prof_file{1};
    profTbl.PLATFORM_NO{kProf} = prof_file{2};
end
close( h_wait )
% Remove the column 'file'
profTbl.file = [];
% Create the table with floats operating in the box
disp(['Creating the table of floats'])
floatTbl = cell2table(tabulate( profTbl.PLATFORM_NO ));
floatTbl.Properties.VariableNames = {'PLATFORM_NO','nProf','perc'};
floatTbl.perc = [];
[ ~, indx_sort ] = sort( floatTbl.nProf, 1, 'descend' );
floatTbl = floatTbl(indx_sort,:);
% Select the floats using minimum number of profiles in the box
indxFloats = ( floatTbl.nProf >= minProf );
floatTbl = floatTbl( indxFloats, : );
% Select the floats using maximum table size
maxFloats = min([maxFloats,height(floatTbl)]);
floatTbl = floatTbl(1:maxFloats,:);
nFloats = height( floatTbl );
floatTbl.dac = cell( nFloats, 1 );
% For each float, find the file name and dac at IFREMER ftp
for kFloat = 1:nFloats
    floatID = floatTbl.PLATFORM_NO{kFloat};
%     disp( ['Downloading ',num2str(k_downl),' of ',num2str(n_downl),' ',floatID] )
    k_fl = find( strcmp( profTbl.PLATFORM_NO, floatID ), 1, 'first' );
    if( isfinite(k_fl) )
        floatTbl.dac{kFloat} = profTbl.dir{k_fl};
    else
        floatTbl.dac{kFloat} = '';
    end
end
%
end


