function add2plotTimeSeriesPresContours( PTMP, PRES, mtime, contLevels, varargin ) 
%
%   Adds to 2D image plot of time series (X) vs. pressure (Y) contour lines 
%       (isotherms, isopycns, etc.)
%
%   Syntax: add2plotTimeSeriesContours( PTMP, PRES, mtime, contLevels, ... )
%
%   Input [Required]:
%       PTMP - Potential temperature. Matrix (double), (nLayers,nProf) 
%       PRES - Pressure, numeric matrix (nLayers,nProf)
%       mtime - datenum format, size nProf
%       contLevels - potential temperature levels to be plotted
%   Input [Optional]:
%       plotAxes - Axes object. Created by default
%       presRange - the range of pressure to plot (numeric 2x1)
%       lineColor - line color (default 'k')
%       lineWidth - line width (numeric) (default 1)
%       lineStyle - line style (default '-')
%   
%   Output:     no
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-22
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'PTMP',@ismatrix);
addRequired(p,'PRES',@ismatrix);
addRequired(p,'mtime',@isnumeric);
addRequired(p,'contLevels',@isnumeric);
addParameter(p,'plotAxes',[],isaxes);
addParameter(p,'presRange',[],is2nums);
addParameter(p,'lineColor','k',iscolor);
addParameter(p,'lineWidth',1,@isnumeric);
addParameter(p,'lineStyle','-',@ischar);
parse(p, PTMP, PRES, mtime, contLevels, varargin{:} )
PTMP = p.Results.PTMP;
PRES = p.Results.PRES;
mtime = p.Results.mtime;
contLevels = p.Results.contLevels;
ax = p.Results.plotAxes;
presRange = p.Results.presRange;
lineColor = p.Results.lineColor;
lineWidth = p.Results.lineWidth;
lineStyle = p.Results.lineStyle;
%
[nLayers,nProf] = size(PTMP);
if( (size(PRES,1)~=nLayers) || (size(PRES,2)~=nProf) )
    error( 'The size of two input matrices must be equal' )
end
if( length(mtime)~=nProf )
    error('The length of mtime is not equal to the size of matrix')
end
if(isempty(lineWidth))
    lineWidth=1;
end
%
if( isempty(ax) )
    ax = gca;
end
if( isempty(presRange) )
    presRange = get(ax,'YLim');
end
hold(ax,'on')
%
[ PTMPq, Xq1, Yq1 ] = calcTimeSeriesPres( PTMP, PRES, mtime, 'presRange',presRange );
if( length(contLevels) > 1 )
    contLines = contour( Xq1, Yq1, PTMPq, contLevels, 'Visible','off' );
else
    contLines = contour( Xq1, Yq1, PTMPq, [contLevels contLevels], 'Visible','off' );
end
nCont = size(contLines,2);
if( nCont>0 )
    kCont = 1;
    while(kCont<nCont)
        kContStart = kCont+1;
        kContEnd = kCont+contLines(2,kCont);
        plot( ax, contLines(1,kContStart:kContEnd),contLines(2,kContStart:kContEnd),...
            'Color',lineColor, 'lineWidth',lineWidth, 'lineStyle',lineStyle )
        kCont = kContEnd+1;
    end
end
% indxBreaks = (contLines(1,:)<min(Xq1)) | (contLines(1,:)>max(Xq1)) |...
%     (contLines(2,:)<min(Yq1)) | (contLines(2,:)>max(Yq1));
% contLines(:,indxBreaks) = NaN;
% plot( ax, contLines(1,:),contLines(2,:), 'Color',lineColor, 'lineWidth',lineWidth,...
%         'lineStyle',lineStyle )
hold(ax,'off')
%
end


