function gdacFloatsGet( floatTbl, varargin )
%
%   Downloads from GDAC the Argo files from the floatTbl
%
%   Syntax: gdacFloatsGet( floatTbl,... )
%
%   Input [Required]:
%       floatTbl - the table with columns:
%           PLATFORM_NO - the platform ID
%           nProf - the number of profiles in the region of interest
%           dac - the name of DAC
%   Input [Optional]:
%       saveDir - the folder to save the argo profile index file
%   
%   Output: No
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-15
%
p = inputParser;
addRequired(p,'floatTbl',@istable);
addParameter(p,'saveDir',[],@ischar);
parse(p, floatTbl, varargin{:} )
floatTbl = p.Results.floatTbl;
saveDir = p.Results.saveDir;
%
colNames = {'PLATFORM_NO','nProf','dac'};
iFields = ismember(colNames,floatTbl.Properties.VariableNames);
if(~all(iFields))
    error(['No columns in floatTbl: ',strjoin(colNames(~iFields),',')])
end
if( isempty(saveDir) )
    saveDir = pwd;
end
if( exist( saveDir, 'dir' ) ~= 7 )
    error(['Does not exist: ',saveDir])
end
%
startDir = pwd;
cd( saveDir )
% Connect to ftp
ifremer = ftp('ftp.ifremer.fr');
% 
h_wait = waitbar( 0, 'Downloading NetCDF files from GDAC...' );
nFloats = height( floatTbl );
for kFloat = 1:nFloats
    floatID = floatTbl.PLATFORM_NO{kFloat};
    disp( ['Downloading float ',floatID,' (',num2str(kFloat),' of ',num2str(nFloats),')'] )
    cd( ifremer, ['/ifremer/argo/dac/',floatTbl.dac{kFloat},'/',floatID,'/'] );
    mget( ifremer, [floatID,'_prof.nc'] );
    waitbar(kFloat/nFloats, h_wait );
end
close( h_wait )
disp('Done!')
cd( startDir )
%
end


