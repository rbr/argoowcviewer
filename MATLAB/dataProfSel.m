function [trajTblOut,dataStrOut,metaParOut] = dataProfSel(trajTblIn,dataStrIn,metaParIn,profSel)
%
%   Removes from trajectory table, data and metadata structures the profiles with
%       profSel=false
%
%   Syntax: [trajTblOut,dataStrOut,metaParOut] = dataProfSel(trajTblIn,dataStrIn,metaParIn,profSel)
%
%   Input [Required]:
%       trajTblIn - table of size nProf 
%       dataStrIn - the structure with Argo data (PRES, PRES_QC, TEMP, etc), 
%           each a double matrix of size [nLayers*nProf]
%       metaParIn - the array of [nProf] structures with metadata about Argo parameters.
%               Each structure includes the char fields: 
%               parameter (nPar = the number of parameters)
%               scientific_calib_equation (nPar)
%               scientific_calib_coefficient (nPar)
%               scientific_calib_comment (nPar)
%           metaParIn may be []
%       profSel - logical array of size nProf
%
%   Output:     
%       trajTblOut - table of size nProfNew 
%       dataStrOut - the structure with Argo data (PRES, PRES_QC, TEMP, etc), 
%           each a double matrix of size [nLayers*nProfNew]
%       metaParOut - the array of [nProf] structures with metadata about Argo parameters.
%           Similar to metaParIn. 
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-05
%
isStrOrEmpty = @(x) isempty(x) || isstruct(x);
p = inputParser;
addRequired(p,'trajTblIn',@istable);
addRequired(p,'dataStrIn',@isstruct);
addRequired(p,'metaParIn',isStrOrEmpty);
addRequired(p,'profSel',@islogical);
parse(p, trajTblIn, dataStrIn, metaParIn, profSel )
trajTblIn = p.Results.trajTblIn;
dataStrIn = p.Results.dataStrIn;
profSel = p.Results.profSel;
metaParIn = p.Results.metaParIn;
%
nProf = height( trajTblIn );
if( length(profSel) ~= nProf )
    error('The lengths of trajTblIn and profSel are different')
end
strFieldNames = fieldnames( dataStrIn );
nFields = length(strFieldNames);
% Check the size of all fields in dataStrIn
nLayers1 = size(dataStrIn.(strFieldNames{1}),1);
for kField = 1:nFields
    [nLayers2,nProf2] = size(dataStrIn.(strFieldNames{kField}));
    if( nLayers2 ~= nLayers1 )
        error(['The number of layers in ',strFieldNames{kField},' is incorrect'])
    end
    if( nProf2 ~= nProf )
        error(['The number of profiles in ',strFieldNames{kField},' is incorrect'])
    end
end
%
trajTblOut = trajTblIn(profSel,:);
dataStrOut = dataStrIn;
for kField = 1:nFields
    dataStrOut.(strFieldNames{kField}) = dataStrIn.(strFieldNames{kField})(:,profSel);
end
%
if(~isempty(metaParIn))
    metaParOut = metaParIn(profSel);
else
    metaParOut = [];
end

end

