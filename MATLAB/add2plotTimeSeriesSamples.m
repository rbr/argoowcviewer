function add2plotTimeSeriesSamples( VV, PRES, mtime, varargin ) 
%
%   Adds to 2D image plot of time series (X) vs. pressure (Y) the locations 
%       (pressure vs. time) of the finite samples of VV
%
%   Syntax: add2plotTimeSeriesSamples( VV, PRES, mtime,... )
%
%   Input [Required]:
%       VV - Finite values are plotted. Matrix (double), (nLayers,nProf) 
%       PRES - numerical matrix (nLayers,nProf)
%       mtime - datenum format, size nProf
%   Input [Optional]:
%       plotAxes - Axes object. Created by default
%       presRange - the range of pressure to plot (numeric 2x1)
%       marker - marker symbol (char) (default 'o')
%       markerSize - marker size (numeric) (default 1)
%       markerEdgeColor - marker edge color (default 'k')
%       markerFaceColor - marker face color (default [0.5 0.5 0.5]
%   
%   Output:     no
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-21
%
isaxes = @(x) strcmp(get(x,'type'),'axes');
is2nums = @(x) isempty(x) || (all(isnumeric(x)) && (length(x)==2));
iscolor = @(x) (all(isnumeric(x)) && (length(x)==3)) ||...
    (ischar(x) && (ismember(x,{'y','m','c','r','g','b','w','k'})) );
p = inputParser;
addRequired(p,'VV',@ismatrix);
addRequired(p,'PRES',@ismatrix);
addRequired(p,'mtime',@isnumeric);
addParameter(p,'plotAxes',[],isaxes);
addParameter(p,'presRange',[],is2nums);
addParameter(p,'marker','o',@ischar);
addParameter(p,'markerSize',1,@isnumeric);
addParameter(p,'markerEdgeColor','k',iscolor);
addParameter(p,'markerFaceColor',[0.5 0.5 0.5],iscolor);
parse(p, VV, PRES, mtime, varargin{:} )
VV = p.Results.VV;
PRES = p.Results.PRES;
mtime = p.Results.mtime;
ax = p.Results.plotAxes;
presRange = p.Results.presRange;
marker = p.Results.marker;
markerSize = p.Results.markerSize;
markerEdgeColor = p.Results.markerEdgeColor;
markerFaceColor = p.Results.markerFaceColor;
%
[nLayers,nProf] = size(VV);
if( (size(PRES,1)~=nLayers) || (size(PRES,2)~=nProf) )
    error( 'The size of two input matrices must be equal' )
end
if( length(mtime)~=nProf )
    error('The length of mtime is not equal to the size of matrix')
end
%
if( isempty(ax) )
    ax = gca;
end
if( isempty(presRange) )
    presRange = [min(PRES(:)) max(PRES(:))];
end
%
hold(ax,'on')
indx_fin = isfinite( VV );
indxPres = ( PRES >= presRange(1) ) & ( PRES <= presRange(2) );
[ n_layers, ~ ] = size( VV );
XX = repmat( mtime', n_layers, 1 );  
plot( ax, XX(indx_fin&indxPres), PRES(indx_fin&indxPres), 'Marker','o','MarkerSize',1,...
    'LineStyle','none', 'Marker',marker,'MarkerSize',markerSize, 'LineStyle','none',...
    'markerEdgeColor',markerEdgeColor, 'markerFaceColor',markerFaceColor );
hold(ax,'off')
% end

end


