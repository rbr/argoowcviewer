function [ trajTbl, dataStr, owcStr ] = loadOWC( owcDir, owcProjdir, owcFn )
%
%   Load the output of OWC analysis
%
%   Syntax: [ trajTbl, dataStr, owcStr ] = loadOWC( owcDir, owcProjdir, owcFn )
%
%   Input [Required]:
%       owcDir - the path to OWC directory, which must be structured
%           follwing OWC method instructions
%       owcProjdir - the 'project' directories in owcDir/data/float_source/,
%                 owcDir/data/float_calib/ and owcDir/data/float_mapped
%       owcFn - the file name in owcDir/data/float_source/
%
%   Output:
%       trajTbl - the float trajectory table (height nProf) with columns:
%           platform_number - cell of char (size nProf*1)
%           cycle_number - numeric (size nProf*1)
%           latitude - numeric (size nProf*1)
%           longitude - numeric (size nProf*1)
%           mtime - datenum format (size nProf*1) 
%           data_mode - char {'R','A','D'}  (size nProf*1)
%               Contains '' if .mat file does not have this variable
%       dataStr - the structure with Argo data (PRES, TEMP, 
%           SAL, PTMP), each a matrix of size [nLayers,nProf]
%           The fields (PRES_QC,TEMP_QC,SAL_QC,PTMP_QC ) contain ones(nLayers,nProf). 
%       owcStr - the structure with OWC output. The fields:
%           owcTbl - includes the trajTbl columns and the OWC results characterizing profiles:
%                   (avg_Soffset,avg_Soffset_err,avg_Staoffset,avg_Staoffset_err)
%                   and the column profFitCoef ( avg_Staoffset-avg_Soffset)
%           la_mapped_sal - OI mapped salinity (matrix [nLayers,nProf])         
%           la_ptmp - OI mapped salinity (matrix [nLayers,nProf]) 
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-05-09
%
p = inputParser;
addRequired(p,'owcDir',@ischar);
addRequired(p,'owcProjdir',@ischar);
addRequired(p,'owcFn',@ischar);
parse(p, owcDir, owcProjdir, owcFn )
owcDir = p.Results.owcDir;
owcProjdir = p.Results.owcProjdir;
owcFn = p.Results.owcFn;
%
if( exist( owcDir,'dir' ) == 0 )
    error([owcDir,' does not exits'])
end
%
% Load OWC 'float_source'
argoMatPath = [ owcDir, filesep, 'data', filesep, 'float_source', filesep, owcProjdir, filesep, owcFn ];
[ trajTbl, dataStr ] = loadArgoMat( argoMatPath );
% Load OWC 'calibration'
try
    c = load([owcDir,filesep,'data',filesep,'float_calib',filesep,owcProjdir,filesep,'cal_',owcFn]); 
catch
    waitfor(warndlg('No <float_calib> directory','No file in <float_calib> directory'))
    owcStr = struct('owcTbl',[], 'la_mapped_sal',[], 'la_ptmp',[] );
    return
end
% Load OWC 'mapped'
try
    m = load([owcDir,filesep,'data',filesep,'float_mapped',filesep,owcProjdir,filesep,'map_',owcFn]); 
catch
    waitfor(warndlg('No <float_calib> directory','No file in <float_mapped> directory'))
    owcStr = struct('owcTbl',[], 'la_mapped_sal',[], 'la_ptmp',[] );
    return
end
%
owcTbl = calc_Soffset( trajTbl, dataStr.SAL, c );
% Calculate the 'bubble' size
owcTbl.profFitCoef = owcTbl.avg_Staoffset - owcTbl.avg_Soffset;
%
owcStr = struct('owcTbl',owcTbl, 'la_mapped_sal',m.la_mapped_sal, 'la_ptmp',m.la_ptmp );

end

function owc_tbl = calc_Soffset( a_tbl, SAL, c )
%
owc_tbl = a_tbl;
%
Soffset = c.cal_SAL - SAL;
Staoffset = c.sta_SAL - SAL;
n_prof = height( a_tbl );
%
owc_tbl.avg_Soffset = NaN( n_prof, 1 );
owc_tbl.avg_Soffset_err = NaN( n_prof, 1 );
owc_tbl.avg_Staoffset = NaN( n_prof, 1 );
owc_tbl.avg_Staoffset_err = NaN( n_prof, 1 );
%
for i=1:n_prof
   ii=find(isnan(Soffset(:,i))==0);
   if ~isempty(ii)
       owc_tbl.avg_Soffset(i)=mean(Soffset(ii,i));
       owc_tbl.avg_Soffset_err(i)=mean(c.cal_SAL_err(ii,i));
   else
       owc_tbl.avg_Soffset(i) = NaN;
       owc_tbl.avg_Soffset_err(i) = NaN;
   end
   ii=find(isnan(Staoffset(:,i))==0);
   if ~isempty(ii)
       owc_tbl.avg_Staoffset(i)=mean(Staoffset(ii,i));
       owc_tbl.avg_Staoffset_err(i)=mean(c.sta_SAL_err(ii,i));
   else
       owc_tbl.avg_Staoffset(i) = NaN;
       owc_tbl.avg_Staoffset_err(i) = NaN;
   end
end
%
end



