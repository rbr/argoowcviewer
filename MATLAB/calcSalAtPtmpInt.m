function [ SalAtPtmpInt, xMtime, yPtmpOut ] = calcSalAtPtmpInt( SAL, PTMP, PRES,...
    mtime, varargin )
%
%   Calculates salinity at potential temperature levels, interpolated at
%   regular time intervals
%
%   Syntax: [SalAtPtmpInt,xMtime,yPtmp]=calcSalAtPtmpInt(SAL,PTMP,PRES,mtime,...)
%
%   Input [Required]:
%       SAL - salinity, numeric matrix of size nLayers*nProf
%       PTMP - potential temperature, numeric matrix of size nLayers*nProf
%       PRES - pressure, numeric matrix of size nLayers*nProf
%       mtime - time in datenum format, length nProf
%   Input [Optional]:
%       yPtmp - the array of potential temperatures for interpolation,
%           default linspace(min(PTMP(:)),max(PTMP(:)),100)
%       nX - the size of xMtime vector (numeric scalar) (default 100)
%
%   Output:  
%       SalAtPtmpInt - salinities at potential temperature levels, size (length(yPtmp),nProf)
%       xMtime - vector of regular datetime steps for interpolation,
%           (length nX)
%       yPtmpOut - vector of potential temperatures for interpolation
%
% Author: Nikolay Nezlin (RBR Ltd. Ottawa ON, Canada)
% email: nikolay.nezlin@rbr-global.com
% Website: www.rbr-global.com
% Last revision: 2020-04-23
%
p = inputParser;
addRequired(p,'SAL',@ismatrix);
addRequired(p,'PTMP',@ismatrix);
addRequired(p,'PRES',@ismatrix);
addRequired(p,'mtime',@isnumeric);
addParameter(p,'yPtmp',[],@isnumeric);
addParameter(p,'nX',100,@isnumeric);
parse(p, SAL, PTMP, PRES, mtime, varargin{:} )
SAL = p.Results.SAL;
PTMP = p.Results.PTMP;
PRES = p.Results.PRES;
mtime = p.Results.mtime;
yPtmpIn = p.Results.yPtmp;
nX = p.Results.nX;
%
[nLayers,nProf] = size(SAL);
if( size(PTMP,1)~=nLayers || size(PRES,1)~=nLayers || size(PTMP,2)~=nProf ||...
        size(PRES,2)~=nProf )
    error('The three input matrices must be of equal size')
end
if(isempty(yPtmpIn))
    nPtmp = 100;
    yPtmpOut = linspace(min(PTMP(:)),max(PTMP(:)),nPtmp);
else
    yPtmpOut = yPtmpIn;
end
% 
SalAtPtmp = calcSalAtPtmp( SAL, PTMP, PRES, 'yPtmp',yPtmpOut );
xMtime = linspace( min(mtime), max(mtime), nX )';
[ Xq, Yq ] = meshgrid( xMtime, yPtmpOut );
SalAtPtmpInt = interp2( mtime, yPtmpOut, SalAtPtmp, Xq, Yq, 'linear' );

end


