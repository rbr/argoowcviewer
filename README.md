# MATLAB interactive tool for Argo salinity Delayed Mode Quality Control visualization

## Abstract

New software is presented to assist users of Argo data to 1) prepare real-time Argo observations for Delayed Mode Quality Control (DMQC) analysis by the ‘Owens-Wong-Cabanes’ (OWC) method and 2) analyze the OWC output. The ArgoOWCviewer MATLAB toolbox includes a number of functions which can be operated from a Graphical User Interface (GUI) or the MATLAB command window. The analysis of OWC output is focused on detecting regions with problematic reference data which could be misinterpreted as salinity sensor drift and erroneously corrected in the DMQC Argo datasets.  

## Method details 

Analyzing the stability of Argo float conductivity sensors is an essential part of the Delayed Mode Quality Control (DMQC) process. Assessments of salinity offset and drift are performed by the ‘Owens-Wong-Cabanes’ (OWC) method (Cabanes et al. 2016; Owens and Wong 2009), which is approved by the Argo community (Wong et al. 2020). This method compares the data collected by an Argo float to reference data calculated using the Objective Mapping (OM) method from the reference profiles.  Most participants of Argo program use the reference datasets approved and used by the Argo community and prepared by the Argo Data Management Team (ADMT) at the Coriolis Data Centre (ftp.ifremer.fr/coriolis). The OWC method is implemented in a MATLAB toolbox available from https://github.com/ArgoDMQC/matlab_owc.

The MATLAB toolbox **argoOWCviewer** described in this document can be used in parallel with the OWC toolbox for two purposes. First, it helps to prepare for OWC analysis the Argo data obtained from the Argo Global Data Assembly Center (GDAC). Second, **argoOWCviewer** helps users to analyze the OWC output and to investigate disagreements between the salinity measured by the Argo float and the reference data. The **argoOWCviewer** toolbox includes MATLAB functions which can be operated interactively using a MATLAB graphic user interface (GUI) programs `guiArgoNetCDF2owc` and `guiArgoOWCviewer`. Otherwise, these functions can be started directly from MATLAB command window.  The toolbox was written and tested in MATLAB 2015b.

### Installation

Copy the MATLAB codes to the desired folder and set the path to this folder in your MATLAB environment. Also, for mapping, we recommend downloading the most recent version of the `m_map` toolbox (https://www.eoas.ubc.ca/~rich/map.html). Install `m_map` and set its path in your MATLAB environment. We also recommend installing the high-resolution coastline database GSHHS (https://www.eoas.ubc.ca/~rich/mapug.html#p8.6.1). The mapping functions of `argoOWCviewer` work without this database, but the quality of the maps is not as good as with GSHHS.

For analysis of the OWC output, one must download one or more of the five climatologies (Table 1). We recommend installing all five. You can install them either by selecting in the main menu of the interface program 'guiArgoOWCviewer' the command **“File > Install climatologies”**, or by starting in your MATLAB environment the function `climInstall`. 

##### Table 1. Climatologies used by the toolbox argoOWCviewer.###

| N  |                 Climatology                 |     Reference    |  Access  |    
| ---|---------------------------------------------|------------------|--------- |
| 1  | World Ocean Atlas of 1-degree resolution    |  (Garcia et al.  | [link](https://www.nodc.noaa.gov/OC5/woa18/woa18data.html) | 
|    | (WOA1), the 2005-2017 decadal period        |   2018)          |          |
| 2  | World Ocean Atlas of 0.25-degree resolution |  (Garcia et al.  | [link](https://www.nodc.noaa.gov/OC5/woa18/woa18data.html) | 
|    | (WOA4), averaged decades starting 1955      |   2018)          |          |
| 3  | Monthly Isopycnal & Mixed-layer Ocean       | (Schmidtko et al.| [link](https://www.pmel.noaa.gov/mimoc/) | 
|    | Climatology (MIMOC)                         | 2013)            |          |
| 4  | CSIRO Atlas of Regional Seas (CARS2009)     | (Ridgway et al.  | [link](http://www.marine.csiro.au/atlas/) |
|    |                                             | 2002)            |          |
| 5  | Roemmich-Gilson Argo Climatology            | (Roemmich and    | [link](http://sio-argo.ucsd.edu/RG_Climatology.html) |
|    |                                             | Gilson, 2009)    |          |

## Preparing Argo data for OWC analysis 

Argo data from the GDACS are in NetCDF format. Each Argo NetCDF file contains core Argo variables (pressure, temperature and salinity) as raw and adjusted (corrected) datasets. Preparing these data for OWC analysis includes: 1) reading the NetCDF file to MATLAB environment; 2) preliminary analysis; 3) selecting which datasets (raw or adjusted) the user is going to process by the OWC toolbox; 4) removing outliers; 5) removing the profiles the user does not need to analyze and 6) saving the data in a MATLAB MAT file formatted specifically for the OWC toolbox. 

The most convenient way to prepare Argo profiles for OWC analysis is starting the interactive MATLAB application `guiArgoNetCDF2owc` (Figure 1a).

![Figure 1](pngFigures/Figure01.png)

##### Figure 1. The interactive application `guiArgoNetCDF2owc` before (a) and after (b) reading Argo NetCDF file.

\.

Use the main menu commands to set the folders for NetCDF and OWC mat files by clicking **“File > Set directory with NetCDF files”** and **“File > Set directory with mat files”**. The selected directories are consistent between MATLAB sessions. 

Click **“File > Open Argo NetCDF file”**. If you wish to make additional edits to the file already prepared for OWC analysis (e.g., remove some profiles), click **“File > Open Argo OWC mat file”**.

The program reads the file and demonstrates the information about the Argo float (Figure 2b):

*  the map of float trajectory
*  The Argo WMO ID
*  the information about the number of profiles and layers
*  which dataset (raw or adjusted) is currently selected (default is ‘adjusted’)
*  the table with the numbers of cycles attributed to different data modes (D = 'delayed mode'; A = 'adjusted mode'; R = 'real-time mode').

To see, whether or not the dataset contains outliers, the user may want to see the vertical profiles of temperature, salinity and TS-diagram (Figure 2).   For this, click **“Figure > Profiles of temperature”**, **“Figure > Profiles of salinity”** or **“Figure > Theta-S diagram”**.

![Figure 2](pngFigures/Figure02.png)

##### Figure 2. The interactive application `guiArgoNetCDF2owc` demonstrating the salinity profiles (a) and the TS-diagram (b) of the selected Argo float.

\.

The color of profiles at the map and the vertical plots is related to the time when the profiles were collected. This helps to see the differences between the measurements collected during different periods of the float operation.  

To see more details, the user can click **“Figure > Figure properties > Profiles and TS”** and select different limits of pressure, temperature and salinity. Also, the user can change the title, colormap, font size, etc. The selected values will be used in all plots during the current session. Also, the user can edit the map (the latitude and longitude limits, land color, the resolution of coastline and the font size of the map axes), and the time-series plots, including the time (X-axis) and the Y-axis limits and the tick style for time X-axis. The later can be set to days, months, years with appropriate intervals between the ticks.

Each plot can be exported to “png” format by the command **“Figure > Export plot”**.

Another option is to click **“Figure > Open plot in new figure”**. It starts a new figure window, in which the user can zoom, edit and save the plot as MATLAB “fig” file for future use.

Different Argo regional Data Assembly Centers (DAC) use raw and adjusted datasets differently. Some of them include the data checked for outliers by coarse semi-automatic Real-Time Quality Control (RTQC) tests into raw datasets, other DACs put them into adjusted data.  As such, the user preparing data for DMQC has to make a choice between raw and adjusted data sources.

To get more information about raw and adjusted datasets, the user can click **“Metadata > Adjustment equations”** and see the information about the methods used to adjust data in each profile. To see how much these equations transformed the data, the user can plot the profiles of the differences between raw and adjusted core Argo variables (pressure, temperature and salinity using the commands in the **“Figure”** menu). After clicking **“Metadata > Missing values”** the user can see the table with the number of missing values in the currently selected dataset (Figure 3a). When the user sees that the data, he/she is going to analyze are stored raw dataset, he/she should click **“Select datasets”** and click the appropriate radio-button (Figure 3b).

![Figure 3](pngFigures/Figure03.png)

##### Figure 3. The table of missing values (a) and selection of the datasets (b).

\.

To select the data points based on their quality control, click **“Select data points > Select QC”**, unselect the checkboxes with QC you wish to remove and click **“Select”**. 

To remove outliers, click **“Select data points > Select points in rectangle”**, click twice at the plot to select the rectangle (Figure 4a), click **“Select data points > Remove points in rectangle”** and see the result (Figure 4b).

![Figure 4](pngFigures/Figure04.png)

##### Figure 4. Removing outliers in the Argo dataset.

\.

To select a subset of profiles, click “Select profiles”.

You can choose from the following options:

*  Select profiles by minimum depth (pressure).  
*  Select profiles by minimum number of salinity data.
*  Select profiles by start date
*  Select profiles by end date
*  Select profiles by the data mode
*  Select profiles from the table

To make a selection, check the checkbox, enter the value to the edit-box and click **“Select”**. If you wish to manually select profiles from the list, click the button **“Select profiles from table”** and unselect the checkboxes of the profiles you prefer not to save in OWC MATLAB file. To save the resulting dataset in MATLAB file readable by OWC toolbox, click **“Save to .mat file -> Save”**. If the dataset contains profiles with no salinity data, the program will report how many profiles are empty and ask your permission to remove them.  

Before saving, the program suggests to the user to interpolate Argo data to different pressure levels. This operation makes sense when the Argo file contains profiles of high resolution (e.g., at 1 dbar interval). OWC analysis of such files takes long time (up to several hours). Interpolating salinity on pressure levels of 10 dbar interval significantly decreases the number of pressure levels and the time of OWC processing. To interpolate, check the box “Interpolate to pressure levels”. It activates the edit box, where the user can enter the desired pressure levels in MATLAB “brackets” ([]) format. By default, the program selects the minimum pressure rounded toward infinity (MATLAB ceil function), multiples of 10 dbar and the maximum pressure rounded toward minus infinity (MATLAB “floor” function).

The program `guiArgoNetCDF2owc` and its function `save2OWCmat` save in the MATLAB OWC file all variables used by the OWC toolbox plus two additional variables: `mtime` (the time when the profiles were collected in MATLAB “datenum” format) and `data_mode` (the data mode, which can be ‘R’,’A’ or ‘D’, which means ‘real-time’, ‘adjusted’ and ‘delayed-mode’, respectively). OWC toolbox does not use these variables, but they are used by the **argoOWCviewer** toolbox analyzing the OWC output. When the function `loadArgoMat` is reading an Argo OWC file which has no these variables, they are calculated automatically. 

All these operations can be performed directly from MATLAB environment. This option may be preferable for advanced MATLAB programmers. Each function has a small number of required inputs and a large number of optional inputs used mostly to improve the quality of the plots. The complete list of inputs for each function can be listed by the command `help functionName`.

**argoOWCviewer** toolbox operates Argo data in the structures returned by the function `readArgoNc`:

*  `trajTbl` – the table with profile coordinates, time, data mode, platform number, cycle numbers;
*  `dataStr` – a structure including the matrices of pressure, temperature, potential temperature and salinity and the QC codes. The function `readArgoNc` returns two structures of similar format: dataRaw (with raw Argo data) and dataAdj (with adjusted data);
*  `metaPar` – a structure with metadata describing the method of adjustment of the core Argo variables.

Preliminary analysis of the Argo dataset can be performed using the following functions:

*  `plotTrajMap` – plots the map of the float trajectory using different colors indicating the time when the profiles were collected.  
*  `plotProf` – plots the vertical profiles using as vertical axis either pressure or potential temperature. Profiles are colored similarly to the trajectory map, helping to see the differences between different periods of the float operation. 
*  `plotTS` – plots a potential temperature-salinity (TS) diagram using colors similar to `plotTrajMap` and `plotProf`.
*  `plotTimeBar` – plots a time series. The **argoOWCviewer** toolbox uses this function to plot the maximum profile depths and the number of samples in each profile. These plots help user to select the profiles based on the minimum number of collected data or minimum depth of observations. 
*  `calcMisValTbl` – calculates and returns the table with the number of missing values for each parameter.
*  `plotTSQC` – plots a TS diagram with Argo quality codes assigned to salinity samples on the basis of the real-time quality control (QC) tests. 
*  `calcQcTbl` – calculates and returns the table with the number of salinity samples with different QC codes.  
*  `dataStrQc2nan` – replaces with NaNs the data with the selected QC codes.
*  `dataProfSel` – returns the new trajectory table `trajTbl` and the new data structure `dataSrt` with selected profiles.
*  `dataStrIntPres` – interpolates data to pressure levels. Decreasing the number of samples in profiles speeds up OWC analysis.
*  `save2OWCmat` – saves the Argo float data to MATLAB file readable by OWC toolbox. 
*  `loadArgoMat` – loads the MATLAB data file saved by `save2OWCmat` for additional analysis/editing.  

An example of using these functions is given in the code [codeExampleArgoNc2owc.m](MATLAB/codeExampleArgoNc2owc.m). 

### Analysis of the results of the Owens-Wong-Cabanes (OWC) salinity correction 

The disagreement between the salinity measured by the Argo float and the reference data can be explained by either sensor drift or the problems with reference data resulting from inaccurate measurements and/or inadequate interpolation. These problems can be detected from collocation of the ‘profile fit coefficients’ (the measure of disagreement between the Argo observations and the reference data) along the float trajectory map. Another method of control of the accuracy of reference data is comparing the interpolated by OWC method reference salinity to other reference source (climatology). The third method is getting from GDAC Argo floats operating at the same time in the same area and the analysis of the trends in salinity measurements. Similar trends in the same area indicate problems with reference data and motivate the user to reject the hypothesis of sensor drift.  

OWC toolbox reads the Argo OWC mat file from the subfolder `data/float_source` and saves the results into the subfolders `data/float_calib`,  `data/float_mapped` and `data/float_plots`.

In the **argoOWCviewer** toolbox, the function `loadOWC` loads the file (created in the previous section) from the subfolder `float_source` and looks for the files with similar name in the subfolders `float_calib` (calibration) and `float_mapped` (objectively mapped reference salinity). The data are returned as the structures `trajTbl` and `dataStr`, similar to the structures described in the previous section.  The data from the folders `float_calib` and `float_mapped` are returned in the structure `owcStr`.  If the files in `float_calib`  and `float_mapped` subfolders are not found (it may happen when the user prepared the data for OWC analysis but did not complete it), the function `loadOWC` returns the structure `owcStr` with empty fields.  In this case, the functions of the **argoOWCviewer** toolbox which require `owcStr` cannot be called and the interface `guiArgoOWCviewer` disables these menu commands.

When the user starts `guiArgoOWCviewer` first time, he/she needs to install climatologies. Click **“File > Set climatology directory”** to select the folder where climatologies will be installed. Then, click **“File > Install climatologies”**, check the boxes for the climatologies to be installed (the climatologies already installed in the selected directory are automatically unchecked) and click the button **“Install”**. Take into account that installation takes time up to several hours.  The climatologies used by the **argoOWCviewer** toolbox are listed in Table 1.

To load the Argo file, click **“File > Load Argo OWC .mat file”**. After the file is loaded, the user can see the map of the float trajectory (Figure 1b) and the information about the file name, Argo WMO ID, the number of profiles and layers and the table with the number of profiles of different data mode (D/A/R). If the Argo source file is associated with the files in the OWC output, the table of selected potential temperature levels (used for stability analysis) is shown. 

The commands **“Figure > Map of profiles”**, **“File > Profiles of theta”**, **“File > Profiles of salinity”** and **“Theta-S diagram”** are similar to the interface `guiArgoNetCDF2owc` (Figures 1-2).

Click **“Figure > Timeseries SAL / PRES”** to plot the salinity variations in the depth (pressure) range (Figure 5a). Click **“Figure > Figure properties > Theta levels”** and check the box **“Show theta levels”** too see the levels of potential temperature (theta) analyzed by the OWC method or selected by the menu command **“Theta levels”**. To see the zones of temperature inversions, click **“Figure > Timeseries Theta gradient / PRES”** (Figure 5b).

![Figure 5](pngFigures/Figure05NOC.png)

##### Figure 5. The time series of Argo salinity (a) and potential temperature vertical gradient (b) for the NOC Argo float 7626 operating in the North Atlantic and the Labrador Sea. The zones of temperature inversions in (b) are shown in red.

\.

The locations of profiles and samples can be added to the plot by clicking **“Figure > Figure properties > Samples”**, checking the box and selecting the marker size and face/edge colors. 

To see the plot of salinity with potential temperature as Y axis, click **“Figure > Timeseries SAL / Theta”**. To edit the Y axis, click **“Figure > Figure properties”**, click the button **“Y axis limits”**, and in the edit field **“Theta”** enter the theta (Y axis) limits.  To see the bottom layer in more detail, enter three values instead of two (e.g., `2 5 10`).  As a result, the vertical axis is divided into two (2-5 and 5-10 in this example) with different scales of Y axes (Figure 6).  To see more details in the lower subplot, click **“Figure > Figure properties > Color limits”** and select different minimum and maximum of salinity color scale.  

![Figure 6](pngFigures/Figure06.png)

##### Figure 6. The time series of Argo salinity with potential temperature as Y-axis.

\.

To compare the OWC input/output to climatology, click in the main menu **“Climatology > Get climatology”**, select the climatology source and click **“Select”**. This operation enables the menu elements which require climatology. 

To see the differences between the salinity measured by the Argo float and climatology, click **“Figure > Timeseries SAL anomaly (vs climatology) / Theta”**. To see these differences in most detail in the deep layer with Theta < 5°C and to ignore the layer with Theta > 10°C, click **“Figure > Figure properties > Y axis limits”** and enter in the **“Theta”** field `2 5 10`. To plot different contours, click **“Figure > Figure properties > Color limits”** and enter different **“Interval”** in the row **“Salinity anomalies (abs)”**.

To see the OWC profile fit coefficients (the disagreement between the measured and reference salinity averaged at 10 potential temperature levels with minimum variability), click **“Figure > OWC fit with errors”** (Figure 7).

![Figure 7](pngFigures/Figure07.png)

##### Figure 7. The time series of OWC profile fit coefficients (the differences between the salinity offsets calculated by OWC and salinities at 10 selected potential temperature levels).

\.

To see, how much the differences between the profile fit coefficients and the constant offset are collocated in space (indicating problems in reference data), click first **“Figure > OWC fit: timescale with bubbles”** (Figure 8a), save this figure and click **“Figure > OWC fit: map with bubbles”** (Figure 8b). 

![Figure 8](pngFigures/Figure08.png)

##### Figure 8. The collocation of the OWC salinity profile fit coefficients (the disagreement between the measured and reference salinity) in time and along the float trajectory demonstrating reference data problems.

\.

To see the disagreement between the reference salinity calculated by OWC method and one of the 5 available climatologies, click **“Figure > OWC reference-climatology”** (Figure 9a). To combine the plots of profile fit coefficients with the disagreement between different references salinities, click **“Figure > OWC fit and reference-climatology”** (Figure 9b).

![Figure 9](pngFigures/Figure09.png)

##### Figure 9. The differences between OWC reference salinity and climatology (a) and the same plot combined with the time series of the OWC profile fit coefficients (b). The coincidence between large disagreements indicate problems with reference data.

\.

To compare the temporal trends in profile fit coefficients at several floats operating at the same time in the same area, click from the main menu **“Floats from GDAC”**. First, select **“Contour polygon and click Enter”**. In the map, contour by mouse clicks the polygon and hit the **“Enter”** key (Figure 10). Then, click **“Floats from GDAC > Find Argo floats”**. Enter the start and end time (the suggested values are based on the profiles in the selected polygon performed by the Argo float you are working with), select the ocean (it speeds up the search). If the recent GDAC profile index file `ar_index_global_prof.txt` is downloaded to your machine and already selected by the command **“File > Set GDAC index file”**, the checkbox **“Use existing index file”** is checked. Uncheck it if you wish to replace the file you already have to new one (it takes time because its size is about 800Mb). Click the button **“Find Argo floats in the contoured area”**. After some time, the table is demonstrated with Argo floats with the number of profiles performed in the selected polygon during the selected time period. Uncheck the floats you do not wish to download and click the button **“Download files from GDAC”**. If the used forgot to set the directory for GDAC files (by clicking the button **“Set directory for GDAC files”** or by the menu command **“File > Set directory for GDAC files”**), the files are downloaded to working directory.

![Figure 10](pngFigures/Figure10.png)

##### Figure 10. Contour of the area of interest where all available Argo floats will be found and downloaded from GDAC.

\.

These files can be converted to OWC MATLAB format using the program `guiArgoNetCDF2owc`, processed using the OWC toolbox with the same settings and reference sources, and the results analyzed in `guiArgoOWCviewer`. Similar trends demonstrated by different floats in the same area indicate problems with reference data rather than sensor drift. 

The described above analysis can be performed directly from MATLAB environment by calling the functions listed below.

The following functions do not need OWC calibration and mapped reference salinity files and can be used for analysis of the Argo OWC source file:

*  `loadArgoMat` loads the source OWC MATLAB file only.
*  `loadOWC` – loads the OWC source file and the OWC output files.  When the output files are not found, an empty `owcStr` structure is returned. 
*  `plotTrajMap, plotProf, plotTS` – described in the section **Preparing Argo data for OWC analysis**.
*  `calcTimeSeriesPres` – calculates a regular grid of salinity or temperature (depending on the input arguments) for demonstration by the function `plotTimeSeriesPres`. 
*  `plotTimeSeriesPres` – plots the color diagram of salinity or temperature with time as X-axis and pressure as Y-axis (see Figure 5a).
*  `add2plotTimeSeriesSamples` – adds to the diagram plotted by the function `plotTimeSeriesPres` markers indicating when (time) and at what depths (pressure) Argo data were collected. 
*  `add2plotTimeSeriesPresContours` - adds to the diagram plotted by the function `plotTimeSeriesPres` the lines indicating the depths of contours of the selected parameter. Used for demonstration of potential temperature levels over salinity or salinity anomalies plots (see Figure 5ab). 
*  `plotTimeSeriesPresGrad` – plots the diagram of vertical gradient of the input parameter. Used to demonstrate potential temperature inversions (see Figure 5b). 
*  `calcSalAtPtmp` - calculates salinity at potential temperature levels (after removing temperature inversions).
*  `calcSalAtPtmpInt` - calculates salinity at potential temperature levels and interpolates each level to regular time intervals. 
*  `climTblSet` – creates the table with climatology sources. 
*  `climAlongTraj` – extracts climatological temperature and salinity along the float trajectory. Returns the structure `CLMdata` with fields PRES (pressure), SAL (salinity), TEMP (temperature) and PTMP (potential temperature). 
*  `plotTimeSeriesPtmp` – plots a time series of the input parameter (usually salinity anomaly) with potential temperature as a Y axis. To demonstrate deep layer in more details, the plot can be divided in two subplots with one X axis and two different Y axes (see Figure 6).

An example of using these functions is given in the code [codeExampleArgoOWCinput.m](MATLAB/codeExampleArgoOWCinput.m). Similar to the functions described above, these functions include a small number of requited input and a large number of optional inputs (in the form `…‘Name’,’Value’,…`) helping users to produce figures of high quality.

To analyze the OWC toolbox output (the ‘source’, ‘calibration’ and ‘mapped’ files), the **argoOWCviewer** toolbox suggests the following functions:

*  `plotOWCsOffsets` – plots as a time series the calculated by OWC method salinity offsets with errors.
*  `plotOWCsOffsetsBB` – plots as a time series OWC salinity offsets as markers (‘bubbles’), which size and color indicate the disagreement between them and the constant salinity offset. This plot is used in combination with the map where Argo profiles are plotted as markers of similar size and color (the function `plotOWCsOffsetsBBmap`). 
*  `plotOWCsOffsetsBBmap` – plots the map to be compared with the time series plotted by the function `plotOWCsOffsetsBB`. 
*  `plotOWCsOffsetsClim` – the plot with two subplots with the same X axis (time), combining `plotOWCsOffsets` and `plotTimeSeriesPtmp`. 
*  `gdacFloatsFind` - finds in the IFREMER GDAC index file the Argo profiles collected in the polygon region and during the time period.  Returns the table of Argo floats with the numbers of profiles in the area/period of interest. If the GDAC index file of Argo profiles is not provided as an optional argument, it is downloaded from the IFREMER GDAC ftp. 
*  `gdacFloatsGet` – downloads from GDAC the Argo floats found by the function `gdacFloatsFind`. These datafiles can be converted into OWC MATLAB format using `guiArgoNetCDF2owc` or other functions of this toolbox (see the section **Preparing Argo data for OWC analysis**), processed by the OWC toolbox and analyzed using the functions described in this section. The resulting plots demonstrating similar trends in the same areas indicate problematic reference data rather than sensor drift.

The examples of using these functions are given in the file [codeExampleArgoOWCoutput.m](MATLAB/codeExampleArgoOWCoutput.m).

Downloading from GDAC the Argo files operating in the same area during the same time can be performed from MATLAB environment. The example is in the file [codeExampleArgoFromGDAC.m](MATLAB/codeExampleArgoFromGDAC.m).

### Additional information

Several visualization tools were developed for analysis of Argo datasets; some of them are listed at the Argo website (http://www.argodatamgt.org/DMQC/Tools-for-DMQC). Most of these tools were focused on detection of outliers during the RTQC of Argo data. We can mention the Visual Quality Control System (VQCS) developed for RTQC of the Argo floats in the Indian Ocean (Udaya Bhaskar et al. 2012; Udaya Bhaskar et al. 2013) and the Scoop-Argo system (with interface in French) (Jerome et al. 2017). These tools help users to visually control the quality flags assigned to Argo data and determine whether automatic Argo tests were excessively flagging good measurements as bad or vice-versa.

An example of the interactive tool most close to the focus of our work is the MATLAB code called a beta-version of PMEL GUI interactive system (https://github.com/ArgoDMQC/PMEL_GUI), which is used for visual comparison of OWC output to available Argo reference profiles and CTD profiles from the World Ocean Database 2018 (Boyer et al. 2018). This kind of analysis helps in assessment of the agreement between the Argo data corrected by the OWC method and other profiles collected in that area.

### References: 

Boyer, T. P., and Coauthors, 2018: World Ocean Database 2018.

Cabanes, C., V. Thierry, and C. Lagadec, 2016: Improvement of bias detection in Argo float conductivity sensors and its application in the North Atlantic. Deep-Sea Research I, 114, 128-136, https://doi.org/10.1016/j.dsr.2016.05.007.

Garcia, H. E., and Coauthors, 2018: World Ocean Atlas 2018 (pre-release): Product Documentation.

Jerome, D., G. Mickael, C. Thierry, T. Baptiste, and M. Pierre, 2017: Scoop-Argo: visual quality control for Argo NetCDF data files. SEANOE, https://doi.org/10.17882/48531.

Owens, W. B., and A. P. S. Wong, 2009: An improved calibration method for the drift of the conductivity sensor on autonomous CTD profiling floats by θ–S climatology. Deep-Sea Research I, 56(3), 450-457, https://doi.org/10.1016/j.dsr.2008.09.008.

Ridgway, K. R., J. R. Dunn, and J. L. Wilkin, 2002: Ocean interpolation by four-dimensional least squares - Application to the waters around Australia. Journal of Atmospheric and Oceanic Technology, 19(9), 1357-1375, https://doi.org/10.1175/1520-0426(2002)019<1357:OIBFDW>2.0.CO;2.

Roemmich, D., and J. Gilson, 2009: The 2004-2008 mean and annual cycle of temperature, salinity, and steric height in the global ocean from the Argo Program. Progress in Oceanography, 82(2Oceanography), 81-100, https://doi.org/10.1016/j.pocean.2009.03.004.

Schmidtko, S., G. C. Johnson, and J. M. Lyman, 2013: MIMOC: A global monthly isopycnal upper-ocean climatology with mixed layers. Journal of Geophysical Research: Oceans, 118(4), 1658–1672, https://doi.org/10.1002/jgrc.20122.

Udaya Bhaskar, T. V. S., E. P. R. Rao, R. V. Shesu, and R. Devender, 2012: A note on three way quality control of Argo temperature and salinity profiles - A semi-automated approach at INCOIS. International Journal of Earth Sciences and Engineering, 5 (6), 1510 - 1514.

Udaya Bhaskar, T. V. S., R. V. Shesu, E. P. R. Rao, and R. Devender, 2013: GUI based interactive system for Visual Quality Control of Argo data. Indian Journal of Geo-Marine Sciences, 42, 580-586.

Wong, A. P. S., R. Keeley, T. Carval, and Argo Data Management Team, 2020: Argo Quality Control Manual for CTD and Trajectory Data.



---

See the details how to use the toolbox in the manual and using the commands `help function-name` in your MATLAB environment. 
